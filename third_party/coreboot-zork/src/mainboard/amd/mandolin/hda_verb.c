/*
 * This file is part of the coreboot project.
 *
 * Copyright (C) 2019 Advanced Micro Devices, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <device/azalia_device.h>

const u32 cim_verb_data[] = {
	/* Realtek ALC701 */
	0x10ec0701,
	0x00000000,
	0x00000016,

	AZALIA_SUBVENDOR(0, 0x1022D001), // HDA Codec Subsystem ID: 0x1022D001

	0x0017ff00, 0x0017ff00, 0x0017ff00, 0x0017ff00, // Widget node 0x01 :
	0x01271c40, 0x01271d01, 0x01271ea6, 0x01271fb7, // Pin widget 0x12 - DMIC
	0x01371c00, 0x01371d00, 0x01371e00, 0x01371f40, // Pin widget 0x13 - DMIC
	0x01471c10, 0x01471d01, 0x01471e17, 0x01471f90, // Pin widget 0x14 - FRONT (Port-D)
	0x01571cf0, 0x01571d11, 0x01571e11, 0x01571f41, // Pin widget 0x15 - I2S-OUT
	0x01671cf0, 0x01671d11, 0x01671e11, 0x01671f41, // Pin widget 0x16 - LINE3 (Port-B)
	0x01771cf0, 0x01771d11, 0x01771e11, 0x01771f41, // Pin widget 0x17 - I2S-OUT
	0x01871cf0, 0x01871d11, 0x01871e11, 0x01871f41, // Pin widget 0x18 - I2S-IN
	0x01971cf0, 0x01971d11, 0x01971e11, 0x01971f41, // Pin widget 0x19 - MIC2 (Port-F)
	0x01a71cf0, 0x01a71d11, 0x01a71e11, 0x01a71f41, // Pin widget 0x1A - LINE1 (Port-C)
	0x01b71c50, 0x01b71d10, 0x01b71ea1, 0x01b71f04, // Pin widget 0x1B - LINE2 (Port-E)
	0x01d71c01, 0x01d71d00, 0x01d71e60, 0x01d71f40, // Pin widget 0x1D - PC-BEEP
	0x01e71c30, 0x01e71d11, 0x01e71e45, 0x01e71f04, // Pin widget 0x1E - S/PDIF-OUT
	0x01f71cf0, 0x01f71d11, 0x01f71e11, 0x01f71f41, // Pin widget 0x1F - S/PDIF-IN
	0x02171c20, 0x02171d10, 0x02171e21, 0x02171f04, // Pin widget 0x21 - HP-OUT (Port-I)
	0x02971cf0, 0x02971d11, 0x02971e11, 0x02971f41, // Pin widget 0x29 - I2S-IN
	0x02050038, 0x02047901, 0x0205006b, 0x02040260, // NID 0x20 -0 Set Class-D output
							//  power as 2.2W@4 Ohm, and
							//  MIC2-VREFO-R is controlled by
							//  Line2 port.
	0x0205001a, 0x02048c03, 0x02050045, 0x0204b289, // NID 0x20 - 1
	0x0205004a, 0x0204201b, 0x0205004a, 0x0204201b, // NID 0x20 - 2
	0x02050010, 0x02040420, 0x01470c00, 0x02050036, // Dos beep path - 1
	0x02047151, 0x01470740, 0x0143b000, 0x01470c02, // Dos beep path - 2

	/* Realtek ALC285 */
	0x10ec0285,
	0x00000000,
	0x00000028,

	AZALIA_SUBVENDOR(0, 0x1022D002),

	0x0017ff00, 0x0017ff00, 0x0017ff00, 0x0017ff00, // Widget node 0x01 :
	0x01271c40, 0x01271d01, 0x01271ea6, 0x01271fb7, // Pin widget 0x12 - DMIC
	0x01371c00, 0x01371d00, 0x01371e00, 0x01371f40, // Pin widget 0x13 - DMIC
	0x01471c10, 0x01471d01, 0x01471e17, 0x01471f90, // Pin widget 0x14 - Front (Port-D)
	0x01671cf0, 0x01671d11, 0x01671e11, 0x01671f41, // Pin widget 0x16 - NPC
	0x01771cf0, 0x01771d11, 0x01771e11, 0x01771f41, // Pin widget 0x17 - I2S OUT
	0x01871cf0, 0x01871d11, 0x01871e11, 0x01871f41, // Pin widget 0x18 - I2S IN
	0x01971cf0, 0x01971d11, 0x01971e11, 0x01971f41, // Pin widget 0x19 - MIC2 (Port-F)
	0x01a71cf0, 0x01a71d11, 0x01a71e11, 0x01a71f41, // Pin widget 0x1A - NPC
	0x01b71c30, 0x01b71d90, 0x01b71ea1, 0x01b71f04, // Pin widget 0x1B - LINE2 (Port-E)
	0x01d71c2d, 0x01d71d19, 0x01d71e66, 0x01d71f40, // Pin widget 0x1D - BEEP-IN
	0x01e71cf0, 0x01e71d11, 0x01e71e11, 0x01e71f41, // Pin widget 0x1E - S/PDIF-OUT
	0x02171c20, 0x02171d10, 0x02171e21, 0x02171f04, // Pin widget 0x21 - HP1-OUT (Port-I)
	0x05c50011, 0x05c40003, 0x05c50011, 0x05c40003, // dis. Silence detect delay turn off
	0x0205003c, 0x0204f254, 0x0205003c, 0x0204f214, // Class-D power on reset
	0x02050045, 0x0204b009, 0x02050063, 0x02040020, // Set TRS + turn off MIC2 VREFO
							//  gating with HP-JD.
	0x0205004a, 0x020420b0, 0x02050009, 0x02043803, // Enable HP JD + Set JD2 to 1 port
							//  JD for WoV
	0x0205000b, 0x0204777a, 0x0205000b, 0x0204777a, // Set TRS + Set JD2 pull up.
	0x02050038, 0x02043909, 0x05c50000, 0x05c43482, // NID 0x20 set class-D to 2W@4ohm
							//  (+12dB gain) + Set sine
							//  tone gain(0x34)
	0x05350000, 0x0534002a, 0x05350000, 0x0534002a, // Disable EQ + set 100Hz 2nd High
							//  Pass filter
	0x0535001d, 0x05340800, 0x0535001e, 0x05340800, // Left Channel-1
	0x05350005, 0x053403f6, 0x05350006, 0x0534854c, // Left Channel-2
	0x05350007, 0x05341e09, 0x05350008, 0x05346472, // Left Channel-3
	0x05350009, 0x053401fb, 0x0535000a, 0x05344836, // Left Channel-4
	0x0535000b, 0x05341c00, 0x0535000c, 0x05340000, // Left Channel-5
	0x0535000d, 0x05340200, 0x0535000e, 0x05340000, // Left Channel-6
	0x05450000, 0x05440000, 0x0545001d, 0x05440800, // Right Channel-1
	0x0545001e, 0x05440800, 0x05450005, 0x054403f6, // Right Channel-2
	0x05450006, 0x0544854c, 0x05450007, 0x05441e09, // Right Channel-3
	0x05450008, 0x05446472, 0x05450009, 0x054401fb, // Right Channel-4
	0x0545000a, 0x05444836, 0x0545000b, 0x05441c00, // Right Channel-5
	0x0545000c, 0x05440000, 0x0545000d, 0x05440200, // Right Channel-6
	0x0545000e, 0x05440000, 0x05350000, 0x0534c02a, // Right Channel-7+ EQ Update & Enable
	0x05d50006, 0x05d44c50, 0x05d50002, 0x05d46004, // AGC-1 Disable + (Front Gain=0dB )
	0x05d50003, 0x05d45e5e, 0x05d50001, 0x05d4d783, // AGC-2 (Back Boost Gain = -0.375dB,
							//  Limiter = -1.125dB)
	0x05d50009, 0x05d451ff, 0x05d50006, 0x05d44e50, // AGC-3 + AGC Enable
	0x02050010, 0x02040020, 0x02050040, 0x02048800, // EAPD set to verb-control.
	0x02050030, 0x02049000, 0x02050037, 0x0204fe15, // Class D silent detection Enable
							//  -84dB threshold
	0x05b50006, 0x05b40044, 0x05a50001, 0x05a4001f, // Set headphone gain and Set pin1
							//  to GPIO2
};

const u32 pc_beep_verbs[] = {
};

AZALIA_ARRAY_SIZES;
