From 36881a947b2fbd4c523cde7a946f6a6f86d7afb5 Mon Sep 17 00:00:00 2001
From: Joel Hockey <joelhockey@chromium.org>
Date: Thu, 31 Jan 2019 19:15:25 -0800
Subject: [PATCH 5/9] shared.Unpack: Add support for a ProgressTracker during
 unpack

This is part 3 of a series of patches to add better progress
tracking support for export and import.

Signed-off-by: Joel Hockey <joelhockey@chromium.org>
---
 lxd/images.go            |  7 ++++---
 lxd/storage_btrfs.go     |  2 +-
 lxd/storage_ceph.go      |  2 +-
 lxd/storage_dir.go       |  2 +-
 lxd/storage_lvm.go       |  2 +-
 lxd/storage_lvm_utils.go |  2 +-
 lxd/storage_zfs.go       |  2 +-
 shared/archive_linux.go  | 41 ++++++++++++++++++++++++++++++----------
 8 files changed, 41 insertions(+), 19 deletions(-)

diff --git a/lxd/images.go b/lxd/images.go
index fc7c2c87..60647725 100644
--- a/lxd/images.go
+++ b/lxd/images.go
@@ -32,6 +32,7 @@ import (
 	"github.com/lxc/lxd/lxd/util"
 	"github.com/lxc/lxd/shared"
 	"github.com/lxc/lxd/shared/api"
+	"github.com/lxc/lxd/shared/ioprogress"
 	"github.com/lxc/lxd/shared/logger"
 	"github.com/lxc/lxd/shared/logging"
 	"github.com/lxc/lxd/shared/osarch"
@@ -97,14 +98,14 @@ var aliasCmd = Command{
    end for whichever finishes last. */
 var imagePublishLock sync.Mutex
 
-func unpackImage(imagefname string, destpath string, sType storageType, runningInUserns bool) error {
+func unpackImage(imagefname string, destpath string, sType storageType, runningInUserns bool, tracker *ioprogress.ProgressTracker) error {
 	blockBackend := false
 
 	if sType == storageTypeLvm || sType == storageTypeCeph {
 		blockBackend = true
 	}
 
-	err := shared.Unpack(imagefname, destpath, blockBackend, runningInUserns)
+	err := shared.Unpack(imagefname, destpath, blockBackend, runningInUserns, tracker)
 	if err != nil {
 		return err
 	}
@@ -116,7 +117,7 @@ func unpackImage(imagefname string, destpath string, sType storageType, runningI
 			return fmt.Errorf("Error creating rootfs directory")
 		}
 
-		err = shared.Unpack(imagefname+".rootfs", rootfsPath, blockBackend, runningInUserns)
+		err = shared.Unpack(imagefname+".rootfs", rootfsPath, blockBackend, runningInUserns, tracker)
 		if err != nil {
 			return err
 		}
diff --git a/lxd/storage_btrfs.go b/lxd/storage_btrfs.go
index 6c781e8e..853722d3 100644
--- a/lxd/storage_btrfs.go
+++ b/lxd/storage_btrfs.go
@@ -1469,7 +1469,7 @@ func (s *storageBtrfs) ImageCreate(fingerprint string) error {
 
 	// Unpack the image in imageMntPoint.
 	imagePath := shared.VarPath("images", fingerprint)
-	err = unpackImage(imagePath, tmpImageSubvolumeName, storageTypeBtrfs, s.s.OS.RunningInUserNS)
+	err = unpackImage(imagePath, tmpImageSubvolumeName, storageTypeBtrfs, s.s.OS.RunningInUserNS, nil)
 	if err != nil {
 		return err
 	}
diff --git a/lxd/storage_ceph.go b/lxd/storage_ceph.go
index c61ba839..255c5b09 100644
--- a/lxd/storage_ceph.go
+++ b/lxd/storage_ceph.go
@@ -1899,7 +1899,7 @@ func (s *storageCeph) ImageCreate(fingerprint string) error {
 
 		// rsync contents into image
 		imagePath := shared.VarPath("images", fingerprint)
-		err = unpackImage(imagePath, imageMntPoint, storageTypeCeph, s.s.OS.RunningInUserNS)
+		err = unpackImage(imagePath, imageMntPoint, storageTypeCeph, s.s.OS.RunningInUserNS, nil)
 		if err != nil {
 			logger.Errorf(`Failed to unpack image for RBD storage volume for image "%s" on storage pool "%s": %s`, fingerprint, s.pool.Name, err)
 
diff --git a/lxd/storage_dir.go b/lxd/storage_dir.go
index faa4a731..f2814e6d 100644
--- a/lxd/storage_dir.go
+++ b/lxd/storage_dir.go
@@ -508,7 +508,7 @@ func (s *storageDir) ContainerCreateFromImage(container container, imageFingerpr
 	}()
 
 	imagePath := shared.VarPath("images", imageFingerprint)
-	err = unpackImage(imagePath, containerMntPoint, storageTypeDir, s.s.OS.RunningInUserNS)
+	err = unpackImage(imagePath, containerMntPoint, storageTypeDir, s.s.OS.RunningInUserNS, nil)
 	if err != nil {
 		return err
 	}
diff --git a/lxd/storage_lvm.go b/lxd/storage_lvm.go
index 0e58853a..944bf947 100644
--- a/lxd/storage_lvm.go
+++ b/lxd/storage_lvm.go
@@ -1623,7 +1623,7 @@ func (s *storageLvm) ImageCreate(fingerprint string) error {
 		}
 
 		imagePath := shared.VarPath("images", fingerprint)
-		err = unpackImage(imagePath, imageMntPoint, storageTypeLvm, s.s.OS.RunningInUserNS)
+		err = unpackImage(imagePath, imageMntPoint, storageTypeLvm, s.s.OS.RunningInUserNS, nil)
 		if err != nil {
 			return err
 		}
diff --git a/lxd/storage_lvm_utils.go b/lxd/storage_lvm_utils.go
index c103f8ff..6370fd3c 100644
--- a/lxd/storage_lvm_utils.go
+++ b/lxd/storage_lvm_utils.go
@@ -458,7 +458,7 @@ func (s *storageLvm) containerCreateFromImageLv(c container, fp string) error {
 
 	imagePath := shared.VarPath("images", fp)
 	containerMntPoint := getContainerMountPoint(s.pool.Name, containerName)
-	err = unpackImage(imagePath, containerMntPoint, storageTypeLvm, s.s.OS.RunningInUserNS)
+	err = unpackImage(imagePath, containerMntPoint, storageTypeLvm, s.s.OS.RunningInUserNS, nil)
 	if err != nil {
 		logger.Errorf(`Failed to unpack image "%s" into non-thinpool LVM storage volume "%s" for container "%s" on storage pool "%s": %s`, imagePath, containerMntPoint, containerName, s.pool.Name, err)
 		return err
diff --git a/lxd/storage_zfs.go b/lxd/storage_zfs.go
index e6282e14..f49c1fcc 100644
--- a/lxd/storage_zfs.go
+++ b/lxd/storage_zfs.go
@@ -1791,7 +1791,7 @@ func (s *storageZfs) ImageCreate(fingerprint string) error {
 	}
 
 	// Unpack the image into the temporary mountpoint.
-	err = unpackImage(imagePath, tmpImageDir, storageTypeZfs, s.s.OS.RunningInUserNS)
+	err = unpackImage(imagePath, tmpImageDir, storageTypeZfs, s.s.OS.RunningInUserNS, nil)
 	if err != nil {
 		return err
 	}
diff --git a/shared/archive_linux.go b/shared/archive_linux.go
index ca359763..c1419363 100644
--- a/shared/archive_linux.go
+++ b/shared/archive_linux.go
@@ -8,6 +8,7 @@ import (
 	"strings"
 	"syscall"
 
+	"github.com/lxc/lxd/shared/ioprogress"
 	"github.com/lxc/lxd/shared/logger"
 )
 
@@ -54,7 +55,7 @@ func DetectCompressionFile(f io.ReadSeeker) ([]string, string, []string, error)
 	}
 }
 
-func Unpack(file string, path string, blockBackend bool, runningInUserns bool) error {
+func Unpack(file string, path string, blockBackend bool, runningInUserns bool, tracker *ioprogress.ProgressTracker) error {
 	extractArgs, extension, _, err := DetectCompression(file)
 	if err != nil {
 		return err
@@ -62,6 +63,7 @@ func Unpack(file string, path string, blockBackend bool, runningInUserns bool) e
 
 	command := ""
 	args := []string{}
+	var reader io.Reader
 	if strings.HasPrefix(extension, ".tar") {
 		command = "tar"
 		if runningInUserns {
@@ -73,8 +75,32 @@ func Unpack(file string, path string, blockBackend bool, runningInUserns bool) e
 		}
 		args = append(args, "-C", path, "--numeric-owner", "--xattrs-include=*")
 		args = append(args, extractArgs...)
-		args = append(args, file)
+		args = append(args, "-")
+
+		f, err := os.Open(file)
+		if err != nil {
+			return err
+		}
+		defer f.Close()
+
+		reader = f
+
+		// Attach the ProgressTracker if supplied.
+		if tracker != nil {
+			fsinfo, err := f.Stat()
+			if err != nil {
+				return err
+			}
+
+			tracker.Length = fsinfo.Size()
+			reader = &ioprogress.ProgressReader{
+				ReadCloser: f,
+				Tracker:    tracker,
+			}
+		}
 	} else if strings.HasPrefix(extension, ".squashfs") {
+		// unsquashfs does not support reading from stdin,
+		// so ProgressTracker is not possible.
 		command = "unsquashfs"
 		args = append(args, "-f", "-d", path, "-n")
 
@@ -91,7 +117,7 @@ func Unpack(file string, path string, blockBackend bool, runningInUserns bool) e
 		return fmt.Errorf("Unsupported image format: %s", extension)
 	}
 
-	output, err := RunCommand(command, args...)
+	err = RunCommandWithFds(reader, nil, command, args...)
 	if err != nil {
 		// Check if we ran out of space
 		fs := syscall.Statfs_t{}
@@ -110,14 +136,9 @@ func Unpack(file string, path string, blockBackend bool, runningInUserns bool) e
 			}
 		}
 
-		co := output
 		logger.Debugf("Unpacking failed")
-		logger.Debugf(co)
-
-		// Truncate the output to a single line for inclusion in the error
-		// message.  The first line isn't guaranteed to pinpoint the issue,
-		// but it's better than nothing and better than a multi-line message.
-		return fmt.Errorf("Unpack failed, %s.  %s", err, strings.SplitN(co, "\n", 2)[0])
+		logger.Debugf(err.Error())
+		return fmt.Errorf("Unpack failed, %s.", err)
 	}
 
 	return nil
-- 
2.20.1.791.gb4d0f1c61a-goog

