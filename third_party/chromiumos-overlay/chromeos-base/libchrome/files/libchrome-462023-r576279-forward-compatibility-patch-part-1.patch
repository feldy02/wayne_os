From 664cad704e2c9b7d5f70f5d02555a980a12ca492 Mon Sep 17 00:00:00 2001
From: Hidehiko Abe <hidehiko@chromium.org>
Date: Sat, 10 Aug 2019 01:49:12 +0900
Subject: [PATCH 1/6] libchrome: r576279 forward compatibility patch part 1.

This CL includes;
- NumberToString overload.
- TaskRunner::BelongsToCurrentThread(),RunsTasksInCurrentSequence()
  alias.
- Value::SetKey()
- SimpleAlarmTimer::Create().
- base/memory/scoped_refptr.h
- base/posix/unix_domain_socket.h
- base/memory/shared_memory_handle.h::GetHandle/GetSize/IsValid/Release
- base/memory/shared_memory.h::GetReadOnlyHandle
- base/message_loop/message_pump_libevent.h:FdWatcher/FdWatchController.
- base/message_loop/message_loop.h:FileDescriptorWatcher::Mode

BUG=chromium:909719
TEST=Build.

Change-Id: I7d987fea70704375a5a3296349749648a19ffa7c
---
 base/files/file_descriptor_watcher_posix.cc |  4 +--
 base/memory/scoped_refptr.h                 | 31 +++++++++++++++++++++
 base/memory/shared_memory.h                 |  6 ++++
 base/memory/shared_memory_handle.h          | 31 ++++++++++++++++++++-
 base/memory/shared_memory_posix.cc          | 13 +++++++--
 base/message_loop/message_loop.h            |  9 +++---
 base/message_loop/message_pump_libevent.h   |  2 ++
 base/posix/unix_domain_socket.h             | 12 ++++++++
 base/strings/string_number_conversions.h    | 20 +++++++++++++
 base/task_runner.h                          |  8 ++++++
 base/values.cc                              |  5 ++++
 base/values.h                               |  2 ++
 components/timers/alarm_timer_chromeos.cc   |  8 ++++++
 components/timers/alarm_timer_chromeos.h    |  2 ++
 14 files changed, 142 insertions(+), 11 deletions(-)
 create mode 100644 base/memory/scoped_refptr.h
 create mode 100644 base/posix/unix_domain_socket.h

diff --git a/base/files/file_descriptor_watcher_posix.cc b/base/files/file_descriptor_watcher_posix.cc
index 9746e35..31dd27c 100644
--- a/base/files/file_descriptor_watcher_posix.cc
+++ b/base/files/file_descriptor_watcher_posix.cc
@@ -120,7 +120,7 @@ void FileDescriptorWatcher::Controller::Watcher::StartWatching() {
 void FileDescriptorWatcher::Controller::Watcher::OnFileCanReadWithoutBlocking(
     int fd) {
   DCHECK_EQ(fd_, fd);
-  DCHECK_EQ(MessageLoopForIO::WATCH_READ, mode_);
+  DCHECK_EQ(MessagePumpLibevent::WATCH_READ, mode_);
   DCHECK(thread_checker_.CalledOnValidThread());
 
   // Run the callback on the sequence on which the watch was initiated.
@@ -131,7 +131,7 @@ void FileDescriptorWatcher::Controller::Watcher::OnFileCanReadWithoutBlocking(
 void FileDescriptorWatcher::Controller::Watcher::OnFileCanWriteWithoutBlocking(
     int fd) {
   DCHECK_EQ(fd_, fd);
-  DCHECK_EQ(MessageLoopForIO::WATCH_WRITE, mode_);
+  DCHECK_EQ(MessagePumpLibevent::WATCH_WRITE, mode_);
   DCHECK(thread_checker_.CalledOnValidThread());
 
   // Run the callback on the sequence on which the watch was initiated.
diff --git a/base/memory/scoped_refptr.h b/base/memory/scoped_refptr.h
new file mode 100644
index 0000000..0815c34
--- /dev/null
+++ b/base/memory/scoped_refptr.h
@@ -0,0 +1,31 @@
+// Copyright 2019 The Chromium Authors. All rights reserved.
+// Use of this source code is governed by a BSD-style license that can be
+// found in the LICENSE file.
+
+// This file is for forward compatibility to uprev r576279.
+
+#ifndef BASE_MEMORY_SCOPED_REFPTR_H_
+#define BASE_MEMORY_SCOPED_REFPTR_H_
+
+#include "base/memory/ref_counted.h"
+
+namespace base {
+
+// Constructs an instance of T, which is a ref counted type, and wraps the
+// object into a scoped_refptr<T>.
+template <typename T, typename... Args>
+    scoped_refptr<T> MakeRefCounted(Args&&... args) {
+  T* obj = new T(std::forward<Args>(args)...);
+  return subtle::AdoptRefIfNeeded(obj, T::kRefCountPreference);
+}
+
+// Takes an instance of T, which is a ref counted type, and wraps the object
+// into a scoped_refptr<T>.
+template <typename T>
+    scoped_refptr<T> WrapRefCounted(T* t) {
+  return scoped_refptr<T>(t);
+}
+
+}  // namespace base
+
+#endif  // BASE_MEMORY_SCOPED_REFPTR_H_
diff --git a/base/memory/shared_memory.h b/base/memory/shared_memory.h
index 4b66cc6..fb580ce 100644
--- a/base/memory/shared_memory.h
+++ b/base/memory/shared_memory.h
@@ -207,6 +207,12 @@ class BASE_EXPORT SharedMemory {
   // It is safe to call Close repeatedly.
   void Close();
 
+  SharedMemoryHandle GetReadOnlyHandle() {
+    SharedMemoryHandle new_handle;
+    GiveReadOnlyToProcess(GetCurrentProcessHandle(), &new_handle);
+    return new_handle;
+  }
+
   // Shares the shared memory to another process.  Attempts to create a
   // platform-specific new_handle which can be used in a remote process to read
   // the shared memory file.  new_handle is an output parameter to receive the
diff --git a/base/memory/shared_memory_handle.h b/base/memory/shared_memory_handle.h
index dc33eea..1054a71 100644
--- a/base/memory/shared_memory_handle.h
+++ b/base/memory/shared_memory_handle.h
@@ -7,6 +7,7 @@
 
 #include <stddef.h>
 
+#include "base/unguessable_token.h"
 #include "build/build_config.h"
 
 #if defined(OS_WIN)
@@ -28,7 +29,35 @@ namespace base {
 // SharedMemoryHandle is a platform specific type which represents
 // the underlying OS handle to a shared memory segment.
 #if defined(OS_POSIX) && !(defined(OS_MACOSX) && !defined(OS_IOS))
-typedef FileDescriptor SharedMemoryHandle;
+class BASE_EXPORT SharedMemoryHandle: public FileDescriptor {
+ public:
+  // Before
+  SharedMemoryHandle() : FileDescriptor() {}
+  SharedMemoryHandle(int ifd, bool iauto_close)
+    : FileDescriptor(ifd, iauto_close) {}
+  // After
+  SharedMemoryHandle(const SharedMemoryHandle& handle) = default;
+  SharedMemoryHandle(const base::FileDescriptor& file_descriptor,
+                     size_t size,
+                     const base::UnguessableToken& guid)
+          : FileDescriptor(file_descriptor) {}
+  SharedMemoryHandle& operator=(const SharedMemoryHandle &handle) = default;
+  static SharedMemoryHandle ImportHandle(int fd, size_t _size) {
+    return SharedMemoryHandle(fd, false);
+  }
+  int GetHandle() const {
+    return this->fd;
+  }
+  size_t GetSize() const;
+  bool IsValid() const {
+    return this->fd >= 0;
+  }
+  int Release() {
+    int old_fd = this->fd;
+    this->fd = -1;
+    return old_fd;
+  }
+};
 #elif defined(OS_WIN)
 class BASE_EXPORT SharedMemoryHandle {
  public:
diff --git a/base/memory/shared_memory_posix.cc b/base/memory/shared_memory_posix.cc
index 287e55d..e442eec 100644
--- a/base/memory/shared_memory_posix.cc
+++ b/base/memory/shared_memory_posix.cc
@@ -85,7 +85,7 @@ SharedMemoryHandle SharedMemory::DuplicateHandle(
   int duped_handle = HANDLE_EINTR(dup(handle.fd));
   if (duped_handle < 0)
     return base::SharedMemory::NULLHandle();
-  return base::FileDescriptor(duped_handle, true);
+  return base::SharedMemoryHandle(duped_handle, true);
 }
 
 // static
@@ -310,11 +310,11 @@ bool SharedMemory::Unmap() {
 }
 
 SharedMemoryHandle SharedMemory::handle() const {
-  return FileDescriptor(mapped_file_, false);
+  return SharedMemoryHandle(mapped_file_, false);
 }
 
 SharedMemoryHandle SharedMemory::TakeHandle() {
-  FileDescriptor handle(mapped_file_, true);
+  SharedMemoryHandle handle(mapped_file_, true);
   mapped_file_ = -1;
   memory_ = nullptr;
   mapped_size_ = 0;
@@ -415,4 +415,11 @@ bool SharedMemory::GetUniqueId(SharedMemory::UniqueId* id) const {
   return true;
 }
 
+size_t SharedMemoryHandle::GetSize() const {
+  size_t size;
+  if (!SharedMemory::GetSizeFromSharedMemoryHandle(*this, &size))
+    return 0;
+  return size;
+}
+
 }  // namespace base
diff --git a/base/message_loop/message_loop.h b/base/message_loop/message_loop.h
index fa054f4..b0a2cd0 100644
--- a/base/message_loop/message_loop.h
+++ b/base/message_loop/message_loop.h
@@ -597,11 +597,10 @@ class BASE_EXPORT MessageLoopForIO : public MessageLoop {
   typedef MessagePumpLibevent::FileDescriptorWatcher
       FileDescriptorWatcher;
 
-  enum Mode {
-    WATCH_READ = MessagePumpLibevent::WATCH_READ,
-    WATCH_WRITE = MessagePumpLibevent::WATCH_WRITE,
-    WATCH_READ_WRITE = MessagePumpLibevent::WATCH_READ_WRITE
-  };
+  using Mode = MessagePumpLibevent::Mode;
+  constexpr static Mode WATCH_READ = MessagePumpLibevent::WATCH_READ;
+  constexpr static Mode WATCH_WRITE = MessagePumpLibevent::WATCH_WRITE;
+  constexpr static Mode WATCH_READ_WRITE = MessagePumpLibevent::WATCH_READ_WRITE;
 #endif
 
 #if defined(OS_WIN)
diff --git a/base/message_loop/message_pump_libevent.h b/base/message_loop/message_pump_libevent.h
index 1124560..ef6c7fe 100644
--- a/base/message_loop/message_pump_libevent.h
+++ b/base/message_loop/message_pump_libevent.h
@@ -34,6 +34,7 @@ class BASE_EXPORT MessagePumpLibevent : public MessagePump {
    protected:
     virtual ~Watcher() {}
   };
+  using FdWatcher = Watcher;
 
   // Object returned by WatchFileDescriptor to manage further watching.
   class FileDescriptorWatcher {
@@ -82,6 +83,7 @@ class BASE_EXPORT MessagePumpLibevent : public MessagePump {
 
     DISALLOW_COPY_AND_ASSIGN(FileDescriptorWatcher);
   };
+  using FdWatchController = FileDescriptorWatcher;
 
   enum Mode {
     WATCH_READ = 1 << 0,
diff --git a/base/posix/unix_domain_socket.h b/base/posix/unix_domain_socket.h
new file mode 100644
index 0000000..5112731
--- /dev/null
+++ b/base/posix/unix_domain_socket.h
@@ -0,0 +1,12 @@
+// Copyright 2019 The Chromium Authors. All rights reserved.
+// Use of this source code is governed by a BSD-style license that can be
+// found in the LICENSE file.
+
+// This file is for forward compatibility to uprev r576279.
+
+#ifndef BASE_POSIX_UNIX_DOMAIN_SOCKET_H_
+#define BASE_POSIX_UNIX_DOMAIN_SOCKET_H_
+
+#include "base/posix/unix_domain_socket_linux.h"
+
+#endif  // BASE_POSIX_UNIX_DOMAIN_SOCKET_H_
diff --git a/base/strings/string_number_conversions.h b/base/strings/string_number_conversions.h
index a95544e..776451c 100644
--- a/base/strings/string_number_conversions.h
+++ b/base/strings/string_number_conversions.h
@@ -42,15 +42,31 @@ namespace base {
 BASE_EXPORT std::string IntToString(int value);
 BASE_EXPORT string16 IntToString16(int value);
 
+inline std::string NumberToString(int value) {
+  return IntToString(value);
+}
+
 BASE_EXPORT std::string UintToString(unsigned value);
 BASE_EXPORT string16 UintToString16(unsigned value);
 
+inline std::string NumberToString(unsigned value) {
+  return UintToString(value);
+}
+
 BASE_EXPORT std::string Int64ToString(int64_t value);
 BASE_EXPORT string16 Int64ToString16(int64_t value);
 
+inline std::string NumberToString(int64_t value) {
+  return Int64ToString(value);
+}
+
 BASE_EXPORT std::string Uint64ToString(uint64_t value);
 BASE_EXPORT string16 Uint64ToString16(uint64_t value);
 
+inline std::string NumberToString(uint64_t value) {
+  return Uint64ToString(value);
+}
+
 BASE_EXPORT std::string SizeTToString(size_t value);
 BASE_EXPORT string16 SizeTToString16(size_t value);
 
@@ -59,6 +75,10 @@ BASE_EXPORT string16 SizeTToString16(size_t value);
 // locale. If you want to use locale specific formatting, use ICU.
 BASE_EXPORT std::string DoubleToString(double value);
 
+inline std::string NumberToString(double value) {
+  return DoubleToString(value);
+}
+
 // String -> number conversions ------------------------------------------------
 
 // Perform a best-effort conversion of the input string to a numeric type,
diff --git a/base/task_runner.h b/base/task_runner.h
index 0421d56..973eae1 100644
--- a/base/task_runner.h
+++ b/base/task_runner.h
@@ -80,6 +80,14 @@ class BASE_EXPORT TaskRunner
   // general to use 'true' as a default value.
   virtual bool RunsTasksOnCurrentThread() const = 0;
 
+  inline bool BelongsToCurrentThread() const {
+    return RunsTasksOnCurrentThread();
+  }
+
+  inline bool RunsTasksInCurrentSequence() const {
+    return RunsTasksOnCurrentThread();
+  }
+
   // Posts |task| on the current TaskRunner.  On completion, |reply|
   // is posted to the thread that called PostTaskAndReply().  Both
   // |task| and |reply| are guaranteed to be deleted on the thread
diff --git a/base/values.cc b/base/values.cc
index b5e44e6..dc67293 100644
--- a/base/values.cc
+++ b/base/values.cc
@@ -340,6 +340,11 @@ bool Value::GetAsDictionary(const DictionaryValue** out_value) const {
   return is_dict();
 }
 
+Value* Value::SetKey(StringPiece key, Value&& value) {
+  CHECK(is_dict());
+  return ((**dict_ptr_)[key.as_string()] = std::make_unique<Value>(std::move(value))).get();
+}
+
 Value* Value::DeepCopy() const {
   return new Value(*this);
 }
diff --git a/base/values.h b/base/values.h
index 925152d..9f916d2 100644
--- a/base/values.h
+++ b/base/values.h
@@ -153,6 +153,8 @@ class BASE_EXPORT Value {
   bool GetAsDictionary(const DictionaryValue** out_value) const;
   // Note: Do not add more types. See the file-level comment above for why.
 
+  Value* SetKey(StringPiece key, Value&& value);
+
   // This creates a deep copy of the entire Value tree, and returns a pointer
   // to the copy. The caller gets ownership of the copy, of course.
   // Subclasses return their own type directly in their overrides;
diff --git a/components/timers/alarm_timer_chromeos.cc b/components/timers/alarm_timer_chromeos.cc
index e332466..c59ec93 100644
--- a/components/timers/alarm_timer_chromeos.cc
+++ b/components/timers/alarm_timer_chromeos.cc
@@ -166,4 +166,12 @@ SimpleAlarmTimer::SimpleAlarmTimer() : AlarmTimer(true, false) {
 SimpleAlarmTimer::~SimpleAlarmTimer() {
 }
 
+std::unique_ptr<SimpleAlarmTimer> SimpleAlarmTimer::Create() {
+  auto result = std::make_unique<SimpleAlarmTimer>();
+  if (!result->CanWakeFromSuspend()) {
+    return nullptr;
+  }
+  return result;
+}
+
 }  // namespace timers
diff --git a/components/timers/alarm_timer_chromeos.h b/components/timers/alarm_timer_chromeos.h
index 8066704..88f2588 100644
--- a/components/timers/alarm_timer_chromeos.h
+++ b/components/timers/alarm_timer_chromeos.h
@@ -101,6 +101,8 @@ class SimpleAlarmTimer : public AlarmTimer {
  public:
   SimpleAlarmTimer();
   ~SimpleAlarmTimer() override;
+
+  std::unique_ptr<SimpleAlarmTimer> Create();
 };
 
 }  // namespace timers
-- 
2.24.1.735.g03f4e72817-goog

