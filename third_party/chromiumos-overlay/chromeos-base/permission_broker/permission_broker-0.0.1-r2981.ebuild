# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI="5"

CROS_WORKON_COMMIT="df5917bf15f95fdbd23335756c2237feb92ff911"
CROS_WORKON_TREE=("587fcc1fc96e0444ffe553cf04588b83796f3de2" "6ebc0d7d913e63ad0a38a0ca233f19e8e9c19912" "e7dba8c91c1f3257c34d4a7ffff0ea2537aeb6bb")
CROS_WORKON_INCREMENTAL_BUILD=1
CROS_WORKON_LOCALNAME="platform2"
CROS_WORKON_PROJECT="chromiumos/platform2"
CROS_WORKON_OUTOFTREE_BUILD=1
CROS_WORKON_SUBTREE="common-mk permission_broker .gn"

PLATFORM_NATIVE_TEST="yes"
PLATFORM_SUBDIR="${PN}"

inherit cros-workon platform udev user

DESCRIPTION="Permission Broker for Chromium OS"
HOMEPAGE="http://www.chromium.org/"

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"
IUSE="cfm_enabled_device fuzzer"

RDEPEND="
	chromeos-base/libbrillo
	sys-apps/dbus
	virtual/libusb:1
	virtual/udev"

DEPEND="${RDEPEND}
	chromeos-base/system_api[fuzzer?]
	sys-kernel/linux-headers"

src_install() {
	dobin "${OUT}"/permission_broker

	# Install upstart configuration
	insinto /etc/init
	doins permission_broker.conf

	# DBus configuration
	insinto /etc/dbus-1/system.d
	doins dbus/org.chromium.PermissionBroker.conf

	# Udev rules for hidraw nodes
	udev_dorules "${FILESDIR}/99-hidraw.rules"

	# Fuzzer.
	platform_fuzzer_install "${S}"/OWNERS "${OUT}"/firewall_fuzzer
	platform_fuzzer_install "${S}"/OWNERS "${OUT}"/port_tracker_fuzzer
}

platform_pkg_test() {
	local tests=(
		permission_broker_test
	)

	local test_bin
	for test_bin in "${tests[@]}"; do
		platform_test "run" "${OUT}/${test_bin}"
	done
}

pkg_preinst() {
	enewuser "devbroker"
	enewgroup "devbroker"
}
