# Copyright 2019 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

CROS_WORKON_COMMIT="af7d9c9aa857dfa686552bad4c20878b6d26ecd4"
CROS_WORKON_TREE=("2ef18d1c42c7aee2c4bb4110359103045c055adf" "c73e1f37fdaafa35e9ffaf067aca34722c2144cd" "405f8436d55479a28352d139c8510bb4db458dc2" "e7dba8c91c1f3257c34d4a7ffff0ea2537aeb6bb")
CROS_WORKON_INCREMENTAL_BUILD=1
CROS_WORKON_LOCALNAME="platform2"
CROS_WORKON_PROJECT="chromiumos/platform2"
CROS_WORKON_DESTDIR="${S}/platform2"
# TODO(crbug.com/809389): Avoid directly including headers from other packages.
CROS_WORKON_SUBTREE="common-mk libpasswordprovider smbfs .gn"

PLATFORM_SUBDIR="smbfs"

inherit cros-workon platform user

DESCRIPTION="FUSE filesystem to mount SMB shares."
HOMEPAGE="https://chromium.googlesource.com/chromiumos/platform2/+/master/smbfs/"

LICENSE="BSD-Google"
SLOT="0"
KEYWORDS="*"

RDEPEND="
	=sys-fs/fuse-2.9*
	net-fs/samba
"

DEPEND="
	${RDEPEND}
	chromeos-base/system_api
	chromeos-base/libpasswordprovider
"

pkg_preinst() {
	enewuser "fuse-smbfs"
	enewgroup "fuse-smbfs"
}

src_install() {
	dosbin "${OUT}"/smbfs

	insinto /usr/share/policy
	newins seccomp_filters/smbfs-seccomp-"${ARCH}".policy smbfs-seccomp.policy
}

platform_pkg_test() {
	local tests=(
		smbfs_test
	)

	local test_bin
	for test_bin in "${tests[@]}"; do
		platform_test "run" "${OUT}/${test_bin}"
	done
}
