# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=7

CROS_WORKON_COMMIT="df5917bf15f95fdbd23335756c2237feb92ff911"
CROS_WORKON_TREE=("587fcc1fc96e0444ffe553cf04588b83796f3de2" "2822c12aff73ad311bb921794d0f5b6557558313" "e7dba8c91c1f3257c34d4a7ffff0ea2537aeb6bb")
CROS_WORKON_INCREMENTAL_BUILD="1"
CROS_WORKON_LOCALNAME="platform2"
CROS_WORKON_PROJECT="chromiumos/platform2"
CROS_WORKON_OUTOFTREE_BUILD=1
CROS_WORKON_SUBTREE="common-mk arc/vm/launch .gn"

inherit cros-workon

# TODO(b/142424802): Rename the package to e.g. arcvm-common-scripts.
DESCRIPTION="A package to run arcvm."
HOMEPAGE="https://chromium.googlesource.com/chromiumos/platform2/+/master/arc/vm/launch"

LICENSE="BSD-Google"
KEYWORDS="*"

src_install() {
	insinto /etc/init
	doins arc/vm/launch/init/arcvm-ureadahead.conf

	insinto /usr/share/arcvm
	doins arc/vm/launch/init/config.json
}
