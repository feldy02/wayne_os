/* Copyright (c) 2018, The Linux Foundation. All rights reserved.
 * Copyright (c) 2018 Google, Inc
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

// QCS MISTRAL DTS

/dts-v1/;

#include "qcs404.dtsi"
#include "pms405.dtsi"
#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/pinctrl/qcom,pmic-gpio.h>

/ {
	model = "Google QCS404/Mistral";
	compatible = "google,mistral", "qcom,qcs404";

	aliases {
		serial0 = &blsp1_uart2;
		serial1 = &blsp1_uart3;
		wifi0 = &wifi;
	};

	chosen {
		stdout-path = "serial0";
	};

	vph_pwr: vph-pwr-regulator {
		compatible = "regulator-fixed";
		regulator-name = "vph_pwr";
		regulator-always-on;
		regulator-boot-on;
	};

	usb3_vbus_reg: regulator-usb3-vbus {
		compatible = "regulator-fixed";
		regulator-name = "VBUS_5V";
		regulator-min-microvolt = <5000000>;
		regulator-max-microvolt = <5000000>;
		gpio = <&pms405_gpios 3 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&usb3_vbus_boost_pin>;
		enable-active-high;
		status = "disabled";
	};

	usb3_con: usb3-connector {
		compatible = "linux,extcon-usb-gpio";
		id-gpio = <&tlmm 116 GPIO_ACTIVE_HIGH>;
		vbus-gpio = <&pms405_gpios 12 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&usb3_id_pin>, <&usb3_vbus_pin>;

		port {
			ep_usb3_con: endpoint {
				remote-endpoint = <&ep_usb3_phy>;
			};
		};
	};

};

&CPU0 {
	cpu-supply = <&vsvs_apc_buck>;
};

&CPU1 {
	cpu-supply = <&vsvs_apc_buck>;
};

&CPU2 {
	cpu-supply = <&vsvs_apc_buck>;
};

&CPU3 {
	cpu-supply = <&vsvs_apc_buck>;
};

&cpu_opp_table {
	opp-1094400000 {
		opp-microvolt = <1225000 1225000 1225000>;
	};

	opp-1248000000 {
		opp-microvolt = <1287500 1287500 1287500>;
	};

	opp-1401600000 {
		opp-hz = /bits/ 64 <1401600000>;
		opp-microvolt = <1356250 1356250 1356250>;
	};
};

/* Add required gpio names to use gpio hog mechanism */
&tlmm {
	gpio-line-names = "", "", "", "", "", "", "", "", "", "",
	                  "", "", "", "", "", "", "", "", "", "",
	                  "", "", "", "", "", "", "", "", "", "",
	                  "", "", "", "", "", "", "", "", "", "",
	                  "", "", "", "", "", "", "", "", "", "",
	                  "", "", "", "", "AP_FLASH_WP_L", "", "", "", "", "",
	                  "", "", "", "", "", "", "", "", "", "",
	                  "", "", "", "", "", "", "", "RECOVERY_SW_L", "", "",
	                  "", "", "", "", "", "", "", "", "", "",
	                  "", "", "", "", "", "", "", "", "", "",
	                  "", "", "", "", "", "", "", "", "", "",
	                  "", "", "", "", "", "", "", "", "", "";
};

&ethernet {
	status = "ok";

	snps,reset-gpio = <&tlmm 60 GPIO_ACTIVE_LOW>;
	snps,reset-active-low;
	snps,reset-delays-us = <0 10000 10000>;

	pinctrl-names = "default";
	pinctrl-0 = <&ethernet_defaults>;

	fixed-link {
		speed = <1000>;
		full-duplex;
	};

	mdio {
		phy_port2: phy@1 {
			reg = <1>;
		};

		phy_port3: phy@2 {
			reg = <2>;
		};

		switch0@16 {
			compatible = "qca,qca8334";
			#address-cells = <1>;
			#size-cells = <0>;
			reg = <16>;

			ports {
				#address-cells = <1>;
				#size-cells = <0>;
				cpu_port1: port@0 {
					reg = <0>;
					label = "cpu";
					ethernet = <&ethernet>;
					phy-mode = "rgmii";
					rx-delay-disable;
					tx-delay-disable;
					fixed-link {
						speed = <1000>;
						full-duplex;
					};
				};

				port@2 {
					reg = <2>;
					label = "lan1";
					local-mac-address = [000000000000];
					cpu = <&cpu_port1>;
					phy-handle = <&phy_port2>;
				};

				port@3 {
					reg = <3>;
					label = "lan2";
					local-mac-address = [000000000000];
					cpu = <&cpu_port1>;
					phy-handle = <&phy_port3>;
				};
			};
		};
	};
};

&tlmm {
	ethernet_defaults: ethernet-defaults {
		int {
			pins = "gpio61";
			function = "rgmii_int";
			bias-disable;
			drive-strength = <2>;
		};
		mdc {
			pins = "gpio76";
			function = "rgmii_mdc";
			bias-pull-up;
		};
		mdio {
			pins = "gpio75";
			function = "rgmii_mdio";
			bias-pull-up;
		};
		tx {
			pins = "gpio67", "gpio66", "gpio65", "gpio64";
			function = "rgmii_tx";
			bias-pull-up;
			drive-strength = <16>;
		};
		rx {
			pins = "gpio73", "gpio72", "gpio71", "gpio70";
			function = "rgmii_rx";
			bias-disable;
			drive-strength = <2>;
		};
		tx-ctl {
			pins = "gpio68";
			function = "rgmii_ctl";
			bias-pull-up;
			drive-strength = <16>;
		};
		rx-ctl {
			pins = "gpio74";
			function = "rgmii_ctl";
			bias-disable;
			drive-strength = <2>;
		};
		tx-ck {
			pins = "gpio63";
			function = "rgmii_ck";
			bias-pull-up;
			drive-strength = <16>;
		};
		rx-ck {
			pins = "gpio69";
			function = "rgmii_ck";
			bias-disable;
			drive-strength = <2>;
		};
	};
};

&pms405_s3 {
	status = "disabled";
};

&rpm_requests {
	pms405-regulators {
		compatible = "qcom,rpm-pms405-regulators";

		vdd_s1-supply = <&vph_pwr>;
		vdd_s2-supply = <&vph_pwr>;
		vdd_s3-supply = <&pms405_s3>;
		vdd_s4-supply = <&vph_pwr>;
		vdd_s5-supply = <&vph_pwr>;
		vdd_l1_l2-supply = <&vreg_s5_1p35>;
		vdd_l3_l8-supply = <&vreg_s5_1p35>;
		vdd_l4-supply = <&vreg_s5_1p35>;
		vdd_l5_l6-supply = <&vreg_s4_1p8>;
		vdd_l7-supply = <&vph_pwr>;
		vdd_l9-supply = <&vreg_s5_1p35>;
		vdd_l10_l11_l12_l13-supply = <&vph_pwr>;

		vreg_s4_1p8: s4 {
			regulator-min-microvolt = <1728000>;
			regulator-max-microvolt = <1920000>;
		};

		vreg_s5_1p35: s5 {
			regulator-min-microvolt = <1352000>;
			regulator-max-microvolt = <1352000>;
			regulator-always-on;
		};

		vreg_l3_1p05: l3 {
			regulator-min-microvolt = <1050000>;
			regulator-max-microvolt = <1160000>;
			regulator-always-on;
		};

		vreg_l4_1p2: l4 {
			regulator-min-microvolt = <1144000>;
			regulator-max-microvolt = <1256000>;
			regulator-always-on;
		};

		vreg_l5_1p8: l5 {
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <1800000>;
			regulator-always-on;
		};

		vreg_l6_1p8: l6 {
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <1800000>;
			regulator-always-on;
		};

		vreg_l7_1p8: l7 {
			regulator-min-microvolt = <1616000>;
			regulator-max-microvolt = <3000000>;
			regulator-always-on;
		};

		vreg_l10_3p3: l10 {
			regulator-min-microvolt = <2936000>;
			regulator-max-microvolt = <3088000>;
			regulator-always-on;
		};

		vreg_l11_sdc2: l11 {
			regulator-min-microvolt = <2696000>;
			regulator-max-microvolt = <3304000>;
		};

		vreg_l12_3p3: l12 {
			regulator-min-microvolt = <2968000>;
			regulator-max-microvolt = <3300000>;
			regulator-always-on;
		};

		vreg_l13_3p3: l13 {
			regulator-min-microvolt = <3000000>;
			regulator-max-microvolt = <3300000>;
			regulator-always-on;
		};
	};
};

&sdcc1 {
	status = "ok";

	mmc-ddr-1_8v;
	mmc-hs200-1_8v;
	mmc-hs400-1_8v;
	mmc-hs400-enhanced-strobe;
	bus-width = <8>;
	non-removable;

	pinctrl-names = "default", "sleep";
	pinctrl-0 = <&sdc1_on>;
	pinctrl-1 = <&sdc1_off>;
};

&tlmm {
	blsp1_uart3_default: blsp1-uart3-default {
		pins = "gpio82", "gpio83", "gpio84", "gpio85";
		function = "blsp_uart3";
		drive-strength = <2>;
		bias-disable;
	};

	sdc1_on: sdc1-on {
		clk {
			pins = "sdc1_clk";
			bias-disable;
			drive-strength = <16>;
		};

		cmd {
			pins = "sdc1_cmd";
			bias-pull-up;
			drive-strength = <10>;
		};

		data {
			pins = "sdc1_data";
			bias-pull-up;
			dreive-strength = <10>;
		};

		rclk {
			pins = "sdc1_rclk";
			bias-pull-down;
		};
	};

	sdc1_off: sdc1-off {
		clk {
			pins = "sdc1_clk";
			bias-disable;
			drive-strength = <2>;
		};

		cmd {
			pins = "sdc1_cmd";
			bias-pull-up;
			drive-strength = <2>;
		};

		data {
			pins = "sdc1_data";
			bias-pull-up;
			dreive-strength = <2>;
		};

		rclk {
			pins = "sdc1_rclk";
			bias-pull-down;
		};
	};

	usb3_id_pin: usb3-id-pin {
		pinmux {
			pins = "gpio116";
			function = "gpio";
		};

		pinconf {
			pins = "gpio116";
			drive-strength = <2>;
			bias-pull-up;
			input-enable;
		};
	};

	h1_ap_int_odl: h1-ap-int-odl {
		pinmux {
			pins = "gpio53";
			function = "gpio";
			input-enable;
		};
		pinconf {
			pins = "gpio53";
			bias-pull-up;
		};
	};

	spi_flash_wp: spi-flash-wp {
		pins = "gpio54";
		function = "gpio";
		bias-disable;
		bias-no-pull;
		drive-strength = <2>;
		input-enable;
	};

	led_lp5562_enable: led-lp5562-enable {
		pins = "gpio119";
		function = "gpio";

		bias-pull-up;
		drive-strength = <2>;
		output-high;
	};
};

&pms405_gpios {
	usb3_vbus_boost_pin: usb3-vbus-boost-pin {
		pinconf {
			pins = "gpio3";
			function = PMIC_GPIO_FUNC_NORMAL;
			output-low;
			power-source = <1>;
		};
	};

	usb3_vbus_pin: usb3-vbus-pin {
		pinconf {
			pins = "gpio12";
			function = PMIC_GPIO_FUNC_NORMAL;
			input-enable;
			bias-pull-down;
			power-source = <1>;
		};
	};

	pms405_sleepclk2_pin: pms405_sleepclk2_pin {
		pinconf {
			pins = "gpio8";
			function = PMIC_GPIO_FUNC_FUNC1;
			qcom,drive-strength = <2>;
			power-source = <0>;
			bias-disable;
			output-low;
		};
	};

	apps_buck_en: apps-buck-en {
		pins = "gpio6";
		function = "normal";
		output-high;
	};

	apps_mode_sel: apps-mode-sel {
		pins = "gpio6";
		function = "normal";
		output-low;
	};
};

&usb2 {
	extcon = <&usb3_con>;
	status = "okay";

	dwc3@78c0000 {
		extcon = <&usb3_con>;
	};
};

&usb2_phy_prim {
	vdd-supply = <&vreg_l4_1p2>;
	vdda1p8-supply = <&vreg_l5_1p8>;
	vdda3p3-supply = <&vreg_l12_3p3>;
	qcom,vdd-voltage-level = <0 1144000 1200000>;
	status = "okay";
};

&usb2_phy_sec {
	vdd-supply = <&vreg_l4_1p2>;
	vdda1p8-supply = <&vreg_l5_1p8>;
	vdda3p3-supply = <&vreg_l12_3p3>;
	qcom,vdd-voltage-level = <0 1144000 1200000>;
	status = "disabled";
};

&usb3 {
	extcon = <&usb3_con>;
	status = "disabled";

	dwc3@7580000 {
		extcon = <&usb3_con>;
	};
};

&usb3_phy {
	vdd-supply = <&vreg_l3_1p05>;
	vdda1p8-supply = <&vreg_l5_1p8>;
	vbus-supply = <&usb3_vbus_reg>;
	qcom,vdd-voltage-level = <0 1050000 1050000>;
	status = "disabled";

	port {
		ep_usb3_phy: endpoint {
			remote-endpoint = <&ep_usb3_con>;
		};
	};
};

/* PINCTRL - additions to nodes defined in qcs404.dtsi */

&blsp1_uart2_default {
	rx {
		drive-strength = <2>;
		bias-disable;
	};

	tx {
		drive-strength = <2>;
		bias-disable;
	};
};

/* SPI Interfaces */

&blsp1_spi0 {
	status = "okay";
	pinctrl-0 = <&blsp1_spi0_default &pms405_sleepclk2_pin>;
	spidev@0 {
		compatible = "spidev";
		reg = <0>;
		spi-max-frequency = <25000000>;
	};
};

&blsp1_spi4 {
	status = "okay";
	spi-max-frequency = <100000>;
	cr50@0 {
		compatible = "google,cr50";
		reg = <0>;
		pinctrl-names = "default";
		pinctrl-0 = <&h1_ap_int_odl>;
		spi-max-frequency = <100000>;
		interrupt-parent = <&tlmm>;
		interrupts = <53 IRQ_TYPE_EDGE_RISING>;
	};
};

&blsp2_spi0 {
	status = "okay";
	pinctrl-0 = <&spi_flash_wp>, <&blsp2_spi0_default>;
	spidev@0 {
		compatible = "spidev";
		reg = <0>;
		spi-max-frequency = <25000000>;
	};
};

/* I2C Interfaces */

&blsp1_i2c1 {
	status = "okay";
	clock-frequency = <400000>;
	pinctrl-0 = <&led_lp5562_enable>, <&blsp1_i2c1_default>;
	lp5562@30 {
		compatible = "ti,lp5562";
		reg = <0x30>;
		clock-mode = [01];

		chan0 {
			chan-name = "LED0_Red";
			led-cur = [64];
			max-cur = [78];
		};

		chan1 {
			chan-name = "LED0_Green";
			led-cur = [64];
			max-cur = [78];
		};

		chan2 {
			chan-name = "LED0_Blue";
			led-cur = [64];
			max-cur = [78];
		};
	};

	vsvs_apc_buck: fan53526@60 {
		compatible = "fsc,fan53526";
		reg = <0x60>;

		regulator-min-microvolt = <1048000>;
		regulator-max-microvolt = <1356250>;
		regulator-ramp-delay = <500>;
		regulator-always-on;

		fcs,suspend-voltage-selector = <1>;

		pinctrl-names = "default";
		pinctrl-0 = <&apps_buck_en>, <&apps_mode_sel>;
	};
};

&blsp1_uart3 {
	pinctrl-names = "default";
	pinctrl-0 = <&blsp1_uart3_default>;
	status = "okay";

	bluetooth: wcn3990-bt {
		compatible = "qcom,wcn3990-bt";
		vddio-supply = <&vreg_l6_1p8>;
		vddxo-supply = <&vreg_l5_1p8>;
		//vddrf-supply = <&vreg_s5_1p35>;
		//vddch0-supply = <&pp3300_l25a_ch0_wcn3990>;
		max-speed = <3200000>;
	};
};
