%# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%from src import settings
%_root = settings.settings.relative_root

%def body_block():
  %# --------------------------------------------------------------------------
  %# Switcher bar for releases (milestones).
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base=view, title=title)

  %#-------------------------------------------------------------------------
  %# The table.
  %#-------------------------------------------------------------------------
  %_data = tpl_vars['data']
  %if not _data:
    <p> No hostnames specified </p>
  %else:
    %_machines = _data['machines']
    %_days_back = _data['days_back']
    %for (_machine_idx, _hostname), _records in _machines.iteritems():
      <h4>
        Top 10 flaky reasons for <strong> {{ _hostname }} </strong>
        %if _days_back:
          for the last {{ _days_back }} days
        %end
     </h4>
      %if _records:
        <table>
          <thead>
            <tr>
              <th class="headeritem centered"> Count </th>
              <th class="headeritem centered"> Reason </th>
            </tr>
          </thead>
          <tbody>
            %for _record in _records:
              <tr>
                <td class="lefted">
                  <a href="{{ _root }}/testrun/?test_ids={{ _record['test_ids'] }}">
                    {{ _record['count'] }}
                  </a>
                </td>
                <td class="lefted"> {{ _record['reason'] }} </td>
              </tr>
            %end
          </tbody>
        </table>
      %else:
        <p>no result found</p>
      %end
    %end
  %end
%end

%rebase('master.tpl', title=title, query_string=query_string, body_block=body_block)
