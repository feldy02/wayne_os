# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Request handlers for Whining Test Results Dashboard.

Modeled after the Django urls.py file but with the benefits of bottlepy.

Debugging Note:

   Viewing debugging information is not straightforward given print
   statements are no longer appropriate for this web app.  Instead,
   debug web pages should be used to convey debugging information.
   A common method for this follows:

    import pprint
    import whining_errors
    raise whining_errors.WhiningInputError(pprint.pformat(some_dict))
"""

import views


def add_routes(app, static_root):
    """Maps urls to view functions ala Django urls.py.

    Isolates url map for easy app interface inspection.
    """
    @app.route('/favicon.ico')
    def route_favicon():
        return views.static_file('favicon.ico', static_root)

    @app.route('/static/<filepath:path>')
    def route_static(filepath):
        return views.static_file(filepath, static_root)

    @app.route('/')
    @app.route('/help')
    def route_default():
        return views.render_help()

    @app.route('/<filter_tag>')
    @app.route('/<filter_tag>/')
    def route_dashboard(filter_tag=None):
        """Render an aggregate of summary + failures."""
        return views.render_dashboard(filter_tag)

    @app.route('/builds')
    @app.route('/builds/')
    def route_builds():
        """Render a view of the Chromium OS builds that ran tests."""
        return views.render_builds()

    @app.route('/comments')
    @app.route('/comments/')
    def route_comments():
        """Render a view of recent triage comment."""
        return views.render_comments()

    @app.route('/comments', method='POST')
    def route_post_comment():
        return views.post_comment()

    @app.route('/debug')
    def route_debug():
        return views.render_debug()

    @app.route('/summary')
    @app.route('/summary/')
    @app.route('/summary/<filter_tag>')
    @app.route('/summary/<filter_tag>/')
    def route_summary(filter_tag=None):
        """Render a summary view of recent failures aggregated by test."""
        return views.render_summary(filter_tag)

    @app.route('/filters')
    @app.route('/filters/')
    def route_filters():
        return views.render_filters()

    @app.route('/jobs/<filter_tag>')
    @app.route('/jobs/<filter_tag>/')
    def route_jobs(filter_tag=None):
        """Render a short view of jobs for debugging odd test counts."""
        return views.render_jobs(filter_tag)

    @app.route('/modems')
    @app.route('/modems/')
    def route_modems(filter_tag='lookups'):
        """Render the modems view."""
        return views.render_modems()

    @app.route('/platform/<filter_tag>')
    @app.route('/platform/<filter_tag>/')
    def route_platform(filter_tag=None):
        """Render a detailed view of recent platform tests (build vs test)."""
        return views.render_platform(filter_tag)

    @app.route('/platforms')
    @app.route('/platforms/')
    def route_platforms():
        """Render a list of the available platforms."""
        return views.render_platforms()

    @app.route('/releases')
    @app.route('/releases/')
    @app.route('/releases/<filter_tag>')
    @app.route('/releases/<filter_tag>/')
    def route_releases(filter_tag='lookups'):
        """Render a view of the Chromium OS releases that ran tests."""
        return views.render_releases(filter_tag)

    @app.route('/suites')
    @app.route('/suites/')
    def route_suites():
        """Render a list of the suites that ran."""
        return views.render_suites()

    @app.route('/suitestats')
    @app.route('/suitestats/')
    @app.route('/suitestats/<filter_tag>')
    @app.route('/suitestats/<filter_tag>/')
    def route_suite_stats(filter_tag='lookups'):
        """Render a list of suite stastics."""
        return views.render_suite_stats(filter_tag)

    @app.route('/matrix')
    @app.route('/matrix/')
    @app.route('/matrix/<filter_tag>')
    @app.route('/matrix/<filter_tag>/')
    def route_matrix(filter_tag=None):
        """Render a matrix view of recent tests (build vs platform)."""
        return views.render_matrix(filter_tag)

    @app.route('/tempfilter')
    @app.route('/tempfilter/')
    def route_temp_filter():
        return views.render_temp_filter()

    @app.route('/tests')
    @app.route('/tests/')
    def route_tests():
        """Render a list of the tests that ran."""
        return views.render_tests()

    @app.route('/testrun')
    @app.route('/testrun/')
    @app.route('/testrun/<filter_tag>')
    @app.route('/testrun/<filter_tag>/')
    def route_testrun(filter_tag='unfiltered'):
        """Render a view with full details of a single test run.

        This view does not use the filter_tag, but passes it on in links.
        """
        return views.render_testrun(filter_tag)

    @app.route('/failures')
    @app.route('/failures/')
    @app.route('/failures/<filter_tag>')
    @app.route('/failures/<filter_tag>/')
    def route_failures(filter_tag=None):
        """Render a detailed list of test failures."""
        return views.render_failures(filter_tag)

    @app.route('/testrun_log/<filter_tag>')
    def route_testrun_log(filter_tag=None):
        """Rounte to the test log url."""
        views.redirect_to_testrun_log(filter_tag)

    @app.route('/suitehealth')
    @app.route('/suitehealth/')
    def route_suitehealth():
        """A suite health matrix - failure rates per suite per platform."""
        return views.render_suitehealth()

    @app.route('/testhealth')
    @app.route('/testhealth/')
    @app.route('/testhealth/<filter_tag>')
    @app.route('/testhealth/<filter_tag>/')
    def route_testhealth(filter_tag='unfiltered'):
        """Render matrix with test failure rates per platform."""
        return views.render_testhealth(filter_tag)

    @app.route('/reasons')
    @app.route('/reasons/')
    def route_reasons():
        """Frequency sorted list of failure reasons for one test."""
        return views.render_reasons()

    @app.route('/retry_teststats')
    @app.route('/retry_teststats/')
    @app.route('/retry_teststats/<filter_tag>')
    @app.route('/retry_teststats/<filter_tag>/')
    def route_retrystats(filter_tag='unfiltered'):
        """Render matrix with retry pass rate per platform."""
        return views.render_retry_teststats(filter_tag)

    @app.route('/retry_hostinfo')
    @app.route('/retry_hostinfo/')
    @app.route('/retry_hostinfo/<filter_tag>')
    @app.route('/retry_hostinfo/<filter_tag>/')
    def route_retry_hostinfo(filter_tag='unfiltered'):
        """Render high frequent flaky reasons per host."""
        return views.render_retry_hostinfo(filter_tag)

    @app.route('/retry_hoststats')
    @app.route('/retry_hoststats/')
    @app.route('/retry_hoststats/<filter_tag>')
    @app.route('/retry_hoststats/<filter_tag>/')
    def route_retry_hoststats(filter_tag='unfiltered'):
        """Render matrix with host retry pass rate per platform."""
        return views.render_retry_hoststats(filter_tag)
