# Copyright (c) 2010 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Common constants for vm scripts.

# Default values for creating VM's.
DEFAULT_QEMU_IMAGE="chromiumos_qemu_image.bin"
