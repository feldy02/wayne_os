# Mask audit from portage-stable
sys-process/audit::portage-stable
sys-apps/shadow::portage-stable
# Mask audit from chromiumos-overlay
sys-process/audit::chromiumos
# Ensure that lakitu still uses current cloud-init version: 0.7.6
>=app-emulation/cloud-init-0.7.7
