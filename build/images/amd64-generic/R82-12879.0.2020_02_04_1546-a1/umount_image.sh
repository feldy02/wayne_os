#!/bin/bash -eu
# File automatically generated. Do not edit.

usage() {
  local ret=0
  if [[ $# -gt 0 ]]; then
    # Write to stderr on errors.
    exec 1>&2
    echo "ERROR: $*"
    echo
    ret=1
  fi
  echo "Usage: $0 [-h|--help] [--nolosetup] [image] [part]"
  echo "Example: $0 chromiumos_image.bin"
  exit ${ret}
}

USE_LOSETUP=yes
TARGET=""
PART=""
ARG_INDEX=0

while [[ $# -gt 0 ]]; do
  case "$1" in
    -h|--help)
      usage
      ;;
    --nolosetup)
      USE_LOSETUP=no
      shift
      ;;
    *)
      if [[ ${ARG_INDEX} -eq 0 ]]; then
        TARGET="${1}"
      elif [[ ${ARG_INDEX} -eq 1 ]]; then
        PART="${1}"
      else
        usage "too many arguments"
      fi
      ARG_INDEX=$((ARG_INDEX+1))
      shift
      ;;
  esac
done

case ${TARGET} in
"")
  for TARGET in chromiumos_{,base_}image.bin ""; do
    if [[ -e ${TARGET} ]]; then
      echo "autodetected image: ${TARGET}"
      break
    fi
  done
  if [[ -z ${TARGET} ]]; then
    usage "could not autodetect an image"
  fi
  ;;
*)
  if [[ ! -e ${TARGET} ]]; then
    usage "image does not exist: ${TARGET}"
  fi
esac

#        start        size    part  contents
#            0           1          PMBR (Boot GUID: 36088E80-CBCF-4141-94BF-79DE8A8BF0DB)
#            1           1          Pri GPT header
#            2          32          Pri GPT table
#      4907008     4194413       1  Label: "STATE"
#                                   Type: Linux data
#                                   UUID: 5613EF8A-A9D3-634E-87C4-8DDF11DBD80E
#        20480      131072       2  Label: "KERN-A"
#                                   Type: ChromeOS kernel
#                                   UUID: 633DA38F-9989-CF41-B957-486749E4328F
#                                   Attr: priority=15 tries=15 successful=0 
#       712704     4194304       3  Label: "ROOT-A"
#                                   Type: ChromeOS rootfs
#                                   UUID: FBBD0E96-5FA1-EC47-9614-4C374DF3E622
#       151552      131072       4  Label: "KERN-B"
#                                   Type: ChromeOS kernel
#                                   UUID: 2BFE30E7-7F69-7E4D-BA22-FD9262F04CCB
#                                   Attr: priority=0 tries=0 successful=0 
#       708608        4096       5  Label: "ROOT-B"
#                                   Type: ChromeOS rootfs
#                                   UUID: 85BB975A-35F7-5E42-AABA-CD26F67FC299
#        16448           1       6  Label: "KERN-C"
#                                   Type: ChromeOS kernel
#                                   UUID: 09510B2E-9BCE-DF46-896D-816C7BE2CF5D
#                                   Attr: priority=0 tries=0 successful=0 
#        16449           1       7  Label: "ROOT-C"
#                                   Type: ChromeOS rootfs
#                                   UUID: 623AD6E1-CE57-8340-A7A4-CB4BE1D7E041
#       282624       32768       8  Label: "OEM"
#                                   Type: Linux data
#                                   UUID: 00B6D463-B5BC-9C49-B776-7EB0ED05680D
#        16450           1       9  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: C0B645A0-E3EC-0F4D-83CE-6E109E04EC82
#        16451           1      10  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: 6DE7D69C-0EC5-D245-B5B6-942FDB7ECB0E
#           64       16384      11  Label: "RWFW"
#                                   Type: ChromeOS firmware
#                                   UUID: DA3C5D16-7101-7F4A-B5A5-63FCEA088225
#       446464      262144      12  Label: "EFI-SYSTEM"
#                                   Type: EFI System Partition
#                                   UUID: 36088E80-CBCF-4141-94BF-79DE8A8BF0DB
#                                   Attr: legacy_boot=1 
#      9101436          32          Sec GPT table
#      9101468           1          Sec GPT header
case ${PART:-1} in
1|"STATE")
if [[ -d dir_1 ]]; then
  (
  sudo umount dir_1 || :
  rmdir dir_1
  rm -f "dir_1_STATE"
  ) &
fi
esac
case ${PART:-2} in
2|"KERN-A")
if [[ -d dir_2 ]]; then
  (
  sudo umount dir_2 || :
  rmdir dir_2
  rm -f "dir_2_KERN-A"
  ) &
fi
esac
case ${PART:-3} in
3|"ROOT-A")
if [[ -d dir_3 ]]; then
  (
  sudo umount dir_3 || :
  rmdir dir_3
  rm -f "dir_3_ROOT-A"
  ) &
fi
esac
case ${PART:-4} in
4|"KERN-B")
if [[ -d dir_4 ]]; then
  (
  sudo umount dir_4 || :
  rmdir dir_4
  rm -f "dir_4_KERN-B"
  ) &
fi
esac
case ${PART:-5} in
5|"ROOT-B")
if [[ -d dir_5 ]]; then
  (
  sudo umount dir_5 || :
  rmdir dir_5
  rm -f "dir_5_ROOT-B"
  ) &
fi
esac
case ${PART:-6} in
6|"KERN-C")
esac
case ${PART:-7} in
7|"ROOT-C")
esac
case ${PART:-8} in
8|"OEM")
if [[ -d dir_8 ]]; then
  (
  sudo umount dir_8 || :
  rmdir dir_8
  rm -f "dir_8_OEM"
  ) &
fi
esac
case ${PART:-9} in
9|"reserved")
esac
case ${PART:-10} in
10|"reserved")
esac
case ${PART:-11} in
11|"RWFW")
if [[ -d dir_11 ]]; then
  (
  sudo umount dir_11 || :
  rmdir dir_11
  rm -f "dir_11_RWFW"
  ) &
fi
esac
case ${PART:-12} in
12|"EFI-SYSTEM")
if [[ -d dir_12 ]]; then
  (
  sudo umount dir_12 || :
  rmdir dir_12
  rm -f "dir_12_EFI-SYSTEM"
  ) &
fi
esac
wait
