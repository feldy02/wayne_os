#!/bin/bash -eu
# File automatically generated. Do not edit.

usage() {
  local ret=0
  if [[ $# -gt 0 ]]; then
    # Write to stderr on errors.
    exec 1>&2
    echo "ERROR: $*"
    echo
    ret=1
  fi
  echo "Usage: $0 [-h|--help] [--nolosetup] [image] [part]"
  echo "Example: $0 chromiumos_image.bin"
  exit ${ret}
}

USE_LOSETUP=yes
TARGET=""
PART=""
ARG_INDEX=0

while [[ $# -gt 0 ]]; do
  case "$1" in
    -h|--help)
      usage
      ;;
    --nolosetup)
      USE_LOSETUP=no
      shift
      ;;
    *)
      if [[ ${ARG_INDEX} -eq 0 ]]; then
        TARGET="${1}"
      elif [[ ${ARG_INDEX} -eq 1 ]]; then
        PART="${1}"
      else
        usage "too many arguments"
      fi
      ARG_INDEX=$((ARG_INDEX+1))
      shift
      ;;
  esac
done

case ${TARGET} in
"")
  for TARGET in chromiumos_{,base_}image.bin ""; do
    if [[ -e ${TARGET} ]]; then
      echo "autodetected image: ${TARGET}"
      break
    fi
  done
  if [[ -z ${TARGET} ]]; then
    usage "could not autodetect an image"
  fi
  ;;
*)
  if [[ ! -e ${TARGET} ]]; then
    usage "image does not exist: ${TARGET}"
  fi
esac

# Losetup has support for partitions, and offset= has issues.
# See crbug.com/954188
LOOPDEV=''
cleanup() {
  if [[ -n "${LOOPDEV}" ]]; then
    sudo losetup -d "${LOOPDEV}"
  fi
}
trap cleanup EXIT
if [[ "${USE_LOSETUP}" == yes ]]; then
  LOOPDEV=$(sudo losetup -P -f --show "${TARGET}") || exit 1
fi

#        start        size    part  contents
#            0           1          PMBR (Boot GUID: 36088E80-CBCF-4141-94BF-79DE8A8BF0DB)
#            1           1          Pri GPT header
#            2          32          Pri GPT table
#      4907008     4194413       1  Label: "STATE"
#                                   Type: Linux data
#                                   UUID: 5613EF8A-A9D3-634E-87C4-8DDF11DBD80E
#        20480      131072       2  Label: "KERN-A"
#                                   Type: ChromeOS kernel
#                                   UUID: 633DA38F-9989-CF41-B957-486749E4328F
#                                   Attr: priority=15 tries=15 successful=0 
#       712704     4194304       3  Label: "ROOT-A"
#                                   Type: ChromeOS rootfs
#                                   UUID: FBBD0E96-5FA1-EC47-9614-4C374DF3E622
#       151552      131072       4  Label: "KERN-B"
#                                   Type: ChromeOS kernel
#                                   UUID: 2BFE30E7-7F69-7E4D-BA22-FD9262F04CCB
#                                   Attr: priority=0 tries=0 successful=0 
#       708608        4096       5  Label: "ROOT-B"
#                                   Type: ChromeOS rootfs
#                                   UUID: 85BB975A-35F7-5E42-AABA-CD26F67FC299
#        16448           1       6  Label: "KERN-C"
#                                   Type: ChromeOS kernel
#                                   UUID: 09510B2E-9BCE-DF46-896D-816C7BE2CF5D
#                                   Attr: priority=0 tries=0 successful=0 
#        16449           1       7  Label: "ROOT-C"
#                                   Type: ChromeOS rootfs
#                                   UUID: 623AD6E1-CE57-8340-A7A4-CB4BE1D7E041
#       282624       32768       8  Label: "OEM"
#                                   Type: Linux data
#                                   UUID: 00B6D463-B5BC-9C49-B776-7EB0ED05680D
#        16450           1       9  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: C0B645A0-E3EC-0F4D-83CE-6E109E04EC82
#        16451           1      10  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: 6DE7D69C-0EC5-D245-B5B6-942FDB7ECB0E
#           64       16384      11  Label: "RWFW"
#                                   Type: ChromeOS firmware
#                                   UUID: DA3C5D16-7101-7F4A-B5A5-63FCEA088225
#       446464      262144      12  Label: "EFI-SYSTEM"
#                                   Type: EFI System Partition
#                                   UUID: 36088E80-CBCF-4141-94BF-79DE8A8BF0DB
#                                   Attr: legacy_boot=1 
#      9101436          32          Sec GPT table
#      9101468           1          Sec GPT header
case ${PART:-1} in
1|"STATE")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_1" of="${LOOPDEV}p1"
else
  dd if="part_1" of="${TARGET}" bs=512 count=4194413 seek=4907008 conv=notrunc
fi
esac
case ${PART:-2} in
2|"KERN-A")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_2" of="${LOOPDEV}p2"
else
  dd if="part_2" of="${TARGET}" bs=512 count=131072 seek=20480 conv=notrunc
fi
esac
case ${PART:-3} in
3|"ROOT-A")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_3" of="${LOOPDEV}p3"
else
  dd if="part_3" of="${TARGET}" bs=512 count=4194304 seek=712704 conv=notrunc
fi
esac
case ${PART:-4} in
4|"KERN-B")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_4" of="${LOOPDEV}p4"
else
  dd if="part_4" of="${TARGET}" bs=512 count=131072 seek=151552 conv=notrunc
fi
esac
case ${PART:-5} in
5|"ROOT-B")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_5" of="${LOOPDEV}p5"
else
  dd if="part_5" of="${TARGET}" bs=512 count=4096 seek=708608 conv=notrunc
fi
esac
case ${PART:-6} in
6|"KERN-C")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_6" of="${LOOPDEV}p6"
else
  dd if="part_6" of="${TARGET}" bs=512 count=1 seek=16448 conv=notrunc
fi
esac
case ${PART:-7} in
7|"ROOT-C")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_7" of="${LOOPDEV}p7"
else
  dd if="part_7" of="${TARGET}" bs=512 count=1 seek=16449 conv=notrunc
fi
esac
case ${PART:-8} in
8|"OEM")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_8" of="${LOOPDEV}p8"
else
  dd if="part_8" of="${TARGET}" bs=512 count=32768 seek=282624 conv=notrunc
fi
esac
case ${PART:-9} in
9|"reserved")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_9" of="${LOOPDEV}p9"
else
  dd if="part_9" of="${TARGET}" bs=512 count=1 seek=16450 conv=notrunc
fi
esac
case ${PART:-10} in
10|"reserved")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_10" of="${LOOPDEV}p10"
else
  dd if="part_10" of="${TARGET}" bs=512 count=1 seek=16451 conv=notrunc
fi
esac
case ${PART:-11} in
11|"RWFW")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_11" of="${LOOPDEV}p11"
else
  dd if="part_11" of="${TARGET}" bs=512 count=16384 seek=64 conv=notrunc
fi
esac
case ${PART:-12} in
12|"EFI-SYSTEM")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_12" of="${LOOPDEV}p12"
else
  dd if="part_12" of="${TARGET}" bs=512 count=262144 seek=446464 conv=notrunc
fi
esac
