#!/bin/bash -eu
# File automatically generated. Do not edit.

usage() {
  local ret=0
  if [[ $# -gt 0 ]]; then
    # Write to stderr on errors.
    exec 1>&2
    echo "ERROR: $*"
    echo
    ret=1
  fi
  echo "Usage: $0 [-h|--help] [--nolosetup] [image] [part]"
  echo "Example: $0 chromiumos_image.bin"
  exit ${ret}
}

USE_LOSETUP=yes
TARGET=""
PART=""
ARG_INDEX=0

while [[ $# -gt 0 ]]; do
  case "$1" in
    -h|--help)
      usage
      ;;
    --nolosetup)
      USE_LOSETUP=no
      shift
      ;;
    *)
      if [[ ${ARG_INDEX} -eq 0 ]]; then
        TARGET="${1}"
      elif [[ ${ARG_INDEX} -eq 1 ]]; then
        PART="${1}"
      else
        usage "too many arguments"
      fi
      ARG_INDEX=$((ARG_INDEX+1))
      shift
      ;;
  esac
done

case ${TARGET} in
"")
  for TARGET in chromiumos_{,base_}image.bin ""; do
    if [[ -e ${TARGET} ]]; then
      echo "autodetected image: ${TARGET}"
      break
    fi
  done
  if [[ -z ${TARGET} ]]; then
    usage "could not autodetect an image"
  fi
  ;;
*)
  if [[ ! -e ${TARGET} ]]; then
    usage "image does not exist: ${TARGET}"
  fi
esac

# Losetup has support for partitions, and offset= has issues.
# See crbug.com/954188
LOOPDEV=''
cleanup() {
  if [[ -n "${LOOPDEV}" ]]; then
    sudo losetup -d "${LOOPDEV}"
  fi
}
trap cleanup EXIT
if [[ "${USE_LOSETUP}" == yes ]]; then
  LOOPDEV=$(sudo losetup -P -f --show "${TARGET}") || exit 1
fi

#        start        size    part  contents
#            0           1          PMBR (Boot GUID: 0CC1C893-D3A5-F643-97A6-1C8E9A9268D6)
#            1           1          Pri GPT header
#            2          32          Pri GPT table
#      4907008     6291565       1  Label: "STATE"
#                                   Type: Linux data
#                                   UUID: 4759E094-15C2-394B-88DA-C78126966839
#        20480      131072       2  Label: "KERN-A"
#                                   Type: ChromeOS kernel
#                                   UUID: 5E902804-FE5A-0C4A-A394-5DA3DB5126B4
#                                   Attr: priority=15 tries=15 successful=0 
#       712704     4194304       3  Label: "ROOT-A"
#                                   Type: ChromeOS rootfs
#                                   UUID: C7EF1AF4-A108-AB4F-A610-DC547EDB3B92
#       151552      131072       4  Label: "KERN-B"
#                                   Type: ChromeOS kernel
#                                   UUID: 0E66C800-F994-D947-AE3C-8B4C8364E0D7
#                                   Attr: priority=0 tries=0 successful=0 
#       708608        4096       5  Label: "ROOT-B"
#                                   Type: ChromeOS rootfs
#                                   UUID: 948E1BBE-CEAC-A847-B993-190452577F5D
#        16448           1       6  Label: "KERN-C"
#                                   Type: ChromeOS kernel
#                                   UUID: 2E089774-1976-7A40-86DA-122B6C723001
#                                   Attr: priority=0 tries=0 successful=0 
#        16449           1       7  Label: "ROOT-C"
#                                   Type: ChromeOS rootfs
#                                   UUID: A5862896-B886-604F-95D4-923B6FD98F4D
#       282624       32768       8  Label: "OEM"
#                                   Type: Linux data
#                                   UUID: 934377BF-8E39-2743-A060-D5EB7068794F
#        16450           1       9  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: EE9AFEE8-64E2-674C-9C29-FD24593EF88E
#        16451           1      10  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: 62CFB794-6877-E146-917A-6B49A3F41987
#           64       16384      11  Label: "RWFW"
#                                   Type: ChromeOS firmware
#                                   UUID: 2119F357-4B53-0B4A-AA2D-96463BE2F89C
#       446464      262144      12  Label: "EFI-SYSTEM"
#                                   Type: EFI System Partition
#                                   UUID: 0CC1C893-D3A5-F643-97A6-1C8E9A9268D6
#                                   Attr: legacy_boot=1 
#     11198588          32          Sec GPT table
#     11198620           1          Sec GPT header
case ${PART:-1} in
1|"STATE")
(
mkdir -p "dir_1"
m=( sudo mount "${LOOPDEV}p1" "dir_1" )
if ! "${m[@]}"; then
  if ! "${m[@]}" -o ro; then
    rmdir dir_1
    exit 0
  fi
fi
ln -sfT dir_1 "dir_1_STATE"
) &
esac
case ${PART:-2} in
2|"KERN-A")
(
mkdir -p "dir_2"
m=( sudo mount "${LOOPDEV}p2" "dir_2" )
if ! "${m[@]}"; then
  if ! "${m[@]}" -o ro; then
    rmdir dir_2
    exit 0
  fi
fi
ln -sfT dir_2 "dir_2_KERN-A"
) &
esac
case ${PART:-3} in
3|"ROOT-A")
(
mkdir -p "dir_3"
m=( sudo mount "${LOOPDEV}p3" "dir_3" )
if ! "${m[@]}"; then
  if ! "${m[@]}" -o ro; then
    rmdir dir_3
    exit 0
  fi
fi
ln -sfT dir_3 "dir_3_ROOT-A"
) &
esac
case ${PART:-4} in
4|"KERN-B")
(
mkdir -p "dir_4"
m=( sudo mount "${LOOPDEV}p4" "dir_4" )
if ! "${m[@]}"; then
  if ! "${m[@]}" -o ro; then
    rmdir dir_4
    exit 0
  fi
fi
ln -sfT dir_4 "dir_4_KERN-B"
) &
esac
case ${PART:-5} in
5|"ROOT-B")
(
mkdir -p "dir_5"
m=( sudo mount "${LOOPDEV}p5" "dir_5" )
if ! "${m[@]}"; then
  if ! "${m[@]}" -o ro; then
    rmdir dir_5
    exit 0
  fi
fi
ln -sfT dir_5 "dir_5_ROOT-B"
) &
esac
case ${PART:-6} in
6|"KERN-C")
esac
case ${PART:-7} in
7|"ROOT-C")
esac
case ${PART:-8} in
8|"OEM")
(
mkdir -p "dir_8"
m=( sudo mount "${LOOPDEV}p8" "dir_8" )
if ! "${m[@]}"; then
  if ! "${m[@]}" -o ro; then
    rmdir dir_8
    exit 0
  fi
fi
ln -sfT dir_8 "dir_8_OEM"
) &
esac
case ${PART:-9} in
9|"reserved")
esac
case ${PART:-10} in
10|"reserved")
esac
case ${PART:-11} in
11|"RWFW")
(
mkdir -p "dir_11"
m=( sudo mount "${LOOPDEV}p11" "dir_11" )
if ! "${m[@]}"; then
  if ! "${m[@]}" -o ro; then
    rmdir dir_11
    exit 0
  fi
fi
ln -sfT dir_11 "dir_11_RWFW"
) &
esac
case ${PART:-12} in
12|"EFI-SYSTEM")
(
mkdir -p "dir_12"
m=( sudo mount "${LOOPDEV}p12" "dir_12" )
if ! "${m[@]}"; then
  if ! "${m[@]}" -o ro; then
    rmdir dir_12
    exit 0
  fi
fi
ln -sfT dir_12 "dir_12_EFI-SYSTEM"
) &
esac
wait
