#!/bin/bash -eu
# File automatically generated. Do not edit.

usage() {
  local ret=0
  if [[ $# -gt 0 ]]; then
    # Write to stderr on errors.
    exec 1>&2
    echo "ERROR: $*"
    echo
    ret=1
  fi
  echo "Usage: $0 [-h|--help] [--nolosetup] [image] [part]"
  echo "Example: $0 chromiumos_image.bin"
  exit ${ret}
}

USE_LOSETUP=yes
TARGET=""
PART=""
ARG_INDEX=0

while [[ $# -gt 0 ]]; do
  case "$1" in
    -h|--help)
      usage
      ;;
    --nolosetup)
      USE_LOSETUP=no
      shift
      ;;
    *)
      if [[ ${ARG_INDEX} -eq 0 ]]; then
        TARGET="${1}"
      elif [[ ${ARG_INDEX} -eq 1 ]]; then
        PART="${1}"
      else
        usage "too many arguments"
      fi
      ARG_INDEX=$((ARG_INDEX+1))
      shift
      ;;
  esac
done

case ${TARGET} in
"")
  for TARGET in chromiumos_{,base_}image.bin ""; do
    if [[ -e ${TARGET} ]]; then
      echo "autodetected image: ${TARGET}"
      break
    fi
  done
  if [[ -z ${TARGET} ]]; then
    usage "could not autodetect an image"
  fi
  ;;
*)
  if [[ ! -e ${TARGET} ]]; then
    usage "image does not exist: ${TARGET}"
  fi
esac

# Losetup has support for partitions, and offset= has issues.
# See crbug.com/954188
LOOPDEV=''
cleanup() {
  if [[ -n "${LOOPDEV}" ]]; then
    sudo losetup -d "${LOOPDEV}"
  fi
}
trap cleanup EXIT
if [[ "${USE_LOSETUP}" == yes ]]; then
  LOOPDEV=$(sudo losetup -P -f --show "${TARGET}") || exit 1
fi

#        start        size    part  contents
#            0           1          PMBR (Boot GUID: 0CC1C893-D3A5-F643-97A6-1C8E9A9268D6)
#            1           1          Pri GPT header
#            2          32          Pri GPT table
#      4907008     6291565       1  Label: "STATE"
#                                   Type: Linux data
#                                   UUID: 4759E094-15C2-394B-88DA-C78126966839
#        20480      131072       2  Label: "KERN-A"
#                                   Type: ChromeOS kernel
#                                   UUID: 5E902804-FE5A-0C4A-A394-5DA3DB5126B4
#                                   Attr: priority=15 tries=15 successful=0 
#       712704     4194304       3  Label: "ROOT-A"
#                                   Type: ChromeOS rootfs
#                                   UUID: C7EF1AF4-A108-AB4F-A610-DC547EDB3B92
#       151552      131072       4  Label: "KERN-B"
#                                   Type: ChromeOS kernel
#                                   UUID: 0E66C800-F994-D947-AE3C-8B4C8364E0D7
#                                   Attr: priority=0 tries=0 successful=0 
#       708608        4096       5  Label: "ROOT-B"
#                                   Type: ChromeOS rootfs
#                                   UUID: 948E1BBE-CEAC-A847-B993-190452577F5D
#        16448           1       6  Label: "KERN-C"
#                                   Type: ChromeOS kernel
#                                   UUID: 2E089774-1976-7A40-86DA-122B6C723001
#                                   Attr: priority=0 tries=0 successful=0 
#        16449           1       7  Label: "ROOT-C"
#                                   Type: ChromeOS rootfs
#                                   UUID: A5862896-B886-604F-95D4-923B6FD98F4D
#       282624       32768       8  Label: "OEM"
#                                   Type: Linux data
#                                   UUID: 934377BF-8E39-2743-A060-D5EB7068794F
#        16450           1       9  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: EE9AFEE8-64E2-674C-9C29-FD24593EF88E
#        16451           1      10  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: 62CFB794-6877-E146-917A-6B49A3F41987
#           64       16384      11  Label: "RWFW"
#                                   Type: ChromeOS firmware
#                                   UUID: 2119F357-4B53-0B4A-AA2D-96463BE2F89C
#       446464      262144      12  Label: "EFI-SYSTEM"
#                                   Type: EFI System Partition
#                                   UUID: 0CC1C893-D3A5-F643-97A6-1C8E9A9268D6
#                                   Attr: legacy_boot=1 
#     11198588          32          Sec GPT table
#     11198620           1          Sec GPT header
case ${PART:-1} in
1|"STATE")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p1" of="part_1"
else
  dd if="${TARGET}" of="part_1" bs=512 count=6291565 skip=4907008
fi
ln -sfT part_1 "part_1_STATE"
esac
case ${PART:-2} in
2|"KERN-A")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p2" of="part_2"
else
  dd if="${TARGET}" of="part_2" bs=512 count=131072 skip=20480
fi
ln -sfT part_2 "part_2_KERN-A"
esac
case ${PART:-3} in
3|"ROOT-A")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p3" of="part_3"
else
  dd if="${TARGET}" of="part_3" bs=512 count=4194304 skip=712704
fi
ln -sfT part_3 "part_3_ROOT-A"
esac
case ${PART:-4} in
4|"KERN-B")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p4" of="part_4"
else
  dd if="${TARGET}" of="part_4" bs=512 count=131072 skip=151552
fi
ln -sfT part_4 "part_4_KERN-B"
esac
case ${PART:-5} in
5|"ROOT-B")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p5" of="part_5"
else
  dd if="${TARGET}" of="part_5" bs=512 count=4096 skip=708608
fi
ln -sfT part_5 "part_5_ROOT-B"
esac
case ${PART:-6} in
6|"KERN-C")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p6" of="part_6"
else
  dd if="${TARGET}" of="part_6" bs=512 count=1 skip=16448
fi
ln -sfT part_6 "part_6_KERN-C"
esac
case ${PART:-7} in
7|"ROOT-C")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p7" of="part_7"
else
  dd if="${TARGET}" of="part_7" bs=512 count=1 skip=16449
fi
ln -sfT part_7 "part_7_ROOT-C"
esac
case ${PART:-8} in
8|"OEM")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p8" of="part_8"
else
  dd if="${TARGET}" of="part_8" bs=512 count=32768 skip=282624
fi
ln -sfT part_8 "part_8_OEM"
esac
case ${PART:-9} in
9|"reserved")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p9" of="part_9"
else
  dd if="${TARGET}" of="part_9" bs=512 count=1 skip=16450
fi
ln -sfT part_9 "part_9_reserved"
esac
case ${PART:-10} in
10|"reserved")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p10" of="part_10"
else
  dd if="${TARGET}" of="part_10" bs=512 count=1 skip=16451
fi
ln -sfT part_10 "part_10_reserved"
esac
case ${PART:-11} in
11|"RWFW")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p11" of="part_11"
else
  dd if="${TARGET}" of="part_11" bs=512 count=16384 skip=64
fi
ln -sfT part_11 "part_11_RWFW"
esac
case ${PART:-12} in
12|"EFI-SYSTEM")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p12" of="part_12"
else
  dd if="${TARGET}" of="part_12" bs=512 count=262144 skip=446464
fi
ln -sfT part_12 "part_12_EFI-SYSTEM"
esac
