#!/bin/bash -eu
# File automatically generated. Do not edit.

usage() {
  local ret=0
  if [[ $# -gt 0 ]]; then
    # Write to stderr on errors.
    exec 1>&2
    echo "ERROR: $*"
    echo
    ret=1
  fi
  echo "Usage: $0 [-h|--help] [--nolosetup] [image] [part]"
  echo "Example: $0 chromiumos_image.bin"
  exit ${ret}
}

USE_LOSETUP=yes
TARGET=""
PART=""
ARG_INDEX=0

while [[ $# -gt 0 ]]; do
  case "$1" in
    -h|--help)
      usage
      ;;
    --nolosetup)
      USE_LOSETUP=no
      shift
      ;;
    *)
      if [[ ${ARG_INDEX} -eq 0 ]]; then
        TARGET="${1}"
      elif [[ ${ARG_INDEX} -eq 1 ]]; then
        PART="${1}"
      else
        usage "too many arguments"
      fi
      ARG_INDEX=$((ARG_INDEX+1))
      shift
      ;;
  esac
done

case ${TARGET} in
"")
  for TARGET in chromiumos_{,base_}image.bin ""; do
    if [[ -e ${TARGET} ]]; then
      echo "autodetected image: ${TARGET}"
      break
    fi
  done
  if [[ -z ${TARGET} ]]; then
    usage "could not autodetect an image"
  fi
  ;;
*)
  if [[ ! -e ${TARGET} ]]; then
    usage "image does not exist: ${TARGET}"
  fi
esac

# Losetup has support for partitions, and offset= has issues.
# See crbug.com/954188
LOOPDEV=''
cleanup() {
  if [[ -n "${LOOPDEV}" ]]; then
    sudo losetup -d "${LOOPDEV}"
  fi
}
trap cleanup EXIT
if [[ "${USE_LOSETUP}" == yes ]]; then
  LOOPDEV=$(sudo losetup -P -f --show "${TARGET}") || exit 1
fi

#        start        size    part  contents
#            0           1          PMBR (Boot GUID: D95DAB41-45AA-B848-9578-CDD7FA95AD4F)
#            1           1          Pri GPT header
#            2          32          Pri GPT table
#      4907008     4194413       1  Label: "STATE"
#                                   Type: Linux data
#                                   UUID: 45FDF2AF-EE28-EE43-8AE0-078A315FBA06
#        20480      131072       2  Label: "KERN-A"
#                                   Type: ChromeOS kernel
#                                   UUID: DE7BDAF6-9615-0441-8E85-3E8128D22ED8
#                                   Attr: priority=15 tries=15 successful=0 
#       712704     4194304       3  Label: "ROOT-A"
#                                   Type: ChromeOS rootfs
#                                   UUID: 73BCCDCD-00C0-4146-92A8-CB57C1B27C16
#       151552      131072       4  Label: "KERN-B"
#                                   Type: ChromeOS kernel
#                                   UUID: FA49597C-B237-5A49-9FDA-E3569ED36C3A
#                                   Attr: priority=0 tries=0 successful=0 
#       708608        4096       5  Label: "ROOT-B"
#                                   Type: ChromeOS rootfs
#                                   UUID: 698E752A-9D16-D549-A12E-881D0B6986FB
#        16448           1       6  Label: "KERN-C"
#                                   Type: ChromeOS kernel
#                                   UUID: 31BFC1DF-C822-AD4A-A701-51C906584D42
#                                   Attr: priority=0 tries=0 successful=0 
#        16449           1       7  Label: "ROOT-C"
#                                   Type: ChromeOS rootfs
#                                   UUID: A4B74A4B-6D6E-7448-A9B8-BEBC96DAB7F4
#       282624       32768       8  Label: "OEM"
#                                   Type: Linux data
#                                   UUID: 81CFD5EC-A68A-2C4D-A59C-60B143299948
#        16450           1       9  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: A2016B48-0D15-FB47-9D54-97457BCCB381
#        16451           1      10  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: A1FD09A5-F55F-BF45-89DC-75E581EF60C4
#           64       16384      11  Label: "RWFW"
#                                   Type: ChromeOS firmware
#                                   UUID: 6779E639-B04C-EA46-A554-7D28E4C6C14B
#       446464      262144      12  Label: "EFI-SYSTEM"
#                                   Type: EFI System Partition
#                                   UUID: D95DAB41-45AA-B848-9578-CDD7FA95AD4F
#                                   Attr: legacy_boot=1 
#      9101436          32          Sec GPT table
#      9101468           1          Sec GPT header
case ${PART:-1} in
1|"STATE")
(
mkdir -p "dir_1"
m=( sudo mount "${LOOPDEV}p1" "dir_1" )
if ! "${m[@]}"; then
  if ! "${m[@]}" -o ro; then
    rmdir dir_1
    exit 0
  fi
fi
ln -sfT dir_1 "dir_1_STATE"
) &
esac
case ${PART:-2} in
2|"KERN-A")
(
mkdir -p "dir_2"
m=( sudo mount "${LOOPDEV}p2" "dir_2" )
if ! "${m[@]}"; then
  if ! "${m[@]}" -o ro; then
    rmdir dir_2
    exit 0
  fi
fi
ln -sfT dir_2 "dir_2_KERN-A"
) &
esac
case ${PART:-3} in
3|"ROOT-A")
(
mkdir -p "dir_3"
m=( sudo mount "${LOOPDEV}p3" "dir_3" )
if ! "${m[@]}"; then
  if ! "${m[@]}" -o ro; then
    rmdir dir_3
    exit 0
  fi
fi
ln -sfT dir_3 "dir_3_ROOT-A"
) &
esac
case ${PART:-4} in
4|"KERN-B")
(
mkdir -p "dir_4"
m=( sudo mount "${LOOPDEV}p4" "dir_4" )
if ! "${m[@]}"; then
  if ! "${m[@]}" -o ro; then
    rmdir dir_4
    exit 0
  fi
fi
ln -sfT dir_4 "dir_4_KERN-B"
) &
esac
case ${PART:-5} in
5|"ROOT-B")
(
mkdir -p "dir_5"
m=( sudo mount "${LOOPDEV}p5" "dir_5" )
if ! "${m[@]}"; then
  if ! "${m[@]}" -o ro; then
    rmdir dir_5
    exit 0
  fi
fi
ln -sfT dir_5 "dir_5_ROOT-B"
) &
esac
case ${PART:-6} in
6|"KERN-C")
esac
case ${PART:-7} in
7|"ROOT-C")
esac
case ${PART:-8} in
8|"OEM")
(
mkdir -p "dir_8"
m=( sudo mount "${LOOPDEV}p8" "dir_8" )
if ! "${m[@]}"; then
  if ! "${m[@]}" -o ro; then
    rmdir dir_8
    exit 0
  fi
fi
ln -sfT dir_8 "dir_8_OEM"
) &
esac
case ${PART:-9} in
9|"reserved")
esac
case ${PART:-10} in
10|"reserved")
esac
case ${PART:-11} in
11|"RWFW")
(
mkdir -p "dir_11"
m=( sudo mount "${LOOPDEV}p11" "dir_11" )
if ! "${m[@]}"; then
  if ! "${m[@]}" -o ro; then
    rmdir dir_11
    exit 0
  fi
fi
ln -sfT dir_11 "dir_11_RWFW"
) &
esac
case ${PART:-12} in
12|"EFI-SYSTEM")
(
mkdir -p "dir_12"
m=( sudo mount "${LOOPDEV}p12" "dir_12" )
if ! "${m[@]}"; then
  if ! "${m[@]}" -o ro; then
    rmdir dir_12
    exit 0
  fi
fi
ln -sfT dir_12 "dir_12_EFI-SYSTEM"
) &
esac
wait
