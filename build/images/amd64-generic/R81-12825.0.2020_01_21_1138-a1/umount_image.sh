#!/bin/bash -eu
# File automatically generated. Do not edit.

usage() {
  local ret=0
  if [[ $# -gt 0 ]]; then
    # Write to stderr on errors.
    exec 1>&2
    echo "ERROR: $*"
    echo
    ret=1
  fi
  echo "Usage: $0 [-h|--help] [--nolosetup] [image] [part]"
  echo "Example: $0 chromiumos_image.bin"
  exit ${ret}
}

USE_LOSETUP=yes
TARGET=""
PART=""
ARG_INDEX=0

while [[ $# -gt 0 ]]; do
  case "$1" in
    -h|--help)
      usage
      ;;
    --nolosetup)
      USE_LOSETUP=no
      shift
      ;;
    *)
      if [[ ${ARG_INDEX} -eq 0 ]]; then
        TARGET="${1}"
      elif [[ ${ARG_INDEX} -eq 1 ]]; then
        PART="${1}"
      else
        usage "too many arguments"
      fi
      ARG_INDEX=$((ARG_INDEX+1))
      shift
      ;;
  esac
done

case ${TARGET} in
"")
  for TARGET in chromiumos_{,base_}image.bin ""; do
    if [[ -e ${TARGET} ]]; then
      echo "autodetected image: ${TARGET}"
      break
    fi
  done
  if [[ -z ${TARGET} ]]; then
    usage "could not autodetect an image"
  fi
  ;;
*)
  if [[ ! -e ${TARGET} ]]; then
    usage "image does not exist: ${TARGET}"
  fi
esac

#        start        size    part  contents
#            0           1          PMBR (Boot GUID: D95DAB41-45AA-B848-9578-CDD7FA95AD4F)
#            1           1          Pri GPT header
#            2          32          Pri GPT table
#      4907008     4194413       1  Label: "STATE"
#                                   Type: Linux data
#                                   UUID: 45FDF2AF-EE28-EE43-8AE0-078A315FBA06
#        20480      131072       2  Label: "KERN-A"
#                                   Type: ChromeOS kernel
#                                   UUID: DE7BDAF6-9615-0441-8E85-3E8128D22ED8
#                                   Attr: priority=15 tries=15 successful=0 
#       712704     4194304       3  Label: "ROOT-A"
#                                   Type: ChromeOS rootfs
#                                   UUID: 73BCCDCD-00C0-4146-92A8-CB57C1B27C16
#       151552      131072       4  Label: "KERN-B"
#                                   Type: ChromeOS kernel
#                                   UUID: FA49597C-B237-5A49-9FDA-E3569ED36C3A
#                                   Attr: priority=0 tries=0 successful=0 
#       708608        4096       5  Label: "ROOT-B"
#                                   Type: ChromeOS rootfs
#                                   UUID: 698E752A-9D16-D549-A12E-881D0B6986FB
#        16448           1       6  Label: "KERN-C"
#                                   Type: ChromeOS kernel
#                                   UUID: 31BFC1DF-C822-AD4A-A701-51C906584D42
#                                   Attr: priority=0 tries=0 successful=0 
#        16449           1       7  Label: "ROOT-C"
#                                   Type: ChromeOS rootfs
#                                   UUID: A4B74A4B-6D6E-7448-A9B8-BEBC96DAB7F4
#       282624       32768       8  Label: "OEM"
#                                   Type: Linux data
#                                   UUID: 81CFD5EC-A68A-2C4D-A59C-60B143299948
#        16450           1       9  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: A2016B48-0D15-FB47-9D54-97457BCCB381
#        16451           1      10  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: A1FD09A5-F55F-BF45-89DC-75E581EF60C4
#           64       16384      11  Label: "RWFW"
#                                   Type: ChromeOS firmware
#                                   UUID: 6779E639-B04C-EA46-A554-7D28E4C6C14B
#       446464      262144      12  Label: "EFI-SYSTEM"
#                                   Type: EFI System Partition
#                                   UUID: D95DAB41-45AA-B848-9578-CDD7FA95AD4F
#                                   Attr: legacy_boot=1 
#      9101436          32          Sec GPT table
#      9101468           1          Sec GPT header
case ${PART:-1} in
1|"STATE")
if [[ -d dir_1 ]]; then
  (
  sudo umount dir_1 || :
  rmdir dir_1
  rm -f "dir_1_STATE"
  ) &
fi
esac
case ${PART:-2} in
2|"KERN-A")
if [[ -d dir_2 ]]; then
  (
  sudo umount dir_2 || :
  rmdir dir_2
  rm -f "dir_2_KERN-A"
  ) &
fi
esac
case ${PART:-3} in
3|"ROOT-A")
if [[ -d dir_3 ]]; then
  (
  sudo umount dir_3 || :
  rmdir dir_3
  rm -f "dir_3_ROOT-A"
  ) &
fi
esac
case ${PART:-4} in
4|"KERN-B")
if [[ -d dir_4 ]]; then
  (
  sudo umount dir_4 || :
  rmdir dir_4
  rm -f "dir_4_KERN-B"
  ) &
fi
esac
case ${PART:-5} in
5|"ROOT-B")
if [[ -d dir_5 ]]; then
  (
  sudo umount dir_5 || :
  rmdir dir_5
  rm -f "dir_5_ROOT-B"
  ) &
fi
esac
case ${PART:-6} in
6|"KERN-C")
esac
case ${PART:-7} in
7|"ROOT-C")
esac
case ${PART:-8} in
8|"OEM")
if [[ -d dir_8 ]]; then
  (
  sudo umount dir_8 || :
  rmdir dir_8
  rm -f "dir_8_OEM"
  ) &
fi
esac
case ${PART:-9} in
9|"reserved")
esac
case ${PART:-10} in
10|"reserved")
esac
case ${PART:-11} in
11|"RWFW")
if [[ -d dir_11 ]]; then
  (
  sudo umount dir_11 || :
  rmdir dir_11
  rm -f "dir_11_RWFW"
  ) &
fi
esac
case ${PART:-12} in
12|"EFI-SYSTEM")
if [[ -d dir_12 ]]; then
  (
  sudo umount dir_12 || :
  rmdir dir_12
  rm -f "dir_12_EFI-SYSTEM"
  ) &
fi
esac
wait
