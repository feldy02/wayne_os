#!/bin/bash -eu
# File automatically generated. Do not edit.

usage() {
  local ret=0
  if [[ $# -gt 0 ]]; then
    # Write to stderr on errors.
    exec 1>&2
    echo "ERROR: $*"
    echo
    ret=1
  fi
  echo "Usage: $0 [-h|--help] [--nolosetup] [image] [part]"
  echo "Example: $0 chromiumos_image.bin"
  exit ${ret}
}

USE_LOSETUP=yes
TARGET=""
PART=""
ARG_INDEX=0

while [[ $# -gt 0 ]]; do
  case "$1" in
    -h|--help)
      usage
      ;;
    --nolosetup)
      USE_LOSETUP=no
      shift
      ;;
    *)
      if [[ ${ARG_INDEX} -eq 0 ]]; then
        TARGET="${1}"
      elif [[ ${ARG_INDEX} -eq 1 ]]; then
        PART="${1}"
      else
        usage "too many arguments"
      fi
      ARG_INDEX=$((ARG_INDEX+1))
      shift
      ;;
  esac
done

case ${TARGET} in
"")
  for TARGET in chromiumos_{,base_}image.bin ""; do
    if [[ -e ${TARGET} ]]; then
      echo "autodetected image: ${TARGET}"
      break
    fi
  done
  if [[ -z ${TARGET} ]]; then
    usage "could not autodetect an image"
  fi
  ;;
*)
  if [[ ! -e ${TARGET} ]]; then
    usage "image does not exist: ${TARGET}"
  fi
esac

# Losetup has support for partitions, and offset= has issues.
# See crbug.com/954188
LOOPDEV=''
cleanup() {
  if [[ -n "${LOOPDEV}" ]]; then
    sudo losetup -d "${LOOPDEV}"
  fi
}
trap cleanup EXIT
if [[ "${USE_LOSETUP}" == yes ]]; then
  LOOPDEV=$(sudo losetup -P -f --show "${TARGET}") || exit 1
fi

#        start        size    part  contents
#            0           1          PMBR (Boot GUID: CFD085B2-A9A8-194C-A0EC-BF57E361D11A)
#            1           1          Pri GPT header
#            2          32          Pri GPT table
#      4907008     4194413       1  Label: "STATE"
#                                   Type: Linux data
#                                   UUID: A74DE8A6-48E9-B247-BB1D-FF2D84B6B3BA
#        20480      131072       2  Label: "KERN-A"
#                                   Type: ChromeOS kernel
#                                   UUID: 4EFD23EE-52AF-0F4E-A192-0712F824CCDD
#                                   Attr: priority=15 tries=15 successful=0 
#       712704     4194304       3  Label: "ROOT-A"
#                                   Type: ChromeOS rootfs
#                                   UUID: 4E385A0D-4EA0-554E-BABF-4CEDD3F65BAA
#       151552      131072       4  Label: "KERN-B"
#                                   Type: ChromeOS kernel
#                                   UUID: AE721F83-3843-0C46-A3CA-120A237AA60A
#                                   Attr: priority=0 tries=0 successful=0 
#       708608        4096       5  Label: "ROOT-B"
#                                   Type: ChromeOS rootfs
#                                   UUID: 4A3576A7-7DC9-6343-A03B-18D39E11C5B9
#        16448           1       6  Label: "KERN-C"
#                                   Type: ChromeOS kernel
#                                   UUID: 48B51318-A660-1D4D-9D83-409206A520AD
#                                   Attr: priority=0 tries=0 successful=0 
#        16449           1       7  Label: "ROOT-C"
#                                   Type: ChromeOS rootfs
#                                   UUID: 027C7B5A-25A6-144C-8360-EC653A5FEB69
#       282624       32768       8  Label: "OEM"
#                                   Type: Linux data
#                                   UUID: 39EECA08-C1E6-6045-8339-ACAF722ADDCD
#        16450           1       9  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: A8F71C18-D6BB-B34A-8864-FFC0C557C635
#        16451           1      10  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: A76DAD3E-1FB3-114A-ABA4-A8578E974912
#           64       16384      11  Label: "RWFW"
#                                   Type: ChromeOS firmware
#                                   UUID: ED727BC8-A994-064B-9FF1-19EBE9BBA5FA
#       446464      262144      12  Label: "EFI-SYSTEM"
#                                   Type: EFI System Partition
#                                   UUID: CFD085B2-A9A8-194C-A0EC-BF57E361D11A
#                                   Attr: legacy_boot=1 
#      9101436          32          Sec GPT table
#      9101468           1          Sec GPT header
case ${PART:-1} in
1|"STATE")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_1" of="${LOOPDEV}p1"
else
  dd if="part_1" of="${TARGET}" bs=512 count=4194413 seek=4907008 conv=notrunc
fi
esac
case ${PART:-2} in
2|"KERN-A")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_2" of="${LOOPDEV}p2"
else
  dd if="part_2" of="${TARGET}" bs=512 count=131072 seek=20480 conv=notrunc
fi
esac
case ${PART:-3} in
3|"ROOT-A")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_3" of="${LOOPDEV}p3"
else
  dd if="part_3" of="${TARGET}" bs=512 count=4194304 seek=712704 conv=notrunc
fi
esac
case ${PART:-4} in
4|"KERN-B")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_4" of="${LOOPDEV}p4"
else
  dd if="part_4" of="${TARGET}" bs=512 count=131072 seek=151552 conv=notrunc
fi
esac
case ${PART:-5} in
5|"ROOT-B")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_5" of="${LOOPDEV}p5"
else
  dd if="part_5" of="${TARGET}" bs=512 count=4096 seek=708608 conv=notrunc
fi
esac
case ${PART:-6} in
6|"KERN-C")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_6" of="${LOOPDEV}p6"
else
  dd if="part_6" of="${TARGET}" bs=512 count=1 seek=16448 conv=notrunc
fi
esac
case ${PART:-7} in
7|"ROOT-C")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_7" of="${LOOPDEV}p7"
else
  dd if="part_7" of="${TARGET}" bs=512 count=1 seek=16449 conv=notrunc
fi
esac
case ${PART:-8} in
8|"OEM")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_8" of="${LOOPDEV}p8"
else
  dd if="part_8" of="${TARGET}" bs=512 count=32768 seek=282624 conv=notrunc
fi
esac
case ${PART:-9} in
9|"reserved")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_9" of="${LOOPDEV}p9"
else
  dd if="part_9" of="${TARGET}" bs=512 count=1 seek=16450 conv=notrunc
fi
esac
case ${PART:-10} in
10|"reserved")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_10" of="${LOOPDEV}p10"
else
  dd if="part_10" of="${TARGET}" bs=512 count=1 seek=16451 conv=notrunc
fi
esac
case ${PART:-11} in
11|"RWFW")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_11" of="${LOOPDEV}p11"
else
  dd if="part_11" of="${TARGET}" bs=512 count=16384 seek=64 conv=notrunc
fi
esac
case ${PART:-12} in
12|"EFI-SYSTEM")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_12" of="${LOOPDEV}p12"
else
  dd if="part_12" of="${TARGET}" bs=512 count=262144 seek=446464 conv=notrunc
fi
esac
