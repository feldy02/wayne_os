#!/bin/bash -eu
# File automatically generated. Do not edit.

usage() {
  local ret=0
  if [[ $# -gt 0 ]]; then
    # Write to stderr on errors.
    exec 1>&2
    echo "ERROR: $*"
    echo
    ret=1
  fi
  echo "Usage: $0 [-h|--help] [--nolosetup] [image] [part]"
  echo "Example: $0 chromiumos_image.bin"
  exit ${ret}
}

USE_LOSETUP=yes
TARGET=""
PART=""
ARG_INDEX=0

while [[ $# -gt 0 ]]; do
  case "$1" in
    -h|--help)
      usage
      ;;
    --nolosetup)
      USE_LOSETUP=no
      shift
      ;;
    *)
      if [[ ${ARG_INDEX} -eq 0 ]]; then
        TARGET="${1}"
      elif [[ ${ARG_INDEX} -eq 1 ]]; then
        PART="${1}"
      else
        usage "too many arguments"
      fi
      ARG_INDEX=$((ARG_INDEX+1))
      shift
      ;;
  esac
done

case ${TARGET} in
"")
  for TARGET in chromiumos_{,base_}image.bin ""; do
    if [[ -e ${TARGET} ]]; then
      echo "autodetected image: ${TARGET}"
      break
    fi
  done
  if [[ -z ${TARGET} ]]; then
    usage "could not autodetect an image"
  fi
  ;;
*)
  if [[ ! -e ${TARGET} ]]; then
    usage "image does not exist: ${TARGET}"
  fi
esac

# Losetup has support for partitions, and offset= has issues.
# See crbug.com/954188
LOOPDEV=''
cleanup() {
  if [[ -n "${LOOPDEV}" ]]; then
    sudo losetup -d "${LOOPDEV}"
  fi
}
trap cleanup EXIT
if [[ "${USE_LOSETUP}" == yes ]]; then
  LOOPDEV=$(sudo losetup -P -f --show "${TARGET}") || exit 1
fi

#        start        size    part  contents
#            0           1          PMBR (Boot GUID: D7C9BEA6-7523-964F-BE61-8D4508A36F4A)
#            1           1          Pri GPT header
#            2          32          Pri GPT table
#      7947880     4194411       1  Label: "STATE"
#                                   Type: Linux data
#                                   UUID: 9045D8A0-CD3D-2642-B415-6090FBCB5DCC
#        20480      131072       2  Label: "KERN-A"
#                                   Type: ChromeOS kernel
#                                   UUID: A00D2FE6-68B6-294E-A3E3-0C1450B88282
#                                   Attr: priority=15 tries=15 successful=0 
#       712704     7235175       3  Label: "ROOT-A"
#                                   Type: ChromeOS rootfs
#                                   UUID: 2B60720F-2DA4-3045-91FC-1D94837EF3E3
#       151552      131072       4  Label: "KERN-B"
#                                   Type: ChromeOS kernel
#                                   UUID: F1DA2034-9B97-0A4F-8638-2744098D996C
#                                   Attr: priority=0 tries=0 successful=0 
#       708608        4096       5  Label: "ROOT-B"
#                                   Type: ChromeOS rootfs
#                                   UUID: 0E8B6FB2-4588-9945-AE3B-1EA57585AC79
#        16448           1       6  Label: "KERN-C"
#                                   Type: ChromeOS kernel
#                                   UUID: B6EE0BF5-7B4B-974D-B514-B85CC6956608
#                                   Attr: priority=0 tries=0 successful=0 
#        16449           1       7  Label: "ROOT-C"
#                                   Type: ChromeOS rootfs
#                                   UUID: F865C283-FE08-544A-847A-21A1575CEE77
#       282624       32768       8  Label: "OEM"
#                                   Type: Linux data
#                                   UUID: F05C3C05-3779-B245-A281-7546E4CC4A8C
#        16450           1       9  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: 572F7F78-E469-1548-94CB-816C7C734A5C
#        16451           1      10  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: FD4AEBCF-F946-4241-B8D0-67A0BEA2B09C
#           64       16384      11  Label: "RWFW"
#                                   Type: ChromeOS firmware
#                                   UUID: D8E0B7E7-B6CE-724F-8922-80CC37D7BD02
#       446464      262144      12  Label: "EFI-SYSTEM"
#                                   Type: EFI System Partition
#                                   UUID: D7C9BEA6-7523-964F-BE61-8D4508A36F4A
#                                   Attr: legacy_boot=1 
#     12142306          32          Sec GPT table
#     12142338           1          Sec GPT header
case ${PART:-1} in
1|"STATE")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_1" of="${LOOPDEV}p1"
else
  dd if="part_1" of="${TARGET}" bs=512 count=4194411 seek=7947880 conv=notrunc
fi
esac
case ${PART:-2} in
2|"KERN-A")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_2" of="${LOOPDEV}p2"
else
  dd if="part_2" of="${TARGET}" bs=512 count=131072 seek=20480 conv=notrunc
fi
esac
case ${PART:-3} in
3|"ROOT-A")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_3" of="${LOOPDEV}p3"
else
  dd if="part_3" of="${TARGET}" bs=512 count=7235175 seek=712704 conv=notrunc
fi
esac
case ${PART:-4} in
4|"KERN-B")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_4" of="${LOOPDEV}p4"
else
  dd if="part_4" of="${TARGET}" bs=512 count=131072 seek=151552 conv=notrunc
fi
esac
case ${PART:-5} in
5|"ROOT-B")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_5" of="${LOOPDEV}p5"
else
  dd if="part_5" of="${TARGET}" bs=512 count=4096 seek=708608 conv=notrunc
fi
esac
case ${PART:-6} in
6|"KERN-C")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_6" of="${LOOPDEV}p6"
else
  dd if="part_6" of="${TARGET}" bs=512 count=1 seek=16448 conv=notrunc
fi
esac
case ${PART:-7} in
7|"ROOT-C")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_7" of="${LOOPDEV}p7"
else
  dd if="part_7" of="${TARGET}" bs=512 count=1 seek=16449 conv=notrunc
fi
esac
case ${PART:-8} in
8|"OEM")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_8" of="${LOOPDEV}p8"
else
  dd if="part_8" of="${TARGET}" bs=512 count=32768 seek=282624 conv=notrunc
fi
esac
case ${PART:-9} in
9|"reserved")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_9" of="${LOOPDEV}p9"
else
  dd if="part_9" of="${TARGET}" bs=512 count=1 seek=16450 conv=notrunc
fi
esac
case ${PART:-10} in
10|"reserved")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_10" of="${LOOPDEV}p10"
else
  dd if="part_10" of="${TARGET}" bs=512 count=1 seek=16451 conv=notrunc
fi
esac
case ${PART:-11} in
11|"RWFW")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_11" of="${LOOPDEV}p11"
else
  dd if="part_11" of="${TARGET}" bs=512 count=16384 seek=64 conv=notrunc
fi
esac
case ${PART:-12} in
12|"EFI-SYSTEM")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_12" of="${LOOPDEV}p12"
else
  dd if="part_12" of="${TARGET}" bs=512 count=262144 seek=446464 conv=notrunc
fi
esac
