#!/bin/bash -eu
# File automatically generated. Do not edit.

usage() {
  local ret=0
  if [[ $# -gt 0 ]]; then
    # Write to stderr on errors.
    exec 1>&2
    echo "ERROR: $*"
    echo
    ret=1
  fi
  echo "Usage: $0 [-h|--help] [--nolosetup] [image] [part]"
  echo "Example: $0 chromiumos_image.bin"
  exit ${ret}
}

USE_LOSETUP=yes
TARGET=""
PART=""
ARG_INDEX=0

while [[ $# -gt 0 ]]; do
  case "$1" in
    -h|--help)
      usage
      ;;
    --nolosetup)
      USE_LOSETUP=no
      shift
      ;;
    *)
      if [[ ${ARG_INDEX} -eq 0 ]]; then
        TARGET="${1}"
      elif [[ ${ARG_INDEX} -eq 1 ]]; then
        PART="${1}"
      else
        usage "too many arguments"
      fi
      ARG_INDEX=$((ARG_INDEX+1))
      shift
      ;;
  esac
done

case ${TARGET} in
"")
  for TARGET in chromiumos_{,base_}image.bin ""; do
    if [[ -e ${TARGET} ]]; then
      echo "autodetected image: ${TARGET}"
      break
    fi
  done
  if [[ -z ${TARGET} ]]; then
    usage "could not autodetect an image"
  fi
  ;;
*)
  if [[ ! -e ${TARGET} ]]; then
    usage "image does not exist: ${TARGET}"
  fi
esac

#        start        size    part  contents
#            0           1          PMBR (Boot GUID: D7C9BEA6-7523-964F-BE61-8D4508A36F4A)
#            1           1          Pri GPT header
#            2          32          Pri GPT table
#      7947880     4194411       1  Label: "STATE"
#                                   Type: Linux data
#                                   UUID: 9045D8A0-CD3D-2642-B415-6090FBCB5DCC
#        20480      131072       2  Label: "KERN-A"
#                                   Type: ChromeOS kernel
#                                   UUID: A00D2FE6-68B6-294E-A3E3-0C1450B88282
#                                   Attr: priority=15 tries=15 successful=0 
#       712704     7235175       3  Label: "ROOT-A"
#                                   Type: ChromeOS rootfs
#                                   UUID: 2B60720F-2DA4-3045-91FC-1D94837EF3E3
#       151552      131072       4  Label: "KERN-B"
#                                   Type: ChromeOS kernel
#                                   UUID: F1DA2034-9B97-0A4F-8638-2744098D996C
#                                   Attr: priority=0 tries=0 successful=0 
#       708608        4096       5  Label: "ROOT-B"
#                                   Type: ChromeOS rootfs
#                                   UUID: 0E8B6FB2-4588-9945-AE3B-1EA57585AC79
#        16448           1       6  Label: "KERN-C"
#                                   Type: ChromeOS kernel
#                                   UUID: B6EE0BF5-7B4B-974D-B514-B85CC6956608
#                                   Attr: priority=0 tries=0 successful=0 
#        16449           1       7  Label: "ROOT-C"
#                                   Type: ChromeOS rootfs
#                                   UUID: F865C283-FE08-544A-847A-21A1575CEE77
#       282624       32768       8  Label: "OEM"
#                                   Type: Linux data
#                                   UUID: F05C3C05-3779-B245-A281-7546E4CC4A8C
#        16450           1       9  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: 572F7F78-E469-1548-94CB-816C7C734A5C
#        16451           1      10  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: FD4AEBCF-F946-4241-B8D0-67A0BEA2B09C
#           64       16384      11  Label: "RWFW"
#                                   Type: ChromeOS firmware
#                                   UUID: D8E0B7E7-B6CE-724F-8922-80CC37D7BD02
#       446464      262144      12  Label: "EFI-SYSTEM"
#                                   Type: EFI System Partition
#                                   UUID: D7C9BEA6-7523-964F-BE61-8D4508A36F4A
#                                   Attr: legacy_boot=1 
#     12142306          32          Sec GPT table
#     12142338           1          Sec GPT header
case ${PART:-1} in
1|"STATE")
if [[ -d dir_1 ]]; then
  (
  sudo umount dir_1 || :
  rmdir dir_1
  rm -f "dir_1_STATE"
  ) &
fi
esac
case ${PART:-2} in
2|"KERN-A")
if [[ -d dir_2 ]]; then
  (
  sudo umount dir_2 || :
  rmdir dir_2
  rm -f "dir_2_KERN-A"
  ) &
fi
esac
case ${PART:-3} in
3|"ROOT-A")
if [[ -d dir_3 ]]; then
  (
  sudo umount dir_3 || :
  rmdir dir_3
  rm -f "dir_3_ROOT-A"
  ) &
fi
esac
case ${PART:-4} in
4|"KERN-B")
if [[ -d dir_4 ]]; then
  (
  sudo umount dir_4 || :
  rmdir dir_4
  rm -f "dir_4_KERN-B"
  ) &
fi
esac
case ${PART:-5} in
5|"ROOT-B")
if [[ -d dir_5 ]]; then
  (
  sudo umount dir_5 || :
  rmdir dir_5
  rm -f "dir_5_ROOT-B"
  ) &
fi
esac
case ${PART:-6} in
6|"KERN-C")
esac
case ${PART:-7} in
7|"ROOT-C")
esac
case ${PART:-8} in
8|"OEM")
if [[ -d dir_8 ]]; then
  (
  sudo umount dir_8 || :
  rmdir dir_8
  rm -f "dir_8_OEM"
  ) &
fi
esac
case ${PART:-9} in
9|"reserved")
esac
case ${PART:-10} in
10|"reserved")
esac
case ${PART:-11} in
11|"RWFW")
if [[ -d dir_11 ]]; then
  (
  sudo umount dir_11 || :
  rmdir dir_11
  rm -f "dir_11_RWFW"
  ) &
fi
esac
case ${PART:-12} in
12|"EFI-SYSTEM")
if [[ -d dir_12 ]]; then
  (
  sudo umount dir_12 || :
  rmdir dir_12
  rm -f "dir_12_EFI-SYSTEM"
  ) &
fi
esac
wait
