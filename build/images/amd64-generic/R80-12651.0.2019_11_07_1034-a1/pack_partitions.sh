#!/bin/bash -eu
# File automatically generated. Do not edit.

usage() {
  local ret=0
  if [[ $# -gt 0 ]]; then
    # Write to stderr on errors.
    exec 1>&2
    echo "ERROR: $*"
    echo
    ret=1
  fi
  echo "Usage: $0 [-h|--help] [--nolosetup] [image] [part]"
  echo "Example: $0 chromiumos_image.bin"
  exit ${ret}
}

USE_LOSETUP=yes
TARGET=""
PART=""
ARG_INDEX=0

while [[ $# -gt 0 ]]; do
  case "$1" in
    -h|--help)
      usage
      ;;
    --nolosetup)
      USE_LOSETUP=no
      shift
      ;;
    *)
      if [[ ${ARG_INDEX} -eq 0 ]]; then
        TARGET="${1}"
      elif [[ ${ARG_INDEX} -eq 1 ]]; then
        PART="${1}"
      else
        usage "too many arguments"
      fi
      ARG_INDEX=$((ARG_INDEX+1))
      shift
      ;;
  esac
done

case ${TARGET} in
"")
  for TARGET in chromiumos_{,base_}image.bin ""; do
    if [[ -e ${TARGET} ]]; then
      echo "autodetected image: ${TARGET}"
      break
    fi
  done
  if [[ -z ${TARGET} ]]; then
    usage "could not autodetect an image"
  fi
  ;;
*)
  if [[ ! -e ${TARGET} ]]; then
    usage "image does not exist: ${TARGET}"
  fi
esac

# Losetup has support for partitions, and offset= has issues.
# See crbug.com/954188
LOOPDEV=''
cleanup() {
  if [[ -n "${LOOPDEV}" ]]; then
    sudo losetup -d "${LOOPDEV}"
  fi
}
trap cleanup EXIT
if [[ "${USE_LOSETUP}" == yes ]]; then
  LOOPDEV=$(sudo losetup -P -f --show "${TARGET}") || exit 1
fi

#        start        size    part  contents
#            0           1          PMBR (Boot GUID: B0E6762F-66E9-2C4B-ACBF-A96E095BBAEA)
#            1           1          Pri GPT header
#            2          32          Pri GPT table
#      4907008     4194413       1  Label: "STATE"
#                                   Type: Linux data
#                                   UUID: 0F575E31-C685-F643-8377-BE58FA39496E
#        20480      131072       2  Label: "KERN-A"
#                                   Type: ChromeOS kernel
#                                   UUID: 05C40F4B-F5AB-C64D-9AF6-82D6EC224BCA
#                                   Attr: priority=15 tries=15 successful=0 
#       712704     4194304       3  Label: "ROOT-A"
#                                   Type: ChromeOS rootfs
#                                   UUID: BC88F6DE-F6B5-E449-8EE2-1A1B19E21A83
#       151552      131072       4  Label: "KERN-B"
#                                   Type: ChromeOS kernel
#                                   UUID: 52BB5BF3-358E-A844-9B6B-182E8943287F
#                                   Attr: priority=0 tries=0 successful=0 
#       708608        4096       5  Label: "ROOT-B"
#                                   Type: ChromeOS rootfs
#                                   UUID: 95967A28-1B2D-EF41-A6AF-04DEF4E8A23C
#        16448           1       6  Label: "KERN-C"
#                                   Type: ChromeOS kernel
#                                   UUID: A4CB502A-D065-B243-BE02-DA5B056F75BD
#                                   Attr: priority=0 tries=0 successful=0 
#        16449           1       7  Label: "ROOT-C"
#                                   Type: ChromeOS rootfs
#                                   UUID: D8DA6793-FFF1-F340-8E5B-F7D2DB90D33C
#       282624       32768       8  Label: "OEM"
#                                   Type: Linux data
#                                   UUID: 08372735-CF44-FD43-9B5C-EDDF75D0B49C
#        16450           1       9  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: 5BABCC89-39D2-764F-B2CD-BEFC471C5AB8
#        16451           1      10  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: 62399D61-F43D-E04D-9DDC-D306CF6276AD
#           64       16384      11  Label: "RWFW"
#                                   Type: ChromeOS firmware
#                                   UUID: 17C1EEA6-CC0C-0348-B147-629215756392
#       446464      262144      12  Label: "EFI-SYSTEM"
#                                   Type: EFI System Partition
#                                   UUID: B0E6762F-66E9-2C4B-ACBF-A96E095BBAEA
#                                   Attr: legacy_boot=1 
#      9101436          32          Sec GPT table
#      9101468           1          Sec GPT header
case ${PART:-1} in
1|"STATE")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_1" of="${LOOPDEV}p1"
else
  dd if="part_1" of="${TARGET}" bs=512 count=4194413 seek=4907008 conv=notrunc
fi
esac
case ${PART:-2} in
2|"KERN-A")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_2" of="${LOOPDEV}p2"
else
  dd if="part_2" of="${TARGET}" bs=512 count=131072 seek=20480 conv=notrunc
fi
esac
case ${PART:-3} in
3|"ROOT-A")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_3" of="${LOOPDEV}p3"
else
  dd if="part_3" of="${TARGET}" bs=512 count=4194304 seek=712704 conv=notrunc
fi
esac
case ${PART:-4} in
4|"KERN-B")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_4" of="${LOOPDEV}p4"
else
  dd if="part_4" of="${TARGET}" bs=512 count=131072 seek=151552 conv=notrunc
fi
esac
case ${PART:-5} in
5|"ROOT-B")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_5" of="${LOOPDEV}p5"
else
  dd if="part_5" of="${TARGET}" bs=512 count=4096 seek=708608 conv=notrunc
fi
esac
case ${PART:-6} in
6|"KERN-C")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_6" of="${LOOPDEV}p6"
else
  dd if="part_6" of="${TARGET}" bs=512 count=1 seek=16448 conv=notrunc
fi
esac
case ${PART:-7} in
7|"ROOT-C")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_7" of="${LOOPDEV}p7"
else
  dd if="part_7" of="${TARGET}" bs=512 count=1 seek=16449 conv=notrunc
fi
esac
case ${PART:-8} in
8|"OEM")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_8" of="${LOOPDEV}p8"
else
  dd if="part_8" of="${TARGET}" bs=512 count=32768 seek=282624 conv=notrunc
fi
esac
case ${PART:-9} in
9|"reserved")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_9" of="${LOOPDEV}p9"
else
  dd if="part_9" of="${TARGET}" bs=512 count=1 seek=16450 conv=notrunc
fi
esac
case ${PART:-10} in
10|"reserved")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_10" of="${LOOPDEV}p10"
else
  dd if="part_10" of="${TARGET}" bs=512 count=1 seek=16451 conv=notrunc
fi
esac
case ${PART:-11} in
11|"RWFW")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_11" of="${LOOPDEV}p11"
else
  dd if="part_11" of="${TARGET}" bs=512 count=16384 seek=64 conv=notrunc
fi
esac
case ${PART:-12} in
12|"EFI-SYSTEM")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="part_12" of="${LOOPDEV}p12"
else
  dd if="part_12" of="${TARGET}" bs=512 count=262144 seek=446464 conv=notrunc
fi
esac
