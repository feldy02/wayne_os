#!/bin/bash -eu
# File automatically generated. Do not edit.

usage() {
  local ret=0
  if [[ $# -gt 0 ]]; then
    # Write to stderr on errors.
    exec 1>&2
    echo "ERROR: $*"
    echo
    ret=1
  fi
  echo "Usage: $0 [-h|--help] [--nolosetup] [image] [part]"
  echo "Example: $0 chromiumos_image.bin"
  exit ${ret}
}

USE_LOSETUP=yes
TARGET=""
PART=""
ARG_INDEX=0

while [[ $# -gt 0 ]]; do
  case "$1" in
    -h|--help)
      usage
      ;;
    --nolosetup)
      USE_LOSETUP=no
      shift
      ;;
    *)
      if [[ ${ARG_INDEX} -eq 0 ]]; then
        TARGET="${1}"
      elif [[ ${ARG_INDEX} -eq 1 ]]; then
        PART="${1}"
      else
        usage "too many arguments"
      fi
      ARG_INDEX=$((ARG_INDEX+1))
      shift
      ;;
  esac
done

case ${TARGET} in
"")
  for TARGET in chromiumos_{,base_}image.bin ""; do
    if [[ -e ${TARGET} ]]; then
      echo "autodetected image: ${TARGET}"
      break
    fi
  done
  if [[ -z ${TARGET} ]]; then
    usage "could not autodetect an image"
  fi
  ;;
*)
  if [[ ! -e ${TARGET} ]]; then
    usage "image does not exist: ${TARGET}"
  fi
esac

# Losetup has support for partitions, and offset= has issues.
# See crbug.com/954188
LOOPDEV=''
cleanup() {
  if [[ -n "${LOOPDEV}" ]]; then
    sudo losetup -d "${LOOPDEV}"
  fi
}
trap cleanup EXIT
if [[ "${USE_LOSETUP}" == yes ]]; then
  LOOPDEV=$(sudo losetup -P -f --show "${TARGET}") || exit 1
fi

#        start        size    part  contents
#            0           1          PMBR (Boot GUID: C76864E7-66E5-FE4D-9AA8-18D324F41C12)
#            1           1          Pri GPT header
#            2          32          Pri GPT table
#      4907008     8388717       1  Label: "STATE"
#                                   Type: Linux data
#                                   UUID: 6A1D2C2C-868B-AA47-8C1B-B8655B42B472
#        20480      131072       2  Label: "KERN-A"
#                                   Type: ChromeOS kernel
#                                   UUID: D90AE842-D0C3-C44F-985D-226CFAACFA88
#                                   Attr: priority=15 tries=15 successful=0 
#       712704     4194304       3  Label: "ROOT-A"
#                                   Type: ChromeOS rootfs
#                                   UUID: 027DC278-775C-AA4C-9F1B-EF32A3B2CC63
#       151552      131072       4  Label: "KERN-B"
#                                   Type: ChromeOS kernel
#                                   UUID: F6C7E295-D283-674E-B716-DC8F6A2CB28E
#                                   Attr: priority=0 tries=0 successful=0 
#       708608        4096       5  Label: "ROOT-B"
#                                   Type: ChromeOS rootfs
#                                   UUID: 2CF0FE9E-0A28-4D42-9998-0310224DF868
#        16448           1       6  Label: "KERN-C"
#                                   Type: ChromeOS kernel
#                                   UUID: F77FD58E-580F-5840-BB2F-6F3E6B8BEA0A
#                                   Attr: priority=0 tries=0 successful=0 
#        16449           1       7  Label: "ROOT-C"
#                                   Type: ChromeOS rootfs
#                                   UUID: C7D234E3-F5CE-0147-82F5-0C452354985A
#       282624       32768       8  Label: "OEM"
#                                   Type: Linux data
#                                   UUID: DF3C882C-D5CD-F54C-B3AB-F61161FA9B8B
#        16450           1       9  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: F9BFE683-1070-1542-8498-3F8CC9118FC0
#        16451           1      10  Label: "reserved"
#                                   Type: ChromeOS reserved
#                                   UUID: 46122527-AD50-B04B-964D-B3DA10673E60
#           64       16384      11  Label: "RWFW"
#                                   Type: ChromeOS firmware
#                                   UUID: AEEAA502-1926-DA45-BF55-9AC5B958211A
#       446464      262144      12  Label: "EFI-SYSTEM"
#                                   Type: EFI System Partition
#                                   UUID: C76864E7-66E5-FE4D-9AA8-18D324F41C12
#                                   Attr: legacy_boot=1 
#     13295740          32          Sec GPT table
#     13295772           1          Sec GPT header
case ${PART:-1} in
1|"STATE")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p1" of="part_1"
else
  dd if="${TARGET}" of="part_1" bs=512 count=8388717 skip=4907008
fi
ln -sfT part_1 "part_1_STATE"
esac
case ${PART:-2} in
2|"KERN-A")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p2" of="part_2"
else
  dd if="${TARGET}" of="part_2" bs=512 count=131072 skip=20480
fi
ln -sfT part_2 "part_2_KERN-A"
esac
case ${PART:-3} in
3|"ROOT-A")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p3" of="part_3"
else
  dd if="${TARGET}" of="part_3" bs=512 count=4194304 skip=712704
fi
ln -sfT part_3 "part_3_ROOT-A"
esac
case ${PART:-4} in
4|"KERN-B")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p4" of="part_4"
else
  dd if="${TARGET}" of="part_4" bs=512 count=131072 skip=151552
fi
ln -sfT part_4 "part_4_KERN-B"
esac
case ${PART:-5} in
5|"ROOT-B")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p5" of="part_5"
else
  dd if="${TARGET}" of="part_5" bs=512 count=4096 skip=708608
fi
ln -sfT part_5 "part_5_ROOT-B"
esac
case ${PART:-6} in
6|"KERN-C")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p6" of="part_6"
else
  dd if="${TARGET}" of="part_6" bs=512 count=1 skip=16448
fi
ln -sfT part_6 "part_6_KERN-C"
esac
case ${PART:-7} in
7|"ROOT-C")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p7" of="part_7"
else
  dd if="${TARGET}" of="part_7" bs=512 count=1 skip=16449
fi
ln -sfT part_7 "part_7_ROOT-C"
esac
case ${PART:-8} in
8|"OEM")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p8" of="part_8"
else
  dd if="${TARGET}" of="part_8" bs=512 count=32768 skip=282624
fi
ln -sfT part_8 "part_8_OEM"
esac
case ${PART:-9} in
9|"reserved")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p9" of="part_9"
else
  dd if="${TARGET}" of="part_9" bs=512 count=1 skip=16450
fi
ln -sfT part_9 "part_9_reserved"
esac
case ${PART:-10} in
10|"reserved")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p10" of="part_10"
else
  dd if="${TARGET}" of="part_10" bs=512 count=1 skip=16451
fi
ln -sfT part_10 "part_10_reserved"
esac
case ${PART:-11} in
11|"RWFW")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p11" of="part_11"
else
  dd if="${TARGET}" of="part_11" bs=512 count=16384 skip=64
fi
ln -sfT part_11 "part_11_RWFW"
esac
case ${PART:-12} in
12|"EFI-SYSTEM")
if [[ -n "${LOOPDEV}" ]]; then
  sudo dd if="${LOOPDEV}p12" of="part_12"
else
  dd if="${TARGET}" of="part_12" bs=512 count=262144 skip=446464
fi
ln -sfT part_12 "part_12_EFI-SYSTEM"
esac
