#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2017 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""Switcher for chrome on chromeos local build bisecting

This script will sync the chrome source tree to the specified version, build,
and deploy.

Typical usage companion with bisect_cr_localbuild_master:
  $ ./bisect_cr_localbuild_master.py config switch \
      ./switch_cros_cr_localbuild_master.py

By default, it will build and deploy chrome. You can specify --target for
alternative binaries.
"""
from __future__ import print_function
import argparse
import logging
import os
import sys

from bisect_kit import cli
from bisect_kit import common
from bisect_kit import configure
from bisect_kit import cros_util
from bisect_kit import cr_util
from bisect_kit import errors
from bisect_kit import gclient_util
from bisect_kit import git_util
import bisect_cr_localbuild_master
import switch_cros_localbuild

logger = logging.getLogger(__name__)


def create_argument_parser():
  parser = argparse.ArgumentParser()
  cli.patching_argparser_exit(parser)
  common.add_common_arguments(parser)
  parser.add_argument(
      '--dut',
      type=cli.argtype_notempty,
      metavar='DUT',
      default=configure.get('DUT'),
      help='Address of DUT')
  parser.add_argument(
      'rev',
      nargs='?',
      type=cli.argtype_notempty,
      metavar='REV',
      default=configure.get('REV', ''),
      help='Version string')
  parser.add_argument(
      '--board', default=configure.get('BOARD'), help='ChromeOS board name')
  parser.add_argument(
      '--chrome_root',
      metavar='CHROME_ROOT',
      type=cli.argtype_dir_path,
      default=configure.get('CHROME_ROOT', ''),
      help='Root of Chrome source tree, like ~/chromium')

  group = parser.add_mutually_exclusive_group()
  group.add_argument(
      '--target',
      action='append',
      help='Binary to build and deploy, like unittest or fuzzers. '
      'Example value: "video_decode_accelerator_unittest". '
      'This option could be specified multiple times.')
  group.add_argument(
      '--without_tests',
      action='store_false',
      dest='with_tests',
      help='(if --target is not specified) '
      'Besides chrome, build test binaries as well.')

  parser.add_argument(
      '--deploy_method',
      choices=['chrome_deploy', 'image'],
      default='chrome_deploy',
      help='Deploy method (default: %(default)s)')
  parser.add_argument(
      '--nobuild',
      action='store_true',
      help='Sync source code only; do not build; imply --nodeploy')
  parser.add_argument(
      '--nodeploy', action='store_true', help='Do not deploy after build')

  group = parser.add_argument_group(
      title='Options for building chrome in chromeos sdk chroot')
  group.add_argument(
      '--noimage',
      action='store_true',
      help='Build packages only; do not build image; imply --nodeploy')
  group.add_argument(
      '--chromeos_root',
      type=cli.argtype_dir_path,
      metavar='CHROMEOS_ROOT',
      default=configure.get('CHROMEOS_ROOT'),
      help='ChromeOS tree root; only necessary if deploy_method is '
      'cros_deploy or image')
  switch_cros_localbuild.add_build_and_deploy_arguments(group)
  return parser


@cli.fatal_error_handler
def main(args=None):
  common.init()
  parser = create_argument_parser()
  opts = parser.parse_args(args)
  common.config_logging(opts)

  if not opts.dut:
    if not opts.nodeploy:
      raise errors.ArgumentError('--dut',
                                 'DUT can be omitted only if --nodeploy')
    if not opts.board:
      raise errors.ArgumentError('--board',
                                 'board must be specified if no --dut')
  if not opts.board:
    opts.board = cros_util.query_dut_board(opts.dut)
  if opts.deploy_method != 'chrome_deploy':
    if not opts.chromeos_root:
      raise errors.ArgumentError(
          '--chromeos_root',
          'should be specified for --deploy_method=' + opts.deploy_method)

  opts.chrome_src = os.path.join(opts.chrome_root, 'src')
  assert os.path.exists(opts.chrome_src)

  assert bisect_cr_localbuild_master.verify_gclient_dep(opts.chrome_root)
  rev = bisect_cr_localbuild_master.guess_git_rev(opts, opts.rev)

  git_util.checkout_version(opts.chrome_src, rev)
  gclient_util.sync(opts.chrome_root)

  if opts.nobuild:
    return

  # TODO(kcwu): support local patch.
  if opts.deploy_method == 'chrome_deploy':
    cr_util.build_and_deploy(
        opts.chrome_src,
        opts.board,
        opts.dut,
        opts.target,
        opts.with_tests,
        nodeploy=opts.nodeploy)
  elif opts.deploy_method == 'image':
    try:
      returncode = switch_cros_localbuild.build_and_deploy(
          opts, do_mark_as_stable=False)
    except Exception:
      logger.exception('switch failed')
      returncode = 1
    sys.exit(returncode)


if __name__ == '__main__':
  main()
