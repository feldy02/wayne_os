# -*- coding: utf-8 -*-
# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
"""Test android_util module."""

from __future__ import print_function
import unittest

try:
  from unittest import mock
except ImportError:
  # TODO(kcwu): remove once migrated to python3
  import mock

from bisect_kit import android_util
from bisect_kit import testing


class TestAndroidUtil(unittest.TestCase):
  """Test functions in android_util module."""

  def test_is_android_build_id(self):
    assert android_util.is_android_build_id('123456')
    assert not android_util.is_android_build_id('foobar')

  def test_get_build_ids_between(self):
    data = '{"ids":["4683388","4683367","4683364","4683353","4683349"]}'
    with mock.patch.object(
        android_util, 'fetch_android_build_data', return_value=data):
      self.assertEqual(
          android_util.get_build_ids_between('aosp-master', 'aosp_arm64-eng',
                                             '4683349', '4683388'),
          ['4683349', '4683353', '4683364', '4683367', '4683388'])

    # check sort order
    data = '{"ids":["10000001","9999999"]}'
    with mock.patch.object(
        android_util, 'fetch_android_build_data', return_value=data):
      self.assertEqual(
          android_util.get_build_ids_between('aosp-master', 'aosp_arm64-eng',
                                             '9999999', '10000001'),
          ['9999999', '10000001'])

  def test_is_good_build(self):
    with testing.mock_function_by_file(android_util, 'fetch_android_build_data',
                                       'android-build-status-4683364.json'):
      self.assertFalse(
          android_util.is_good_build('aosp-master', 'build_test', '4683364'))
      self.assertTrue(
          android_util.is_good_build('aosp-master', 'aosp_x86-eng', '4683364'))


if __name__ == '__main__':
  unittest.main()
