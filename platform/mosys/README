mosys - The more open system tool

Introduction
------------
Mosys is designed to aid in obtaining obscure system-specific information
and present it in a uniform, human-friendly manner that is also parseable. It
is intended to complement other systems software and may overlap in some areas.

Examples of information this program is useful for:
- Platform identification
- Snapshot of machine information
- Listing non-enumerable devices
- Retrieving vital product data

Due to the system-specific nature of this program, it is unlikely to work with
most platforms. A generic subset of functionality will be available, though
its usefulness may be extremely limited.

Configuration & Building
------------------------
Dependencies:
  libfmap
  libuuid
  libfdt (typically from the dtc package in your distribution)

Mosys is a two-stage compile: the first stage is for all of the C components and
uses the Meson build system and the second stage is Rust Cargo for all of the
Rust components.

Chrome OS contributors will want to use cros_workon instead. Using the ebuild is
vastly easier than trying to build manually.

See meson.build for valid arch options.

Flags of interest:
-Duse_cros_config=true/false: whether to enable linking to cros_config for
                              Chrome OS. See Chrome OS example in Mosys ebuild.

Note that you might have better luck compiling Mosys with Clang while -Werror is
enabled. This is because we (Chromium) use Clang as our default compiler. To do
that, add CC=clang before you invoke Meson.

All of this should look something like, for the C components:
  CC=clang meson -Darch=amd64 output_dir
  ninja -C output_dir

or:
  CC=clang meson -Darch=amd64 -Duse_cros_config=true output_dir
  ninja -C output_dir

To build with use_cros_config=true, you need one file that is dynamically
provided as inputs to the build process from other ebuilds in the Chrome OS
build system:
  lib/cros_config/cros_config_data.c

You can use -Dcros_config_data_src to override the config file that will be used
by the build system.

If you just want to compile something quickly, you can do:
  cp lib/cros_config/dummy_cros_config_data.c lib/cros_config/cros_config_data.c

and then run the `meson` command as above.

Then, to build the Rust components, first build the C components using the
instructions above. Then, run:

  MESON_BUILD_ROOT=output_dir cargo build

If you are building with use_cros_config, as above, instead you will do:

  MESON_BUILD_ROOT=output_dir USE=unibuild cargo build

In the end, this will output a "target/debug/mosys" binary that is dynamically
linked. Use `--release` argument to `cargo` to get a release build.

The `mosys` binary that is output will typically abort on most platforms when
run as a user without root privs. To try to work with this locally without
moving it to a target platform, you can run:

  sudo mosys --help

or:

  sudo mosys -j -p dummy

to disable the Minijail and force the platform to the "dummy" platform.

To run tests (with cros_config enabled):
  CC=clang meson -Darch=amd64 -Duse_cros_config=true -Dtest_cros_config=true out
  ninja -C out
  USE=unibuild MESON_BUILD_ROOT=out cargo test

Usage
-----
Mosys uses a natural language syntax to navigate thru a command hierarchy for
a given platform. Each supported platform will have a specific set of commands
available.

Typically, top-level commands will reflect classes of hardware components
such as "cpu" or "sensor", available busses such as "pci" or "i2c", or a
type of information such as "smbios".

Mosys returns EXIT_SUCCESS upon successful completion of a command, or
EXIT_FAILURE if a command is not supported on a given platform. These values
are defined in the system's stdlib.h. In case of failure, Mosys will also print
a message to stderr to let the user know.

Use "mosys -t" to display the command tree for the host platform.

Debugging
---------
Mosys uses a minijail to restrict behavior. This can sometimes restrict desired
behavior, especially system calls not whitelisted by the seccomp filter.
If a subcommand is returning with an error that should be passing, this might
be why. In order to debug this, run seccomp_debug.sh with the failing command.
This will print out the system calls that caused the run to fail.

For example, to debug `mosys ec info`, copy over `seccomp_debug.sh` to
the DUT and run:

    ./seccomp_debug.sh mosys ec info
