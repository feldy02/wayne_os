// Copyright 2019 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

package firmware

import (
	"bytes"
	"context"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"reflect"
	"strings"
	"time"

	"boringssl.googlesource.com/boringssl/util/fipstools/acvp/acvptool/acvp"
	"boringssl.googlesource.com/boringssl/util/fipstools/acvp/acvptool/subprocess"

	"chromiumos/tast/errors"
	"chromiumos/tast/local/testexec"
	"chromiumos/tast/shutil"
	"chromiumos/tast/testing"
)

func init() {
	testing.AddTest(&testing.Test{
		Func: ACVP,
		Attr: []string{"informational", "disabled"},
		Contacts: []string{
			"gurleengrewal@chromium.org", // Test author
			"sukhomlinov@chromium.org",   // CR50 certification lead
		},
		Desc:         "Takes a JSON generated by the ACVP server and runs the test cases in it",
		SoftwareDeps: []string{"chrome", "tpm"},
		Params: []testing.Param{{
			Name: "aes_ecb_full",
			Val: data{
				inputFile:    "aes-ecb-full.json",
				expectedFile: "aes-ecb-full-expected.json",
			},
			ExtraData: []string{
				"aes-ecb-full.json",
				"aes-ecb-full-expected.json",
			},
		}, {
			Name: "aes_ecb_short",
			Val: data{
				inputFile:    "aes-ecb-short.json",
				expectedFile: "aes-ecb-short-expected.json",
			},
			ExtraData: []string{
				"aes-ecb-short.json",
				"aes-ecb-short-expected.json",
			},
		}, {
			Name: "aes_cbc_full",
			Val: data{
				inputFile:    "aes-cbc-full.json",
				expectedFile: "aes-cbc-full-expected.json",
			},
			ExtraData: []string{
				"aes-cbc-full.json",
				"aes-cbc-full-expected.json",
			},
		}, {
			Name: "aes_cbc_short",
			Val: data{
				inputFile:    "aes-cbc-short.json",
				expectedFile: "aes-cbc-short-expected.json",
			},
			ExtraData: []string{
				"aes-cbc-short.json",
				"aes-cbc-short-expected.json",
			},
		}, {
			Name: "sha2_256_full",
			Val: data{
				inputFile:    "sha2-256-full.json",
				expectedFile: "sha2-256-full-expected.json",
			},
			ExtraData: []string{
				"sha2-256-full.json",
				"sha2-256-full-expected.json",
			},
		}, {
			Name: "sha2_256_short",
			Val: data{
				inputFile:    "sha2-256-short.json",
				expectedFile: "sha2-256-short-expected.json",
			},
			ExtraData: []string{
				"sha2-256-short.json",
				"sha2-256-short-expected.json",
			},
		}, {
			Name: "sha2_384_full",
			Val: data{
				inputFile:    "sha2-384-full.json",
				expectedFile: "sha2-384-full-expected.json",
			},
			ExtraData: []string{
				"sha2-384-full.json",
				"sha2-384-full-expected.json",
			},
		}, {
			Name: "sha2_384_short",
			Val: data{
				inputFile:    "sha2-384-short.json",
				expectedFile: "sha2-384-short-expected.json",
			},
			ExtraData: []string{
				"sha2-384-short.json",
				"sha2-384-short-expected.json",
			},
		}, {
			Name: "sha2_512_full",
			Val: data{
				inputFile:    "sha2-512-full.json",
				expectedFile: "sha2-512-full-expected.json",
			},
			ExtraData: []string{
				"sha2-512-full.json",
				"sha2-512-full-expected.json",
			},
		}, {
			Name: "sha2_512_short",
			Val: data{
				inputFile:    "sha2-512-short.json",
				expectedFile: "sha2-512-short-expected.json",
			},
			ExtraData: []string{
				"sha2-512-short.json",
				"sha2-512-short-expected.json",
			},
		}, {
			Name: "hmac_drbg",
			Val: data{
				inputFile:    "drbg-test.json",
				expectedFile: "drbg-expected.json",
			},
			ExtraData: []string{
				"drbg-test.json",
				"drbg-expected.json",
			},
		}, {
			Name: "hmac_sha2_256",
			Val: data{
				inputFile:    "hmac-sha2-256-test.json",
				expectedFile: "hmac-sha2-256-expected.json",
			},
			ExtraData: []string{
				"hmac-sha2-256-test.json",
				"hmac-sha2-256-expected.json",
			},
		}},
		Timeout: time.Hour * 10,
	})
}

const (
	wordLen            = 4
	cr50HeaderSize     = 12
	cr50RespHeaderSize = 12
	ecb                = "AES"
	cbc                = "AES-CBC"
	// Trunks hash cmd modes are as follows:
	// 0 - start, 1 - cont., 2 - finish, 3 - single
	// 4 - SW HMAC single shot (TPM code)
	// 5 - HW HMAC SHA256 single shot (dcrypto code)
	cmdModeHash = "03"
	cmdModeHMAC = "05"
)

// Holds test data information for each test type.
type data struct {
	inputFile    string
	expectedFile string
}

// Holds trunks parameters for a hash operation.
type hashPrimitive struct {
	msg string
	alg string
	key string
	cmd string
}

func (hp *hashPrimitive) setAlg(alg string) error {
	// Set the hash_mode for use in call to trunks command
	// Command structure is as follows:
	// field     |    size  |                  note
	// ===================================================================
	// mode      |    1     | 0 - start, 1 - cont., 2 - finish, 3 - single
	// hash_mode |    1     | 0 - sha1, 1 - sha256, 2 - sha384, 3 - sha512
	// handle    |    1     | seassion handle, ignored in 'single' mode
	// text_len  |    2     | size of the text to process, big endian
	// text      | text_len | text to hash
	switch alg {
	case "SHA-1", "HMAC-SHA1":
		hp.alg = "00"
	case "SHA2-256", "HMAC-SHA2-256":
		hp.alg = "01"
	case "SHA2-384", "HMAC-SHA2-384":
		hp.alg = "02"
	case "SHA2-512", "HMAC-SHA2-512":
		hp.alg = "03"
	default:
		return errors.Errorf("unsupported algorithm: %s", alg)
	}
	return nil
}

// setCmd sets the hash cmd for use in call to trunks command
func (hp *hashPrimitive) setCmd(alg, key string) error {
	switch alg {
	case "SHA-1", "SHA2-256", "SHA2-384", "SHA2-512":
		hp.cmd = cmdModeHash
		if key != "" {
			return errors.Errorf("unexpected key value in hash algorithm: %q", key)
		}
	case "HMAC-SHA-1", "HMAC-SHA2-256", "HMAC-SHA2-384", "HMAC-SHA2-512":
		hp.cmd = cmdModeHMAC
		if key == "" {
			return errors.New("missing key value for HMAC")
		}
	default:
		return errors.Errorf("unsupported algorithm: %s", alg)
	}
	return nil
}

// newSHA returns a new hashPrimitive struct for SHA
func newSHA(msg, alg, key string) (*hashPrimitive, error) {
	hp := &hashPrimitive{msg: msg, key: key}
	if err := hp.setAlg(alg); err != nil {
		return nil, err
	}
	if err := hp.setCmd(alg, key); err != nil {
		return nil, err
	}
	return hp, nil
}

// Holds trunks parameters for a block cipher operation.
type blockCipher struct {
	encrypt string
	mode    string
	key     string
	in      string
	iv      string
}

func (bc *blockCipher) setMode(mode string) error {
	switch mode {
	case cbc:
		bc.mode = "02"
	case ecb:
		bc.mode = "00"
	default:
		return errors.Errorf("unsupported mode: %s", mode)
	}
	return nil
}

func (bc *blockCipher) setEnc(encrypt string) error {
	switch encrypt {
	case "encrypt":
		bc.encrypt = "01"
	case "decrypt":
		bc.encrypt = "00"
	default:
		return errors.New("encryption string should contain 'encrypt' or 'decrypt'")
	}
	return nil
}

// newAES returns a new blockCipher struct for AES.
func newAES(dir, key, in, iv, mode string) (*blockCipher, error) {
	bc := &blockCipher{key: key, in: in}
	if mode == ecb && iv != "" {
		return nil, errors.Errorf("have IV in ECB mode: %q", iv)
	}
	bc.iv = iv
	if err := bc.setMode(mode); err != nil {
		return nil, err
	}
	if err := bc.setEnc(dir); err != nil {
		return nil, err
	}
	return bc, nil
}

// Holds trunks parameters for an HMAC DRBG based on SHA256 operation.
type drbgSHA256 struct {
	entropy string
	perso   string
	input   string
	input2  string
	nonce   string
	outLen  uint32
}

// newDRBGSHA256 returns a new HMAC-SHA256-DRBG struct.
func newDRBGSHA256(primitive, outLenStr, entropy, perso, input, input2, nonce string) (*drbgSHA256, error) {
	d := &drbgSHA256{entropy: entropy, perso: perso, input: input, input2: input2, nonce: nonce}
	if primitive != "SHA2-256" {
		return nil, errors.Errorf("HMAC DRBG requesting unsupported hash primitive: %q", primitive)
	}
	outLenBytes, err := hex.DecodeString(outLenStr)
	if err != nil {
		return nil, errors.Errorf("unable to decode required output length from argument: %q", outLenStr)
	}
	d.outLen = binary.LittleEndian.Uint32(outLenBytes)
	if d.outLen > 128 {
		return nil, errors.Errorf("DRBG requested too many bytes: %d, maximum is 128", d.outLen)
	}
	return d, nil
}

// Writes commands to DUT and reads the output.
// This interface is required by acvptool.
type cr50IO struct {
	ctx    context.Context
	outBuf bytes.Buffer // ACVPtool output goes here
}

// Runs a trunks command on the DUT.
func (w *cr50IO) Write(b []byte) (int, error) {
	cmdArgs, err := getTrunksCmds(b)
	if err != nil {
		return 0, errors.Wrap(err, "getTrunksCmd failed")
	}
	for i, cmdArg := range cmdArgs {
		cmd := testexec.CommandContext(w.ctx, "trunks_send", "--raw", cmdArg)
		out, err := cmd.Output(testexec.DumpLogOnError)
		if err != nil {
			return 0, errors.Wrap(err, shutil.EscapeSlice(cmd.Args))
		}
		// For some reason there is an extra byte at the end of command output
		if len(out) > 0 && len(out)%2 != 0 {
			out = out[:len(out)-1]
		}
		respBytes, err := extractTrunksResp(out)
		if err != nil {
			return 0, errors.Wrapf(err, "extractTrunksResp failed after sending trunks command %q", cmdArg)
		}
		// Populate OutBuf with response bytes if we are at the last command.
		if i == len(cmdArgs)-1 {
			w.populateOutBuf(respBytes)
		}
	}
	return len(b), nil
}

// Close does nothing, but is required to implement the ReadWriteCloser interface.
func (w *cr50IO) Close() error {
	return nil
}

// Read the output of the trunks command.
func (w *cr50IO) Read(b []byte) (int, error) {
	return w.outBuf.Read(b)
}

// populateOutBuf takes a cr50 command response and
// converts to output consumable by ACVPtool.
func (w *cr50IO) populateOutBuf(b []byte) {
	respSize := uint32(len(b))
	w.outBuf.Write([]byte{01, 00, 00, 00}) // num responses
	respSizeBytes := make([]byte, 4)
	binary.LittleEndian.PutUint32(respSizeBytes, respSize)
	w.outBuf.Write(respSizeBytes)
	w.outBuf.Write(b)
}

// getTrunksCmds converts contents of b into one or more trunks commands.
func getTrunksCmds(b []byte) ([]string, error) {
	args, err := parseInBuf(b)
	if err != nil {
		return nil, err
	}

	// The first index in array contains algorithm arguments separated by '/'
	algArgs := strings.Split(args[0], "/")
	algType := algArgs[0]
	algArgs = algArgs[1:]
	algArgs = append(algArgs, args[1:]...)
	argLen := len(algArgs)
	switch strings.Split(algType, "-")[0] {
	case "AES":
		var iv string
		switch argLen {
		case 3:
			iv = ""
		case 4:
			iv = algArgs[3]
		default:
			return nil, errors.Errorf("incorrect number of args for AES: got %d, want 3 or 4", argLen)
		}
		bc, err := newAES(algArgs[0], algArgs[1], algArgs[2], iv, algType)
		if err != nil {
			return nil, err
		}
		return []string{getAESCommand(bc)}, nil
	case "SHA", "SHA2", "HMAC":
		var key string
		switch argLen {
		case 1:
			key = ""
		case 2:
			key = algArgs[1]
		default:
			return nil, errors.Errorf("incorrect number of args for hash/HMAC operation: got %d, want 1 or 2", argLen)
		}
		hp, err := newSHA(algArgs[0], algType, key)
		if err != nil {
			return nil, err
		}
		return []string{getHashCommand(hp)}, nil
	case "hmacDRBG":
		if argLen != 7 {
			return nil, errors.Errorf("incorrect number of args for DRBG: got %d, want 7", argLen)
		}
		d, err := newDRBGSHA256(algArgs[0], algArgs[1], algArgs[2], algArgs[3], algArgs[4], algArgs[5], algArgs[6])
		if err != nil {
			return nil, err
		}
		return getDRBGCommands(d), nil
	default:
		return nil, errors.Errorf("unrecognized algorithm: %s", algType)
	}
}

// parseInBuf is a generic parser for ACVPTool input format.
// It returns an array of args passed.
func parseInBuf(b []byte) ([]string, error) {
	if len(b) < 4 {
		return nil, errors.Errorf("input buffer too short: %d bytes", len(b))
	}
	numArgs := binary.LittleEndian.Uint32(b[0:4])
	var res []string
	startInd := numArgs*4 + 4
	if uint32(len(b)) < startInd {
		return nil, errors.Errorf("input buffer too short: %d bytes, cannot extract arguments", len(b))
	}
	// first arg is already a string
	argLen := binary.LittleEndian.Uint32(b[4:8])
	endInd := startInd + argLen
	if uint32(len(b)) < endInd {
		return nil, errors.Errorf("input buffer too short: %d bytes, expected at least %d bytes",
			len(b), endInd)
	}
	res = append(res, string(b[startInd:endInd]))
	startInd = endInd
	for i := uint32(8); i < numArgs*4+4; i += 4 {
		endInd = startInd + binary.LittleEndian.Uint32(b[i:i+4])
		if uint32(len(b)) < endInd {
			return nil, errors.Errorf("input buffer too short: %d bytes, expected at least % bytes",
				len(b), endInd)
		}
		res = append(res, hex.EncodeToString(b[startInd:endInd]))
		startInd = endInd
	}
	return res, nil
}

// getHashCommand constructs a trunks Hash command
// Trunks command gets executed via hash_command_handler in cr50
func getHashCommand(hp *hashPrimitive) string {
	// 8001      TPM_ST_NO_SESSIONS
	// 00000000  Command/response size
	// 20000000  Cr50 Vendor Command (Constant, TPM Command Code)
	// 0000      Vendor Command Code (VENDOR_CC_ enum) 0001 for SHA1, SHA-256
	// Command structure, shared out of band with the test driver running
	// on the host:
	//
	// field     |    size  |                  note
	// ===================================================================
	// hash_cmd  |    1     | 0 - start, 1 - cont., 2 - finish, 3 - single
	//           |          | 4 - SW HMAC single shot (TPM code)
	//           |          | 5 - HW HMAC SHA256 single shot (dcrypto code)
	// hash_alg  |    1     | 0 - sha1, 1 - sha256, 2 - sha384, 3 - sha512
	// handle    |    1     | session handle, ignored in 'single' mode
	// text_len  |    2     | size of the text to process, big endian
	// text      | text_len | text to hash
	// for HMAC single shot only:
	// key_len   |    2     | size of the key for HMAC, big endian
	// key       | key_len  | key for HMAC single shot

	var cmdBody, cmdHeader bytes.Buffer
	cmdHeader.WriteString("8001")
	cmdBody.WriteString(hp.cmd)
	cmdBody.WriteString(hp.alg)
	cmdBody.WriteString("00")
	cmdBody.WriteString(fmt.Sprintf("%04x", len(hp.msg)/2))
	cmdBody.WriteString(hp.msg)
	if hp.cmd == cmdModeHMAC {
		cmdBody.WriteString(fmt.Sprintf("%04x", len(hp.key)/2))
		cmdBody.WriteString(hp.key)
	}
	cmdHeader.WriteString(fmt.Sprintf("%08x", cmdBody.Len()/2+cr50HeaderSize))
	cmdHeader.WriteString("200000000001")
	return cmdHeader.String() + cmdBody.String()
}

// getAESCommand constructs a trunks AES command
// Trunks command gets executed via aes_command_handler in cr50
func getAESCommand(bc *blockCipher) string {
	// Cipher modes being tested
	// CIPHER_MODES = {'ECB': '00', 'CTR': '01', 'CBC': '02',
	// 				'GCM': '03', 'OFB': '04', 'CFB': '05'}
	// 8001      TPM_ST_NO_SESSIONS
	// 00000000  Command/response size
	// 20000000  Cr50 Vendor Command (Constant, TPM Command Code)
	// 0000      Vendor Command Code (VENDOR_CC_ enum) 0000 for AES
	// Command body: test_mode|cipher_mode|
	// Command structure, shared out of band with the test driver running
	// on the host:

	//  field       |    size  |              note
	//  ================================================================
	//  mode        |    1     | 0 - decrypt, 1 - encrypt
	//  cipher_mode |    1     | as per aes_test_cipher_mode
	//  key_len     |    1     | key size in bytes (16, 24 or 32)
	//  key         | key len  | key to use
	//  iv_len      |    1     | either 0 or 16
	//  iv          | 0 or 16  | as defined by iv_len
	//  aad_len     |  <= 127  | additional authentication data length
	//  aad         |  aad_len | additional authentication data
	//  text_len    |    2     | size of the text to process, big endian
	//  text        | text_len | text to encrypt/decrypt

	var cmdBody, cmdHeader bytes.Buffer
	cmdHeader.WriteString("8001")
	cmdBody.WriteString(bc.encrypt)
	cmdBody.WriteString(bc.mode)
	cmdBody.WriteString(fmt.Sprintf("%02x", len(bc.key)/2))
	cmdBody.WriteString(bc.key)
	cmdBody.WriteString(fmt.Sprintf("%02x", len(bc.iv)/2))
	cmdBody.WriteString(bc.iv)
	// AAD is always empty for currently supported modes
	cmdBody.WriteString("00")
	cmdBody.WriteString(fmt.Sprintf("%04x", len(bc.in)/2))
	cmdBody.WriteString(bc.in)
	cmdHeader.WriteString(fmt.Sprintf("%08x", cmdBody.Len()/2+cr50HeaderSize))
	cmdHeader.WriteString("200000000000")

	return cmdHeader.String() + cmdBody.String()
}

// getDRBGGenCommand constructs a trunks command to
// generate output from an already initialized DRBG.
func getDRBGGenCommand(input string, outLen uint32) string {
	var cmdBody, cmdHeader bytes.Buffer
	cmdHeader.WriteString("8001")
	cmdBody.WriteString("02")
	cmdBody.WriteString(fmt.Sprintf("%04x", len(input)/2))
	cmdBody.WriteString(input)
	cmdBody.WriteString(fmt.Sprintf("%04x", outLen))
	cmdHeader.WriteString(fmt.Sprintf("%08x", cmdBody.Len()/2+cr50HeaderSize))
	cmdHeader.WriteString("200000000032")
	return cmdHeader.String() + cmdBody.String()
}

// getDRBGCommands constructs the sequence of trunks commands
// that need to be executed for a given NIST test case.
func getDRBGCommands(d *drbgSHA256) []string {
	// 8001      TPM_ST_NO_SESSIONS
	// 00000000  Command/response size
	// 20000000  Cr50 Vendor Command (Constant, TPM Command Code)
	// 0032      Vendor Command Code (VENDOR_CC_ enum) 0x32 for DRBG
	//
	// DRBG_TEST command structure:
	//
	// field       |    size  |              note
	// ==========================================================================
	// mode        |    1     | 0 - DRBG_INIT, 1 - DRBG_RESEED, 2 - DRBG_GENERATE
	// p0_len      |    2     | size of first input in bytes
	// p0          |  p0_len  | entropy for INIT & SEED, input for GENERATE
	// p1_len      |    2     | size of second input in bytes (for INIT & RESEED)
	//             |          | or size of expected output for GENERATE
	// p1          |  p1_len  | nonce for INIT & SEED
	// p2_len      |    2     | size of third input in bytes for DRBG_INIT
	// p2          |  p2_len  | personalization for INIT & SEED
	//
	// DRBG_INIT (entropy, nonce, perso)
	// DRBG_RESEED (entropy, additional input 1, additional input 2)
	// DRBG_INIT and DRBG_RESEED returns empty response
	// DRBG_GENERATE (p0_len, p0 - additional input 1, p1_len - size of output)
	// DRBG_GENERATE returns p1_len bytes of generated data
	// (up to a maximum of 128 bytes)

	result := make([]string, 3)
	var cmdBody, cmdHeader bytes.Buffer
	cmdHeader.WriteString("8001")
	cmdBody.WriteString("00")
	cmdBody.WriteString(fmt.Sprintf("%04x", len(d.entropy)/2))
	cmdBody.WriteString(d.entropy)
	cmdBody.WriteString(fmt.Sprintf("%04x", len(d.nonce)/2))
	cmdBody.WriteString(d.nonce)
	cmdBody.WriteString(fmt.Sprintf("%04x", len(d.perso)/2))
	cmdBody.WriteString(d.perso)
	cmdHeader.WriteString(fmt.Sprintf("%08x", cmdBody.Len()/2+cr50HeaderSize))
	cmdHeader.WriteString("200000000032")
	result[0] = cmdHeader.String() + cmdBody.String()
	result[1] = getDRBGGenCommand(d.input, d.outLen)
	result[2] = getDRBGGenCommand(d.input2, d.outLen)
	return result
}

// verifyResult verifies that the result groups returned by subprocess equal expected result groups.
func verifyResult(actual, expected []byte) (bool, error) {
	actual = bytes.ToLower(actual)
	expected = bytes.ToLower(expected)
	var actualGroups, expectedGroups interface{}
	if err := json.Unmarshal(actual, &actualGroups); err != nil {
		return false, err
	}
	if err := json.Unmarshal(expected, &expectedGroups); err != nil {
		return false, err
	}
	return reflect.DeepEqual(actualGroups, expectedGroups), nil
}

// extractTrunksResp validates the trunks response is a success and
// extracts the response bytes from raw trunks output
func extractTrunksResp(b []byte) ([]byte, error) {
	// Responses to TPM vendor commands have the following header structure:
	// 8001      TPM_ST_NO_SESSIONS
	// 00000000  Response size
	// 00000000  Response code
	// 0000      Vendor Command Code
	b, err := hex.DecodeString(string(b))
	if err != nil {
		return nil, errors.Wrap(err, "failed to decode string from byte slice")
	}

	if len(b) < cr50RespHeaderSize {
		return nil, errors.Errorf("trunks response too small: %d bytes", len(b))
	}

	respCode := binary.LittleEndian.Uint32(b[6:10])
	if respCode != 0 {
		return nil, errors.Errorf("unexpected response code from Cr50: %x", respCode)
	}

	return b[12:], nil
}

// ACVP takes a JSON generated by the ACVP server and runs the test cases in it.
func ACVP(ctx context.Context, s *testing.State) {
	var vectors acvp.Vectors
	d := s.Param().(data)
	vectorsBytes, err := ioutil.ReadFile(s.DataPath(d.inputFile))
	if err != nil {
		s.Fatal("Failed reading internal data file: ", err)
	} else {
		s.Log("Read data file: ", d.inputFile)
	}

	if err := json.Unmarshal([]byte(vectorsBytes), &vectors); err != nil {
		s.Fatal("Failed to parse vector set: ", err)
	}

	inout := cr50IO{
		ctx: ctx,
	}

	cmd := testexec.CommandContext(ctx, "trunks_send", "--raw")

	// TODO(b/141372763): cmd.Cmd should actually be something empty.
	middle := subprocess.NewWithIO(cmd.Cmd, &inout, &inout)
	defer middle.Close()

	replyGroups, err := middle.Process(vectors.Algo, vectorsBytes)
	if err != nil {
		s.Errorf("Failed to process middle: %s", err)
	}
	s.Log(string(replyGroups))

	// Verify results are correct if an expected NIST results file is provided.
	// Expected results file may not always be present, for example when the
	// result relies on random numbers generated by Cr50.
	if d.expectedFile != "" {
		expectedBytes, err := ioutil.ReadFile(s.DataPath(d.expectedFile))
		if err != nil {
			s.Fatal("Failed reading results data file: ", err)
		}
		match, err := verifyResult(replyGroups, expectedBytes)
		if err != nil {
			s.Fatal("Failed to verify expected result matches processed result: ", err)
		}
		if !match {
			s.Fatal("Result returned by Cr50 does not match expected result")
		}
	}
}
