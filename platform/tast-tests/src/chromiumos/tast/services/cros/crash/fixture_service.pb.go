// Code generated by protoc-gen-go. DO NOT EDIT.
// source: fixture_service.proto

package crash

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	empty "github.com/golang/protobuf/ptypes/empty"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// This deliberately does NOT use the "oldFiles" parameter in WaitForCrashFiles
// because that is redundant with SetUp's function of moving crashes to a
// temporary stash directory (and will eventually be removed).
type WaitForCrashFilesRequest struct {
	Dirs                 []string `protobuf:"bytes,1,rep,name=dirs,proto3" json:"dirs,omitempty"`
	Regexes              []string `protobuf:"bytes,2,rep,name=regexes,proto3" json:"regexes,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *WaitForCrashFilesRequest) Reset()         { *m = WaitForCrashFilesRequest{} }
func (m *WaitForCrashFilesRequest) String() string { return proto.CompactTextString(m) }
func (*WaitForCrashFilesRequest) ProtoMessage()    {}
func (*WaitForCrashFilesRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_4b51ac24a74eb33d, []int{0}
}

func (m *WaitForCrashFilesRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WaitForCrashFilesRequest.Unmarshal(m, b)
}
func (m *WaitForCrashFilesRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WaitForCrashFilesRequest.Marshal(b, m, deterministic)
}
func (m *WaitForCrashFilesRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WaitForCrashFilesRequest.Merge(m, src)
}
func (m *WaitForCrashFilesRequest) XXX_Size() int {
	return xxx_messageInfo_WaitForCrashFilesRequest.Size(m)
}
func (m *WaitForCrashFilesRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_WaitForCrashFilesRequest.DiscardUnknown(m)
}

var xxx_messageInfo_WaitForCrashFilesRequest proto.InternalMessageInfo

func (m *WaitForCrashFilesRequest) GetDirs() []string {
	if m != nil {
		return m.Dirs
	}
	return nil
}

func (m *WaitForCrashFilesRequest) GetRegexes() []string {
	if m != nil {
		return m.Regexes
	}
	return nil
}

type WaitForCrashFilesResponse struct {
	Matches              []*RegexMatch `protobuf:"bytes,1,rep,name=matches,proto3" json:"matches,omitempty"`
	XXX_NoUnkeyedLiteral struct{}      `json:"-"`
	XXX_unrecognized     []byte        `json:"-"`
	XXX_sizecache        int32         `json:"-"`
}

func (m *WaitForCrashFilesResponse) Reset()         { *m = WaitForCrashFilesResponse{} }
func (m *WaitForCrashFilesResponse) String() string { return proto.CompactTextString(m) }
func (*WaitForCrashFilesResponse) ProtoMessage()    {}
func (*WaitForCrashFilesResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_4b51ac24a74eb33d, []int{1}
}

func (m *WaitForCrashFilesResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_WaitForCrashFilesResponse.Unmarshal(m, b)
}
func (m *WaitForCrashFilesResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_WaitForCrashFilesResponse.Marshal(b, m, deterministic)
}
func (m *WaitForCrashFilesResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_WaitForCrashFilesResponse.Merge(m, src)
}
func (m *WaitForCrashFilesResponse) XXX_Size() int {
	return xxx_messageInfo_WaitForCrashFilesResponse.Size(m)
}
func (m *WaitForCrashFilesResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_WaitForCrashFilesResponse.DiscardUnknown(m)
}

var xxx_messageInfo_WaitForCrashFilesResponse proto.InternalMessageInfo

func (m *WaitForCrashFilesResponse) GetMatches() []*RegexMatch {
	if m != nil {
		return m.Matches
	}
	return nil
}

type RemoveAllFilesRequest struct {
	Matches              []*RegexMatch `protobuf:"bytes,1,rep,name=matches,proto3" json:"matches,omitempty"`
	XXX_NoUnkeyedLiteral struct{}      `json:"-"`
	XXX_unrecognized     []byte        `json:"-"`
	XXX_sizecache        int32         `json:"-"`
}

func (m *RemoveAllFilesRequest) Reset()         { *m = RemoveAllFilesRequest{} }
func (m *RemoveAllFilesRequest) String() string { return proto.CompactTextString(m) }
func (*RemoveAllFilesRequest) ProtoMessage()    {}
func (*RemoveAllFilesRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_4b51ac24a74eb33d, []int{2}
}

func (m *RemoveAllFilesRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_RemoveAllFilesRequest.Unmarshal(m, b)
}
func (m *RemoveAllFilesRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_RemoveAllFilesRequest.Marshal(b, m, deterministic)
}
func (m *RemoveAllFilesRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_RemoveAllFilesRequest.Merge(m, src)
}
func (m *RemoveAllFilesRequest) XXX_Size() int {
	return xxx_messageInfo_RemoveAllFilesRequest.Size(m)
}
func (m *RemoveAllFilesRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_RemoveAllFilesRequest.DiscardUnknown(m)
}

var xxx_messageInfo_RemoveAllFilesRequest proto.InternalMessageInfo

func (m *RemoveAllFilesRequest) GetMatches() []*RegexMatch {
	if m != nil {
		return m.Matches
	}
	return nil
}

type RegexMatch struct {
	Regex                string   `protobuf:"bytes,1,opt,name=regex,proto3" json:"regex,omitempty"`
	Files                []string `protobuf:"bytes,2,rep,name=files,proto3" json:"files,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *RegexMatch) Reset()         { *m = RegexMatch{} }
func (m *RegexMatch) String() string { return proto.CompactTextString(m) }
func (*RegexMatch) ProtoMessage()    {}
func (*RegexMatch) Descriptor() ([]byte, []int) {
	return fileDescriptor_4b51ac24a74eb33d, []int{3}
}

func (m *RegexMatch) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_RegexMatch.Unmarshal(m, b)
}
func (m *RegexMatch) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_RegexMatch.Marshal(b, m, deterministic)
}
func (m *RegexMatch) XXX_Merge(src proto.Message) {
	xxx_messageInfo_RegexMatch.Merge(m, src)
}
func (m *RegexMatch) XXX_Size() int {
	return xxx_messageInfo_RegexMatch.Size(m)
}
func (m *RegexMatch) XXX_DiscardUnknown() {
	xxx_messageInfo_RegexMatch.DiscardUnknown(m)
}

var xxx_messageInfo_RegexMatch proto.InternalMessageInfo

func (m *RegexMatch) GetRegex() string {
	if m != nil {
		return m.Regex
	}
	return ""
}

func (m *RegexMatch) GetFiles() []string {
	if m != nil {
		return m.Files
	}
	return nil
}

func init() {
	proto.RegisterType((*WaitForCrashFilesRequest)(nil), "tast.cros.crash.WaitForCrashFilesRequest")
	proto.RegisterType((*WaitForCrashFilesResponse)(nil), "tast.cros.crash.WaitForCrashFilesResponse")
	proto.RegisterType((*RemoveAllFilesRequest)(nil), "tast.cros.crash.RemoveAllFilesRequest")
	proto.RegisterType((*RegexMatch)(nil), "tast.cros.crash.RegexMatch")
}

func init() { proto.RegisterFile("fixture_service.proto", fileDescriptor_4b51ac24a74eb33d) }

var fileDescriptor_4b51ac24a74eb33d = []byte{
	// 346 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x9c, 0x92, 0xcf, 0x4e, 0xf2, 0x40,
	0x14, 0xc5, 0x81, 0xef, 0x43, 0xe4, 0x9a, 0x60, 0x9c, 0x88, 0xa9, 0xb0, 0x21, 0x35, 0x1a, 0x74,
	0x31, 0x4d, 0x30, 0x26, 0x9a, 0xb8, 0xf1, 0x1f, 0x71, 0xa3, 0x8b, 0x41, 0x63, 0xe2, 0xc6, 0x94,
	0x7a, 0x81, 0x26, 0x2d, 0x53, 0xe7, 0x4e, 0x11, 0x9f, 0xd1, 0x97, 0x32, 0xd3, 0x81, 0x18, 0x04,
	0x8c, 0xba, 0xeb, 0xbd, 0x3d, 0x3d, 0x3d, 0xe7, 0x37, 0x03, 0xd5, 0x5e, 0x38, 0xd6, 0xa9, 0xc2,
	0x27, 0x42, 0x35, 0x0a, 0x03, 0xe4, 0x89, 0x92, 0x5a, 0xb2, 0x75, 0xed, 0x93, 0xe6, 0x81, 0x92,
	0xc4, 0x03, 0xe5, 0xd3, 0xa0, 0x56, 0xef, 0x4b, 0xd9, 0x8f, 0xd0, 0xcb, 0x5e, 0x77, 0xd3, 0x9e,
	0x87, 0x71, 0xa2, 0xdf, 0xac, 0xda, 0xbd, 0x06, 0xe7, 0xc1, 0x0f, 0x75, 0x5b, 0xaa, 0x0b, 0x23,
	0x6e, 0x87, 0x11, 0x92, 0xc0, 0x97, 0x14, 0x49, 0x33, 0x06, 0xff, 0x9f, 0x43, 0x45, 0x4e, 0xbe,
	0xf1, 0xaf, 0x59, 0x16, 0xd9, 0x33, 0x73, 0xa0, 0xa4, 0xb0, 0x8f, 0x63, 0x24, 0xa7, 0x90, 0xad,
	0xa7, 0xa3, 0x2b, 0x60, 0x7b, 0x81, 0x13, 0x25, 0x72, 0x48, 0xc8, 0x8e, 0xa0, 0x14, 0xfb, 0x3a,
	0x18, 0xa0, 0x75, 0x5b, 0x6b, 0xd5, 0xf9, 0x97, 0x98, 0x5c, 0x18, 0x9f, 0x1b, 0x23, 0x12, 0x53,
	0xad, 0x7b, 0x0b, 0x55, 0x81, 0xb1, 0x1c, 0xe1, 0x59, 0x14, 0xcd, 0x44, 0xfb, 0xa3, 0xdf, 0x31,
	0xc0, 0xe7, 0x9a, 0x6d, 0x42, 0x31, 0x0b, 0xef, 0xe4, 0x1b, 0xf9, 0x66, 0x59, 0xd8, 0xc1, 0x6c,
	0x7b, 0xe6, 0x57, 0x93, 0x7e, 0x76, 0x68, 0xbd, 0x17, 0xa0, 0xd2, 0xb6, 0xbc, 0x3b, 0x16, 0x37,
	0x3b, 0x81, 0x62, 0x07, 0xf5, 0x7d, 0xc2, 0xb6, 0xb8, 0x25, 0xcc, 0xa7, 0x84, 0xf9, 0x95, 0x21,
	0x5c, 0x5b, 0xb2, 0x77, 0x73, 0x2c, 0x82, 0x8d, 0x39, 0x56, 0x6c, 0x7f, 0xae, 0xc2, 0xb2, 0x93,
	0xa9, 0x1d, 0xfc, 0x44, 0x6a, 0xd1, 0xbb, 0x39, 0x26, 0xa0, 0x32, 0x4b, 0x91, 0xed, 0x2d, 0xa0,
	0xb5, 0x00, 0xf3, 0x37, 0x0d, 0x4e, 0x61, 0xf5, 0x0e, 0x7d, 0x75, 0x29, 0x5f, 0x87, 0xbf, 0xef,
	0x7f, 0xbe, 0xfb, 0xb8, 0x13, 0x0c, 0x94, 0x8c, 0xc3, 0x34, 0x96, 0xe4, 0x99, 0x2c, 0xde, 0xe4,
	0x12, 0x93, 0x67, 0x42, 0x79, 0x59, 0xa8, 0xee, 0x4a, 0xf6, 0xe1, 0xe1, 0x47, 0x00, 0x00, 0x00,
	0xff, 0xff, 0xd0, 0x3a, 0x63, 0x8c, 0xea, 0x02, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// FixtureServiceClient is the client API for FixtureService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type FixtureServiceClient interface {
	// SetUp sets up the DUT for a crash test.
	// For more details on what, precisely, it does, see documentation for
	// "SetUpCrashTest" in the local/crash tast library.
	// *NOTE*: If the DUT reboots during the test, it will clear
	// crash_test_in_progress state.
	// After the test is complete, you must call TearDown to clean up the
	// associated resources.
	SetUp(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*empty.Empty, error)
	// WaitForCrashFiles waits for the specified crash files to be present.
	// See crash.go's WaitForCrashFiles for interface details.
	WaitForCrashFiles(ctx context.Context, in *WaitForCrashFilesRequest, opts ...grpc.CallOption) (*WaitForCrashFilesResponse, error)
	// RemoveAllFiles removes all files in the request.
	RemoveAllFiles(ctx context.Context, in *RemoveAllFilesRequest, opts ...grpc.CallOption) (*empty.Empty, error)
	// TearDown undoes the actions SetUp does and resets the machine to normal
	// state.
	TearDown(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*empty.Empty, error)
}

type fixtureServiceClient struct {
	cc *grpc.ClientConn
}

func NewFixtureServiceClient(cc *grpc.ClientConn) FixtureServiceClient {
	return &fixtureServiceClient{cc}
}

func (c *fixtureServiceClient) SetUp(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.crash.FixtureService/SetUp", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fixtureServiceClient) WaitForCrashFiles(ctx context.Context, in *WaitForCrashFilesRequest, opts ...grpc.CallOption) (*WaitForCrashFilesResponse, error) {
	out := new(WaitForCrashFilesResponse)
	err := c.cc.Invoke(ctx, "/tast.cros.crash.FixtureService/WaitForCrashFiles", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fixtureServiceClient) RemoveAllFiles(ctx context.Context, in *RemoveAllFilesRequest, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.crash.FixtureService/RemoveAllFiles", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *fixtureServiceClient) TearDown(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, "/tast.cros.crash.FixtureService/TearDown", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// FixtureServiceServer is the server API for FixtureService service.
type FixtureServiceServer interface {
	// SetUp sets up the DUT for a crash test.
	// For more details on what, precisely, it does, see documentation for
	// "SetUpCrashTest" in the local/crash tast library.
	// *NOTE*: If the DUT reboots during the test, it will clear
	// crash_test_in_progress state.
	// After the test is complete, you must call TearDown to clean up the
	// associated resources.
	SetUp(context.Context, *empty.Empty) (*empty.Empty, error)
	// WaitForCrashFiles waits for the specified crash files to be present.
	// See crash.go's WaitForCrashFiles for interface details.
	WaitForCrashFiles(context.Context, *WaitForCrashFilesRequest) (*WaitForCrashFilesResponse, error)
	// RemoveAllFiles removes all files in the request.
	RemoveAllFiles(context.Context, *RemoveAllFilesRequest) (*empty.Empty, error)
	// TearDown undoes the actions SetUp does and resets the machine to normal
	// state.
	TearDown(context.Context, *empty.Empty) (*empty.Empty, error)
}

// UnimplementedFixtureServiceServer can be embedded to have forward compatible implementations.
type UnimplementedFixtureServiceServer struct {
}

func (*UnimplementedFixtureServiceServer) SetUp(ctx context.Context, req *empty.Empty) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SetUp not implemented")
}
func (*UnimplementedFixtureServiceServer) WaitForCrashFiles(ctx context.Context, req *WaitForCrashFilesRequest) (*WaitForCrashFilesResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method WaitForCrashFiles not implemented")
}
func (*UnimplementedFixtureServiceServer) RemoveAllFiles(ctx context.Context, req *RemoveAllFilesRequest) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method RemoveAllFiles not implemented")
}
func (*UnimplementedFixtureServiceServer) TearDown(ctx context.Context, req *empty.Empty) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method TearDown not implemented")
}

func RegisterFixtureServiceServer(s *grpc.Server, srv FixtureServiceServer) {
	s.RegisterService(&_FixtureService_serviceDesc, srv)
}

func _FixtureService_SetUp_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(empty.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FixtureServiceServer).SetUp(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.crash.FixtureService/SetUp",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FixtureServiceServer).SetUp(ctx, req.(*empty.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _FixtureService_WaitForCrashFiles_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(WaitForCrashFilesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FixtureServiceServer).WaitForCrashFiles(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.crash.FixtureService/WaitForCrashFiles",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FixtureServiceServer).WaitForCrashFiles(ctx, req.(*WaitForCrashFilesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FixtureService_RemoveAllFiles_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RemoveAllFilesRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FixtureServiceServer).RemoveAllFiles(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.crash.FixtureService/RemoveAllFiles",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FixtureServiceServer).RemoveAllFiles(ctx, req.(*RemoveAllFilesRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _FixtureService_TearDown_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(empty.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FixtureServiceServer).TearDown(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/tast.cros.crash.FixtureService/TearDown",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FixtureServiceServer).TearDown(ctx, req.(*empty.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

var _FixtureService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "tast.cros.crash.FixtureService",
	HandlerType: (*FixtureServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SetUp",
			Handler:    _FixtureService_SetUp_Handler,
		},
		{
			MethodName: "WaitForCrashFiles",
			Handler:    _FixtureService_WaitForCrashFiles_Handler,
		},
		{
			MethodName: "RemoveAllFiles",
			Handler:    _FixtureService_RemoveAllFiles_Handler,
		},
		{
			MethodName: "TearDown",
			Handler:    _FixtureService_TearDown_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "fixture_service.proto",
}
