विकसक पर्याय
डीबग माहिती दर्शवा
OS पडताळणी चालू करा
बंद करा
भाषा
नेटवर्कवरून बूट करा
बूट Legacy BIOS
USB ने बूट करा
USB किंवा SD कार्डवरून बूट करा
अंतर्गत डिस्कने बूट करा
रद्द करा
OS पडताळणी चालू करणे निश्चित करा
OS पडताळणी बंद करा
OS पडताळणी बंद करणे निश्चित करा
वर किंवा खाली नेव्हिगेट करण्यासाठी व्हॉल्युम बटणे वापरा
आणि एखादा पर्याय निवडण्यासाठी पॉवर बटण वापरा.
OS पडताळणी बंद केल्याने तुमची सिस्टम असुरक्षित होईल.
सुरक्षित राहण्यासाठी "रद्द करा" निवडा.
OS पडताळणी बंद आहे. तुमची सिस्टम असुरक्षित आहे.
सुरक्षितता पुन्हा मिळवण्यासाठी "OS पडताळणी सुरू करा" निवडा.
तुमच्या सिस्टमच्या सुरक्षेसाठी "OS पडताळणी सुरू करणे निश्चित करा" निवडा.
