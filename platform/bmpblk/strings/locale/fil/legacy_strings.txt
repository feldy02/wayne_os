Hindi makita o nasira ang Chrome OS.
Maglagay ng pang-recover na USB stick o SD card.
Maglagay ng pang-recover na USB stick.
Maglagay ng pang-recover na SD card o USB stick (tandaan: HINDI gagana ang asul na USB port sa pag-recover).
Maglagay ng pang-recover na USB stick sa isa sa 4 na port sa LIKOD ng device.
Walang Chrome OS sa device na iyong inilagay.
NAKA-OFF ang pag-verify ng OS
Pindutin ang SPACE upang muling i-enable.
Pindutin ang ENTER upang kumpirmahing gusto mong i-on ang pag-verify ng OS.
Magre-reboot ang iyong system at maki-clear ang lokal na data.
Upang bumalik, pindutin ang ESC.
NAKA-ON ang pag-verify ng OS.
Upang I-OFF ang pag-verify ng OS, pindutin ang ENTER.
Para sa tulong, bisitahin ang https://google.com/chromeos/recovery
Code ng error
Pakialis ang lahat ng external na device upang simulan ang pag-recover.
Modelong 60061e
Upang I-OFF ang pag-verify ng OS, pindutin ang button na PAG-RECOVER.
Walang sapat na power ang nakakonektang power supply upang paganahin ang device na ito.
Magsa-shut down na ngayon ang Chrome OS.
Pakigamit ang tamang adapter at subukang muli.
Pakialis ang lahat ng nakakonektang device at simulan ang pag-recover.
Pumindot ng numeric key para pumili ng alternatibong bootloader:
Pindutin ang POWER button para patakbuhin ang mga diagnostic.
Para I-OFF ang pag-verify ng OS, pindutin ang POWER button.
