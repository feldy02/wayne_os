# Copyright (c) 2014 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
from fuzzy_check import FuzzyCheck
from validators import *

# originally generated gestures:
# Motion d=288.4 x=287.9 y=5.243 r=0.2036 s=81.65

def Validate(raw, events, gestures):
  fuzzy = FuzzyCheck()
  fuzzy.expected = [
    MotionValidator("== 288.4 ~ 10"),
  ]
  fuzzy.unexpected = [
  ]
  return fuzzy.Check(gestures)
