# Tast: Getting Help

## Bug reports

Please file bug reports and feature requests in the [Chromium issue tracker],
using the following components:

*   `Tests>Tast>Framework`: Core framework issues.
*   `Tests>Tast`: Everything else.

[Chromium issue tracker]: https://bugs.chromium.org/p/chromium/issues/list

## Mailing lists

*   [tast-users]: General discussion about writing and running tests.
*   [tast-reviewers]: (Googler only) Alias of [Tast reviewers](code_reviews.md).
*   [chromeos-velocity]: (Googler only) Group of engineers working on Tast core
    framework development.

[tast-users]: https://groups.google.com/a/chromium.org/forum/#!forum/tast-users
[tast-reviewers]: https://groups.google.com/a/google.com/forum/#!forum/tast-reviewers
[chromeos-velocity]: https://groups.google.com/a/google.com/forum/#!forum/chromeos-velocity

## Core team meeting notes (Googler only)

Meeting notes of Tast core framework weekly is available at [go/tast-weekly].

[go/tast-weekly]: https://goto.google.com/tast-weekly
