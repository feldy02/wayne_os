# Firmware EngProd Tools

* fw_e2e_coverage_summarizer: Used to generate firmware coverage reports (if demonstrating sufficient value, this utility will eventually be generalized to other, non-firmware test areas).
* fw_lab_triage_helper: Used to surface information that is useful for isolating problems during triage of firmware lab test results.

Maintainers contact: cros-fw-te@google.com
