// Copyright 2019 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

syntax = "proto3";

package testplans;

option go_package = "go.chromium.org/chromiumos/infra/proto/go/testplans";

import "chromite/api/depgraph.proto";
import "testplans/common.proto";
import "google/protobuf/wrappers.proto";

// The input proto for invocations of the Pointless Build Checker program.
message PointlessBuildCheckRequest {
  // Serialized buildbucket GerritChanges, or none if this is a postsubmit run
  // or similar. Explicit proto import is avoided here to prevent a dependency
  // on the luci-go repo.
  // See https://chromium.googlesource.com/infra/luci/luci-go/+/master/buildbucket/proto/common.proto
  repeated ProtoBytes gerrit_changes = 7;

  // The DepGraph for a child builder. This can be left empty, implying that no
  // DepGraph-based pointless build checking should be skipped.
  chromite.api.DepGraph dep_graph = 5;

  // The manifest-internal snapshot commit hash that's being used for the current build.
  string manifest_commit = 6;
}

// The output proto for invocations of the Pointless Build Checker program.
message PointlessBuildCheckResponse {
  // Whether the build is pointless and can be terminated without proceeding to
  // building packages and testing.
  google.protobuf.BoolValue build_is_pointless = 1;

  enum PointlessBuildReason {
    POINTLESS_BUILD_REASON_UNSPECIFIED = 0;
    IRRELEVANT_TO_DEPS_GRAPH = 1;
    IRRELEVANT_TO_KNOWN_NON_PORTAGE_DIRECTORIES = 2;
  }

  // If build_is_pointless, this is the reason that the Pointless Build Checker
  // came to that conclusion. Otherwise, this is unspecified.
  PointlessBuildReason pointless_build_reason = 2;
}
