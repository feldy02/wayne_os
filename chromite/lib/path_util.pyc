ó
Ûë+]c           @  s&  d  Z  d d l m Z d d l Z d d l Z d d l Z d d l m Z d d l m Z d d l m	 Z
 d d l m Z d d l m Z d d	 l m Z d
 Z d Z d Z d Z d Z d Z e j d d d d g  Z d e f d     YZ d   Z d   Z d   Z d d  Z d d  Z d S(   s&   Handle path inference and translation.iÿÿÿÿ(   t   print_functionN(   t	   constants(   t   cros_build_lib(   t   cros_logging(   t   git(   t   memoize(   t   osutilss   .cachet
   cros_caches   .cros_cachet   unknownt   gclientt   repot   CheckoutInfot   typet   roott   chrome_src_dirt   ChrootPathResolverc           B  s}   e  Z d  Z d e d  Z e e j d     Z	 d   Z
 d   Z d   Z d   Z d   Z d   Z d	   Z d
   Z RS(   s  Perform path resolution to/from the chroot.

  Args:
    source_path: Value to override default source root inference.
    source_from_path_repo: Whether to infer the source root from the converted
      path's repo parent during inbound translation; overrides |source_path|.
  c         C  s²   t  j   |  _ | d  k r$ t j n | |  _ | |  _ |  j r] d  |  _ d  |  _	 d  |  _
 nQ |  j |  j  |  _ |  j |  j  |  _	 t j |  j f t j |  j f f |  _
 d  S(   N(   R   t   IsInsideChroott   _inside_chroott   NoneR   t   SOURCE_ROOTt   _source_patht   _source_from_path_repot   _chroot_patht   _chroot_linkt   _chroot_to_host_rootst   _GetSourcePathChroott   _ReadChrootLinkt   CHROOT_SOURCE_ROOTt   CHROOT_CACHE_ROOTt   _GetCachePath(   t   selft   source_patht   source_from_path_repo(    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyt   __init__8   s    					c         C  s   t  j j t    S(   s   Returns the cache directory.(   t   ost   patht   realpatht   GetCacheDir(   t   cls(    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyR   R   s    c         C  s&   | d k r d St j j | t j  S(   s<   Returns path to the chroot directory of a given source root.N(   R   R"   R#   t   joinR   t   DEFAULT_CHROOT_DIR(   R   R   (    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyR   X   s    c         C  s?   | s
 d St j j |  } t j |  } | | k r; d S| S(   sD  Convert a chroot symlink to its absolute path.

    This contains defaults/edge cases assumptions for chroot paths. Not
    recommended for non-chroot paths.

    Args:
      path (str|None): The path to resolve.

    Returns:
      str|None: The resolved path if the provided path is a symlink, None
        otherwise.
    N(   R   R"   R#   t   abspathR   t   ResolveSymlink(   R   R#   t   abs_patht   link(    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyR   ^   s    c         C  s   | j  t j j | d   r/ | | k r/ d St |  rD |   n | } | d k re t d   n  t j j | | t |  j t j j	   S(   sX  If |path| starts with |src_root|, replace it using |dst_root_input|.

    Args:
      path: An absolute path we want to convert to a destination equivalent.
      src_root: The root that path needs to be contained in.
      dst_root_input: The root we want to relocate the relative path into, or a
        function returning this value.

    Returns:
      A translated path, or None if |src_root| is not a prefix of |path|.

    Raises:
      ValueError: If |src_root| is a prefix but |dst_root_input| yields None,
        which means we don't have sufficient information to do the translation.
    t    s#   No target root to translate path toN(
   t
   startswithR"   R#   R'   R   t   callablet
   ValueErrort   lent   lstript   sep(   R   R#   t   src_roott   dst_root_inputt   dst_root(    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyt   _TranslatePath{   s    +c         C  s]  d } |  j } |  j } |  j } |  j r t j |  } | d k	 ri t j j	 t j j
 | d   } n  |  j |  } |  j |  } n  | d k	 rÞ |  j | | d  } | d k rÞ | d k	 rÞ |  j | | d  } qÞ n  | d k r|  j | |  j   t j  } n  | d k r>| d k	 r>|  j | | t j  } n  | d k rYt d   n  | S(   sÎ  Translates a fully-expanded host |path| into a chroot equivalent.

    This checks path prefixes in order from the most to least "contained": the
    chroot itself, then the cache directory, and finally the source tree. The
    idea is to return the shortest possible chroot equivalent.

    Args:
      path: A host path to translate.

    Returns:
      An equivalent chroot path.

    Raises:
      ValueError: If |path| is not reachable from the chroot.
    s   ..t   /s%   Path is not reachable from the chrootN(   R   R   R   R   R   R   t   FindRepoDirR"   R#   R)   R'   R   R   R7   R   R   R   R   R0   (   R   R#   t   new_pathR   t   chroot_patht   chroot_linkt   path_repo_dir(    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyt   _GetChrootPath   s.    				$c         C  sÖ   d } x< |  j D]1 \ } } |  j | | |  } | d k	 r Pq q W| d k rl |  j | d |  j  } nf |  j | |  j d  } | d k r´ |  j r´ |  j | |  j d  } n  | d k	 rÒ |  j |  } n  | S(   s  Translates a fully-expanded chroot |path| into a host equivalent.

    We first attempt translation of known roots (source). If any is successful,
    we check whether the result happens to point back to the chroot, in which
    case we trim the chroot path prefix and recurse. If neither was successful,
    just prepend the chroot path.

    Args:
      path: A chroot path to translate.

    Returns:
      An equivalent host path.

    Raises:
      ValueError: If |path| could not be mapped to a proper host destination.
    R-   R8   N(   R   R   R7   R   R   t   _GetHostPath(   R   R#   R:   R4   R6   (    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyR?   È   s    c         C  s½   t  j j |  } t  j j |  r` t  j j t  j j t  j j |   t  j j |   } n t  j j |  } |  j r | Sy | |  SWn) t	 k
 r¸ } t	 d | | f   n Xd S(   s#  Expands |path|; if outside the chroot, applies |get_converted_path|.

    Args:
      path: A path to be converted.
      get_converted_path: A conversion function.

    Returns:
      An expanded and (if needed) converted path.

    Raises:
      ValueError: If path conversion failed.
    s   %s: %sN(
   R"   R#   t
   expandusert   isfileR'   R$   t   dirnamet   basenameR   R0   (   R   R#   t   get_converted_patht   expanded_patht   e(    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyt   _ConvertPathð   s    		c         C  s   |  j  | |  j  S(   s:   Resolves current environment |path| for use in the chroot.(   RG   R>   (   R   R#   (    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyt   ToChroot  s    c         C  s   |  j  | |  j  S(   s:   Resolves chroot |path| for use in the current environment.(   RG   R?   (   R   R#   (    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyt
   FromChroot  s    N(   t   __name__t
   __module__t   __doc__R   t   TrueR!   t   classmethodR   t   MemoizedSingleCallR   R   R   R7   R>   R?   RG   RH   RI   (    (    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyR   "   s   				6	(	#	c         C  sÝ   t  } d \ } } xy t j |   D]h } t j j | d  } t j j |  rY t } Pn  t j j | d  } t j j	 |  r" t
 } Pq" q" W| t  k r£ | } n  d } | t k rÍ t j j | d  } n  t | | |  S(   s  Gather information on the checkout we are in.

  There are several checkout types, as defined by CHECKOUT_TYPE_XXX variables.
  This function determines what checkout type |cwd| is in, for example, if |cwd|
  belongs to a `repo` checkout.

  Returns:
    A CheckoutInfo object with these attributes:
      type: The type of checkout.  Valid values are CHECKOUT_TYPE_*.
      root: The root of the checkout.
      chrome_src_dir: If the checkout is a Chrome checkout, the path to the
        Chrome src/ directory.
  s   .gclients   .repot   srcN(   NN(   t   CHECKOUT_TYPE_UNKNOWNR   R   t   IteratePathParentsR"   R#   R'   t   existst   CHECKOUT_TYPE_GCLIENTt   isdirt   CHECKOUT_TYPE_REPOR   (   t   cwdt   checkout_typeR   R#   t   gclient_filet   repo_dirR   (    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyt   DetermineCheckout  s"    	c          C  sö   t  j   }  t |   } d } | j t k rH t  j j | j t	  } nª | j t
 k r² t  j j | j d t  } t  j j | j t  } t  j j |  rò t j d |  qò n@ | j t k rß t  j j t j   d  } n t d | j   | S(   s@   Returns the cache directory location based on the checkout type.t   buildsY   The location of Chrome's cache dir has changed. The old path at %s can safely be removed.s   chromeos-caches   Unexpected type %sN(   R"   t   getcwdR[   R   R   RV   R#   R'   R   t   GENERAL_CACHE_DIRRT   R   t   CHROME_CACHE_DIRt   OLD_CHROME_CACHE_DIRRS   t   loggingt   warningRQ   t   tempfilet
   gettempdirt   AssertionError(   RW   t   checkoutR#   t   old_path(    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyt   FindCacheDirB  s     c           C  s   t  j j t j t    S(   s   Returns the current cache dir.(   R"   t   environt   getR   t   SHARED_CACHE_ENVVARRh   (    (    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyR%   Y  s    c         C  s   t  d |  j |   S(   sk  Resolves current environment |path| for use in the chroot.

  Args:
    path: string path to translate into chroot namespace.
    source_path: string path to root of source checkout with chroot in it.

  Returns:
    The same path converted to "inside chroot" namespace.

  Raises:
    ValueError: If the path references a location not available in the chroot.
  R   (   R   RH   (   R#   R   (    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyt   ToChrootPath^  s    c         C  s   t  d |  j |   S(   s  Resolves chroot |path| for use in the current environment.

  Args:
    path: string path to translate out of chroot namespace.
    source_path: string path to root of source checkout with chroot in it.

  Returns:
    The same path converted to "outside chroot" namespace.
  R   (   R   RI   (   R#   R   (    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyt   FromChrootPathn  s    
(   RL   t
   __future__R    t   collectionsR"   Rc   t   chromite.libR   R   R   Ra   R   R   R   R^   R_   R`   RQ   RT   RV   t
   namedtupleR   t   objectR   R[   Rh   R%   R   Rl   Rm   (    (    (    s0   /home/dev01/chromiumos/chromite/lib/path_util.pyt   <module>   s0   ú	&		